/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef ADDBOOKMARK_H
#define ADDBOOKMARK_H

#include <QtCore>
#include <QtGui>
#include <QString>
#include <QDialog>

class AddBookmarkDialog : public QDialog
{
    Q_OBJECT

public:
    AddBookmarkDialog(QWidget * parent = 0, Qt::WindowFlags f = 0);
    ~AddBookmarkDialog();
    void addFolderLocation(QString path);
    void setPageDetails(QString u, QString t);
signals:
    void addNewBookmark(QString, QString, QString);
protected slots:
    void accept();
private:
    QDialogButtonBox *buttonBox;
    QLabel *labelAddBookmark;
    QLabel *label_task;
    QWidget *widget;
    QHBoxLayout *layout;
    QVBoxLayout *vlayout1;
    QLabel *label_title;
    QLabel *label_CreateIn;
    QVBoxLayout *vlayout2;
    QLineEdit *titleBox;
    QComboBox *folderBox;
    QString url;
    QString title;
};

#endif // ADDBOOKMARK_H
