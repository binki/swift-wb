/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef COOKIEJAR_H
#define COOKIEJAR_H

#include <QNetworkCookieJar>
#include <QList>
#include <QNetworkCookie>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include "settings.h"

class CookieJar : public QNetworkCookieJar
{
	Q_OBJECT

public:
	CookieJar(QObject *parent);
	~CookieJar();
	virtual QList<QNetworkCookie> cookiesForUrl (const QUrl &) const;
	virtual bool setCookiesFromUrl(const QList<QNetworkCookie> &, const QUrl &);
    void deleteSessionCookies();
protected:
	QList<QNetworkCookie> allCookies() const;
	void setAllCookies (const QList<QNetworkCookie> &);
private:
    void createCookieTable();
    bool isValidCookie(const QUrl &myUrl, QString cookieHost, QString cookiePath) const;
    QString getSqlFormatDomain(const QUrl &myUrl) const;
    void buildGTldTable();
    QStringList gTldTable;
    QStringList levelTwo;
};

#endif // COOKIEJAR_H
