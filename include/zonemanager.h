/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef ZONEMANAGER_H
#define ZONEMANAGER_H

#include <QWebSettings>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QUrl>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDesktopServices>
#include <QMessageBox>

class ZoneManager
{
public:
    enum Zone { Internet, Trusted, Restricted };
    static void checkForZoneDatabaseTable();
    static QString addWebSite(QString site, ZoneManager::Zone zone);
    static void removeWebSite(QString site);
    static ZoneManager::Zone webSiteZone(QUrl site);
    static QStringList sitesForZone(ZoneManager::Zone zone);
};

class EditZoneDialog : public QDialog
{
    Q_OBJECT
public:
    EditZoneDialog(QWidget *parent, ZoneManager::Zone zone, QString inputURL);
protected slots:
    void addWebSite();
    void removeWebSite();
    void currentRowChanged(int);
private:
    QGridLayout *gridLayout;
    QLabel *m_pixmap;
    QLabel *descriptionLabel;
    QFrame *line;
    QLabel *addWebSiteLabel;
    QLineEdit *m_urlEdit;
    QPushButton *m_add;
    QLabel *websites;
    QListWidget *m_websites;
    QPushButton *m_remove;
    QSpacerItem *verticalSpacer;
    QDialogButtonBox *m_buttonBox;
    ZoneManager::Zone m_zone;
};

class ZoneLabel : public QWidget
{
	Q_OBJECT

public:
    ZoneLabel(QWidget *parent);
    ~ZoneLabel();
    void setZone(ZoneManager::Zone zone);
private:
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QLabel *icon;
    QLabel *zoneText;
};

#endif
