/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef SWIFTWND_H
#define SWIFTWND_H

#include <QtGui>
#include <math.h>

#include "tabwidget.h"
#include "zonemanager.h"
#include "locationbar.h"
#include "bookmarkmanager.h"
#include "addbookmark.h"
#include "searchbox.h"
#include "about.h"
#include "preferences.h"
#include "sevenhelper.h"
#include "historymanager.h"

class TabWidget;

class SwiftMainWindow : public QMainWindow
{
	Q_OBJECT
public:
    SwiftMainWindow(QWidget *parent = 0, Qt::WFlags flags = 0, bool isPrivate = false, bool isPopup = false, QUrl url = QUrl());
    ~SwiftMainWindow();
protected slots:
    /* swift core functions */
    void exitSwift();
    void aboutToShowGoMenu();
    void triggeredGoMenuAction(QAction*);
    void navigateToLocation(QUrl);
    void updateLocation(const QUrl);
    void startPrivateBrowsing();
    void newTab();
    void newTab(QUrl);
    void newWindow();
    void navigateToSearch(QUrl);
    void addTrusted();
    /* webkit signals, et al */
    void currentChanged(int);
    void trashedTab(QString, QUrl);
    void updateActions();
    void loadProgress(int);
    void loadStarted();
    void loadFinished(bool);
    void urlClicked(const QUrl);
    void linkHovered(QString, QString);
    void updateIcon();
    /* view functions */
    void raiseParent();
    void reopenTrashedTab(QAction*);
    void toggleToolBar();
    void toggleBookmarkBar();
    void toggleStatusBar();
    void toggleMenuBar();
    /* ---zoom functions */
    void changeZoom(QAction*);
    void zoomIn();
    void zoomOut();
    void normalZoomLevel();
    /* find functions */
    void findTextChanged(QString);
    void findText();
    void findTextPrevious();
    void showFindBar();
    void hideFindBar();
    /* bookmark management functions */
    void bookmarkTriggered(QAction *action);
    void addNewBookmark(QString,QString,QString);
    void showPreferences();
    void showDownloads();
    void showAbout();
    /* trivial actions */
    void print();
    void printPreview();
    void showSource();
    void showInfo();
    /* void openFile();
    void savePage(); */
    void toggleFullScreen();
protected:
    void closeEvent(QCloseEvent *);
private:
    void removeHistoryActions();
    void setupTabWidget();
    void setupActions();
    void setupStatusBar();
    void setupToolbar();
    void setupMenuBar();
    void setupZoomButton();
    void setupFindBar();
    void setZoomItemsUnchecked();
    void setCheckedZoomItem(QString);
    /* bookmark functions */
    void parseBookmarks();
    void addBookmarkFolder(QString folder);
    void insertFolder(QString);
    void insertBookmark(QString, QString, QString);
    void insertSeparator(QString);
    bool haveMenu(QString);
    QMenu* findMenu(QString);

    /* widgets, vars, et al */
    QAction *action_New_Window;
    QAction *action_New_Tab;
    QAction *actionOpen_Location;
    QAction *actionOpen_File;
    QAction *action_Save_Page_As;
    QAction *actionSend_Link;
    QAction *action_Print;
    QAction *action_Print_Preview;
    QAction *action_Import;
    QAction *action_Work_Offline;
    QAction *action_Close_Tab;
    QAction *actionClose_Window;
    QAction *action_Exit;
    QAction *action_Undo;
    QAction *action_Redo;
    QAction *action_Cut;
    QAction *actionC_opy;
    QAction *action_Paste;
    QAction *action_Find;
    QAction *actionWeb_Search;
    QAction *actionFind_Again;
    QAction *action_Back;
    QAction *action_Forward;
    QAction *action_Home;
    QAction *action_History;
    QAction *action_Recent_Pages;
    QAction *action_Add_Bookmark;
    QAction *action_Bookmark_All_Tabs;
    QAction *action_Organize_Bookmarks;
    QAction *action_Downloads;
    QAction *action_Error_Console;
    QAction *action_Page_Info;
    QAction *actionPage_Source;
    QAction *action_Start_Private_Browsing;
    QAction *action_Clear_Private_Data;
    QAction *action_Preferences;
    QAction *action_Swift_Help;
    QAction *action_Swift_on_the_Web;
    QAction *action_New_Users_Guide;
    QAction *action_Report_a_Bug;
    QAction *action_About_Swift;
    QAction *action_Check_for_Updates;
    QAction *action_Status_Bar;
    QAction *action_Menu_Bar;
    QAction *action_Stop;
    QAction *action_Zoom_In;
    QAction *actionZoom_Out;
    QAction *actionNormal_Zoom_Level;
    QAction *action_Full_Screen;
    QAction *action_Reload;
    QAction *action_Add_Trusted;
    QAction *menuButton;
    QMenu *mainMenu;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menu_File;
    QMenu *menu_Edit;
    QMenu *menu_Go;
    QMenu *menu_Bookmark;
    QMenu *menu_Tools;
    QMenu *menu_Help;
    QMenu *menu_View;
    QToolBar *mainToolBar;
    QToolBar *bookmarkBar;
    QToolBar *findBar;
    QLineEdit *findInPageText;
    QAction *findNext;
    QAction *findPrevious;
    QAction *closeFindBar;
    QLabel *infoPixmap;
    QLabel *infoLabel;
    QCheckBox *findMatchCase;
    QLabel *findLabel;
    QStatusBar *statusBar;
    QLabel *recentStatus;
    QProgressBar *m_loadProgress;
    QToolButton *m_newTab;
    QToolButton *m_trash;
    QMenu *m_trashedTabs;
    QToolButton *m_zoomFactor;
    QMenu *m_zoomMenu;
    QList<QMenu*> bookmarkFolders;
    /* swift classes (non-Qt Widgets et cetera) */
    LocationBar *locationBar;
    ZoneLabel *m_zone;
    TabWidget *tabWidget;
    Settings *settings;
    BookmarkManager *bm;
    AddBookmarkDialog *bookmarkDlg;
    bool m_private;
    bool m_popup;
};

#endif // SWIFT_H
