/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef PAGEINFO_H
#define PAGEINFO_H

#include <QtCore>
#include <QtGui>
#include <QSslCipher>

#include "webview.h"
#include "historymanager.h"

class WebView;

class PageInfo : public QDialog
{
    Q_OBJECT
public:
    PageInfo(WebView *parent, bool showSource);
private:
    void retranslate();
    QGridLayout *gridLayout_2;
    QTabWidget *tabWidget;
    QWidget *general;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_3;
    QLabel *pageTitle;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *labelLayout;
    QLabel *tAddress;
    QLabel *tType;
    QLabel *tEncoding;
    QLabel *tSize;
    QLabel *tReferring;
    QVBoxLayout *verticalLayout_2;
    QLabel *pageLocation;
    QLabel *pageType;
    QLabel *pageEncoding;
    QLabel *pageSize;
    QLabel *referringURL;
    QGroupBox *metaGroupBox;
    QGridLayout *gridLayout_3;
    QTableWidget *metaTags;
    QWidget *security;
    QGridLayout *gridLayout_9;
    QGroupBox *identity;
    QGridLayout *gridLayout_6;
    QGridLayout *identityLayout;
    QLabel *label;
    QLabel *website;
    QLabel *label_3;
    QLabel *owner;
    QLabel *label_5;
    QLabel *verifiedby;
    QGroupBox *privhistory;
    QGridLayout *gridLayout_7;
    QLabel *label_2;
    QLabel *visitsToSite;
    QLabel *label_4;
    QLabel *storingCookies;
    QLabel *label_6;
    QLabel *savedPasswords;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_8;
    QLabel *encryptedConnection;
    QPushButton *viewCertificate;
    QLabel *labelInfo;
    QSpacerItem *verticalSpacer;
    QWidget *source;
    QGridLayout *gridLayout_5;
    QGroupBox *sourceBox;
    QGridLayout *gridLayout_4;
    QTextBrowser *htmlSource;
};

#endif