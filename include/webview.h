/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef WEBVIEW_H
#define WEBVIEW_H

#include <QWebView>
#include <QFile>
#include <QNetworkReply>
#include <QByteArray>
#include <QTextStream>
#include <QWidget>
#include <QWebFrame>
#include <QSslConfiguration>
#include <QMessageBox>
#include <QGraphicsWebView>

#include "networkaccessmanager.h"
#include "tabwidget.h"
#include "downloads.h"
#include "historymanager.h"
#include "zonemanager.h"
#include "pageinfo.h"

class TabWidget;
class PageInfo;

class WebPage : public QWebPage
{
    Q_OBJECT
public:
    WebPage(QObject *parent);
    void setZone(ZoneManager::Zone zone) {m_zone = zone; }
protected slots:
    void handleUnsupportedContent(QNetworkReply *);
protected:
    QString userAgentForUrl(const QUrl&) const;
    void javaScriptAlert(QWebFrame *frame, const QString &msg);
    bool acceptNavigationRequest(QWebFrame *frame, const QNetworkRequest &request, NavigationType type);
private:
    void handleErrorPage(QNetworkReply *);
    ZoneManager::Zone m_zone;
    int jsAlertCount;
};

class WebView : public QWebView
{
	Q_OBJECT

public:
    enum EncryptionStatus { NoEncryption, SSL3, TLS1 };
    WebView(TabWidget*, int, bool);
    ~WebView();
    int tabIndex();
    void setTabIndex(int);
    bool loading();
    void printDocument();
    void printPreview();
    bool isSecure();
    QString doctype() { return m_doctype; }
    ZoneManager::Zone zone() { return m_zone; }
    QSslConfiguration sslConfiguration();
    TabWidget *m_parent;
public slots:
    void showPageInfo();
    void showPageSource();
signals:
    void locationChanged(int, const QUrl);
    void loadStartedOnTab(int);
    void loadProgressOnTab(int, int);
    void loadFinishedOnTab(int, bool);
    void urlClickedOnTab(int, const QUrl&);
    void linkHoveredOnTab(int, QString, QString);
    void switchToTab(int);
    void iconChangedOnTab(int);
    void titleChangedOnTab(int, QString);
    void download(DownloadItem *);
protected:
    QWebView *createWindow(QWebPage::WebWindowType);
    void contextMenuEvent(QContextMenuEvent *event);
    void handleErrorPage(QNetworkReply *);
protected slots:
    void loadStarted();
    void urlChanged2(const QUrl);
    void loadProgress(int);
    void loadFinished(bool);
    void linkClicked(const QUrl&);
    void linkHovered(QString, QString, QString);
    void downloadRequested(const QNetworkRequest);
    void titleChanged(QString);
    void unsupportedContent(QNetworkReply *);
    void gainFocus();
    void iconChanged();
    void pageRequested(QPrinter*);
    void sslErrors(QNetworkReply*,QList<QSslError>);
    void finished(QNetworkReply*);
private:
    void setupContextMenu();
    int m_tabIndex;
    bool m_loading;
    bool m_isSecure;
    bool m_private;
    QSslConfiguration m_sslConfig;
    QPrintPreviewDialog *m_preview;
    QStringList m_acceptSsl;
    QMenu *contextMenu;
    QMenu *imageContextMenu;
    QMenu *textContextMenu;
    QMenu *linkContextMenu;
    QAction *viewSource;
    QAction *pageInfo;
    QAction *bookmarkPage;
    QAction *searchText;
    QString m_doctype;
    WebPage *m_page;
    NetworkAccessManager *m_network;
    ZoneManager::Zone m_zone;
    PageInfo *m_pageInfo;
};

#endif // WEBVIEW_H
