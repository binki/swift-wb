/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QList>
#include <QNetworkCookie>
#include <QStringList>
#include <QVariant>
#include <QUrl>
#include <QList>

#include "searchbox.h"
#include "zonemanager.h"
#include "desktopservices.h"
#include "cookiejar.h"
#include "historymanager.h"

class Settings : public QSettings
{
	Q_OBJECT

public:
	Settings(QObject *parent);
	~Settings();
    void setToolbarShown(bool show);
    void setStatusBarShown(bool show);
    void setBookmarkBarShown(bool show);
    void setMenuBarShown(bool show);
    bool toolbarShown();
    bool statusBarShown();
    bool bookmarkBarShown();
    bool menuBarShown();
    bool alwaysShowTabs();
    static void appendSslException(QString host);
    static bool hasSslException(QString host);
    static bool zonePreference(ZoneManager::Zone zone, QString preference);
    static bool useProxy();
    static proxy_t networkProxy();
    static bool hasCookies(QUrl website);
    //void addSearchEngine(search_t search);
private:
	
};

#endif // SETTINGS_H
