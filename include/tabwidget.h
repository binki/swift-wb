/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QTabWidget>
#include <QtGui>
#include <QDir>
#include "webview.h"
#include "downloads.h"

class WebView;

class TabBar : public QTabBar
{
    Q_OBJECT
public:
    TabBar(QWidget *parent);
    ~TabBar();
signals:
    void newTabRequested();
    void closeTabById(int tabID);
    void tabReloadRequested(int);
    void allReloadRequested();
    void closeRightRequested(int);
    void closeLeftRequested(int);
    void closeAllTabsButID(int);
protected slots:
    void closeTabRequested();
    void reloadTabSlot();
    void closeAllTabsSlot();
    void closeToLeft();
    void closeToRight();
protected:
    void contextMenuEvent(QContextMenuEvent * event);
private:
    void createContextMenu();
    QMenu *tabContextMenu;
    QAction *newTab;
    QAction *closeTab;
    QAction *closeAllTabs;
    QAction *closeTabsOnRight;
    QAction *closeTabsOnLeft;
    QAction *reloadTab;
    QAction *reloadAllTabs;
    QPoint m_lastUsedPoint;
};

class TabWidget : public QTabWidget
{
	Q_OBJECT

public:
    TabWidget(QWidget *parent);
    ~TabWidget();
    WebView* newTab(bool);
    WebView* newTab(QUrl, bool);
    WebView* currentWebView();
    WebView* webView(int);
    void showDownloadsWindow();
    void closeTab(int index);
signals:
    void updateLocation(const QUrl);
    void updateActions();
    void loadStarted();
    void loadProgress(int);
    void urlClicked(const QUrl&);
    void loadFinished(bool);
    void linkHovered(QString, QString);
    void raiseParent();
    void updateIcon();
    void titleChanged(QString);
    void trashedTab(QString, QUrl);
protected:
    void tabRemoved(int);
protected slots:
    void newTabRequested();
    void tabCloseRequested(int);
    void tabReloadRequested(int);
    void allReloadRequested();
    void closeRightRequested(int);
    void closeLeftRequested(int);
    void closeAllButID(int);
    void linkHoveredOnTab(int, QString, QString);
    void locationChanged(int, const QUrl);
    void loadStartedOnTab(int);
    void goBack();
    void goForward();
    void stop();
    void reload();
    void loadProgressOnTab(int, int);
    void loadFinishedOnTab(int, bool);
    void urlClickedOnTab(int, const QUrl);
    void switchToTab(int);
    void download(DownloadItem *);
    void iconChangedOnTab(int);
    void tabMoved(int, int);
    void titleChangedOnTab(int, QString);
private:
    void checkActions();
    void reorderWebViewTabID();
    DownloadsWindow *dl;
    TabBar *b;
};

#endif // TABWIDGET_H
