/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef LOCATIONBAR_H
#define LOCATIONBAR_H

#include <QComboBox>
#include <QLineEdit>
#include <QUrl>
#include <QWebSettings>
#include <QSslConfiguration>
#include <QList>

#include "searchbox.h"

struct urlitem_
{
    QString title;
    QUrl url;
    bool isBookmark;
};

typedef struct urlitem_ urlitem_t;

class AwesomeBarItem : public QWidget
{
    Q_OBJECT

public:
    AwesomeBarItem(QWidget *parent, urlitem_t url, int position);
    QUrl url() { return m_url.url; }
    QString title() { return m_url.title; }
signals:
    void hilighted(int);
protected:
    void mouseMoveEvent(QMouseEvent *event);
private:
    QGridLayout *gridLayout;
    QHBoxLayout *mainLayout;
    QVBoxLayout *pageIconLayout;
    QLabel *pageIcon;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *textLayout;
    QLabel *titleLabel;
    QLabel *urlLabel;
    QVBoxLayout *bookmarkLayout;
    QLabel *bookmarkIcon;
    QSpacerItem *verticalSpacer_2;
    urlitem_t m_url;
    int position;
};

class AwesomeBarView : public QTableWidget
{
    Q_OBJECT
public:
    AwesomeBarView(QWidget *parent);
    void addItem(int position, AwesomeBarItem *item);
    void doShow();
signals:
    void urlActivated(QUrl);
protected slots:
    void hilightedItem(int);
protected:
    void leaveEvent(QEvent *event);
    void mousePressEvent(QMouseEvent *event);
private:
    void clearObjects();
    QSize m_myOrigSize;
    int m_itemHeight;
};

class LocationBar : public QWidget
{
	Q_OBJECT
   
friend class AwesomeBarView;

public:
    LocationBar(QWidget *parent);
    ~LocationBar();
    QLineEdit *lineEdit() { return m_lineEdit->lineEdit(); }
    void setText(QString);
    void setIcon(QIcon);
    void setSecure(QSslConfiguration);
    void setInsecure();
    void saveSplitterRect();
signals:
    void loadUrl(QUrl);
    void loadSearch(QUrl);
protected slots:
    void returnPressed();
    void navigateToSearch(QUrl);
    void activateSearchBox();
    void activatetLocation();
    void textEdited(const QString &text);
    void searchTimeout();
private:
    void createAndPopulateTableView(QString search);
    void removeTableView();
    QList<urlitem_t> completeString(QString search);
    AwesomeBarView *m_tableWidget;
    QComboBox *m_lineEdit;
    QLineEdit *m_edit;
    QSplitter *m_splitter;
    QHBoxLayout *m_layout;
    QToolButton *m_pageActions;
    QString m_lastTypedItem;
    QTimer *timer;
    SearchBox *m_searchBox;
    bool shownView;
};

#endif // LOCATIONBAR_H
