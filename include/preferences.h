/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QtGui>
#include <QPixmap>
#include "settings.h"
#include "zonemanager.h"

class PreferencesWindow : public QDialog
{
    Q_OBJECT
public:
    PreferencesWindow(QWidget *parent);
protected slots:
    void accept();
    void zoneIndexChanged(int);
    void addRemoveZone();
private:
    void saveZoneSettings(ZoneManager::Zone zone);
    void loadZoneSettings(ZoneManager::Zone zone);
    void saveSettings();
    void loadSettings();
    void retranslate();
    Settings *settings;
    QGridLayout *gridLayout_6;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab_general;
    QGridLayout *gridLayout_13;
    QGroupBox *groupBoxStartup;
    QGridLayout *gridLayout;
    QLabel *label_Startup;
    QComboBox *onStartup;
    QLabel *label_Homepage;
    QLineEdit *homePage;
    QPushButton *setToCurrentPage;
    QSpacerItem *horizontalSpacer_2;
    QGroupBox *downloadsGB;
    QGridLayout *gridLayout_12;
    QCheckBox *showDownloads;
    QCheckBox *hideDownloads;
    QRadioButton *alwaysAsk;
    QHBoxLayout *horizontalLayout;
    QRadioButton *saveFiles;
    QComboBox *saveLocation;
    QSpacerItem *verticalSpacer_4;
    QWidget *tabs;
    QGridLayout *gridLayout_11;
    QGroupBox *groupBoxTabs;
    QGridLayout *gridLayout_3;
    QCheckBox *openNewTab;
    QCheckBox *confirmClose;
    QCheckBox *alwaysShowTabs;
    QSpacerItem *verticalSpacer;
    QWidget *content;
    QGridLayout *gridLayout_10;
    QGroupBox *fontsGB;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *labelFixedFont;
    QFontComboBox *fixedFont;
    QLabel *labelFixedSize;
    QSpinBox *fixedSize;
    QHBoxLayout *horizontalLayout_3;
    QLabel *labelDefault;
    QFontComboBox *standardFont;
    QLabel *labelStdSize;
    QSpinBox *standardSize;
    QGroupBox *useCustomStylesheet;
    QGridLayout *gridLayout_9;
    QLineEdit *userStylesheetPath;
    QPushButton *browseStylesheet;
    QSpacerItem *verticalSpacer_2;
    QWidget *zonestab;
    QGridLayout *gridLayout_5;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelChange;
    QComboBox *zones;
    QTreeWidget *zoneSettings;
    QSpacerItem *horizontalSpacer;
    QPushButton *addRemove;
    QPushButton *zoneHelp;
    QWidget *advanced;
    QGridLayout *gridLayout_8;
    QGroupBox *proxyGroup;
    QGridLayout *gridLayout_4;
    QComboBox *proxy;
    QSpacerItem *horizontalSpacer_4;
    QLabel *labelType;
    QComboBox *proxyType;
    QLabel *labelHostname;
    QLineEdit *proxyHost;
    QLabel *labelPort;
    QSpinBox *proxyPort;
    QLabel *labelUName;
    QLineEdit *proxyUser;
    QLabel *labelPass;
    QLineEdit *proxyPass;
    QGroupBox *additionalSettingsGB;
    QGridLayout *gridLayout_7;
    QLabel *labelMaxCache;
    QSpinBox *spinBox;
    QSpacerItem *horizontalSpacer_3;
    QCheckBox *defaultBrowser;
    QSpacerItem *verticalSpacer_3;
    QDialogButtonBox *buttonBox;
    ZoneManager::Zone m_selectedZone;
};

#endif // PREFERENCES_H
