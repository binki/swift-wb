/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef SEARCHBOX_H
#define SEARCHBOX_H

#include <QtGui>
#include <qdom.h>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "settings.h"

struct search_
{
    QString name;
    QString description;
    QByteArray icon;
    QString searchUrl;
    QString suggestionsUrl;
};

typedef struct search_ search_t;


class SearchBox : public QLineEdit
{
    Q_OBJECT

public:
    SearchBox(QWidget *parent);
    ~SearchBox();
    QToolButton *engineMenu();
    QToolButton *tbIcon;
signals:
    void navigateToSearch(QUrl);
protected slots:
    void selectionChanged();
    void returnPressed();
    void menuTriggered(QAction*);
protected:
    void focusInEvent(QFocusEvent*);
    void focusOutEvent(QFocusEvent*);
private:
    void loadSearchEngines();
    void addSearchEngine();
    void referenceCacheForImg();
    void downloadImgToCache();
    void changeEngine(search_t engine);
    QList<search_t> engines;
    QMenu *engMenu;
    int uset;
    search_t current;
};

#endif // SEARCHBOX_H
