/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#ifndef DOWNLOADS_H
#define DOWNLOADS_H

#include <QtGui>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QFileDialog>
#include <QDesktopServices>

class DownloadItem : public QWidget
{
    Q_OBJECT
public:
    DownloadItem(QWidget *parent, QNetworkRequest request);
    bool prompt();
protected slots:
    void downloadProgress(qint64, qint64);
    void finished();
private:
    bool saveDownload();
    void openDownload();
    QString figureOutFileName();
    void retranslate();
private:
    QHBoxLayout *mainLayout;
    QVBoxLayout *iconLayout;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *informationLayout;
    QHBoxLayout *fileNameLayout;
    QLabel *m_fileName;
    QSpacerItem *fileNameSpacer;
    QLabel *m_time;
    QHBoxLayout *hProgressLayout;
    QProgressBar *m_progress;
    QToolButton *m_stop;
    QToolButton *m_pause;
    QLabel *m_information;
    QLabel *m_icon;
    QNetworkRequest m_request;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
    bool m_active;
    int role;
    QString filename;
};

class DownloadsWindow : public QDialog
{
    Q_OBJECT
public:
    DownloadsWindow(QWidget *parent);
    void addItem(DownloadItem *);
protected slots:
    void clearItems();
private:
    QVBoxLayout *layout;
    QTableWidget *downloadTable;
    QDialogButtonBox *buttonBox;
};

#endif // DOWNLOADS_H
