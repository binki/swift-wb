/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "zonemanager.h"

void ZoneManager::checkForZoneDatabaseTable()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.exec(QString("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'zonecontents';"));
    if(!query.first())
    {
        query.exec(QString("CREATE TABLE [zonecontents] ([ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, [URL] TEXT  NOT NULL, [Zone] INTEGER DEFAULT '0' NULL)"));
        query.exec(QString("INSERT INTO `zonecontents` (`URL`, `Zone`) VALUES ('%.youtube.com', 1);"));
        query.exec(QString("INSERT INTO `zonecontents` (`URL`, `Zone`) VALUES ('www.swift.ws', 1);"));
    }

    db.close();
}

QString ZoneManager::addWebSite(QString website, ZoneManager::Zone zone)
{
    if(website == "")
        return "";

    QString host = website;
    host.replace('*', '%');
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT `Zone` FROM `zonecontents` WHERE `URL` = :url");
    query.bindValue(":url", website);
    query.exec();
    db.close();

    if(!query.first())
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
        db.open();
        QSqlQuery query(db);
        query.prepare("INSERT INTO `zonecontents` (`URL`, `Zone`) VALUES (:url, :zone);");
        query.bindValue(":url", host);
        query.bindValue(":zone", zone);
        if(!query.exec())
        {
            db.close();
            return QString("FAILED!!!");
        }
    }
    return host.replace("%", "*");
}

void ZoneManager::removeWebSite(QString site)
{
    site.replace('*', '%');
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM `zonecontents` WHERE `URL` = :url;");
    query.bindValue(":url", site);
    query.exec();
    db.close();
}

ZoneManager::Zone ZoneManager::webSiteZone(QUrl website)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    QString host = website.host();
    query.prepare("SELECT `Zone` FROM `zonecontents` WHERE LIKE(`URL`, :url);");
    query.bindValue(":url", host);
    ZoneManager::Zone zone = ZoneManager::Internet;
    if(query.exec())
    {
        int mzone = query.value(0).toInt();
        switch(mzone)
        {
        case 0:
            zone = ZoneManager::Internet;
            break;
        case 1:
            zone = ZoneManager::Trusted;
            break;
        case 2:
            zone = ZoneManager::Restricted;
            break;
        }
    }
    db.close();

    return zone;
}

QStringList ZoneManager::sitesForZone(ZoneManager::Zone zone)
{
    QStringList list;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT `URL` FROM `zonecontents` WHERE `Zone` = :zone");
    switch(zone) {
        case ZoneManager::Trusted:
            query.bindValue(":zone", 1);
            break;
        case ZoneManager::Restricted:
            query.bindValue(":zone", 2);
            break;
    }
    query.exec();
    while (query.next()) {
        list.append(query.value(0).toString().replace('%', '*'));
    }

    db.close();

    return list;
}

ZoneLabel::ZoneLabel(QWidget *parent)
	: QWidget(parent)
{	
    horizontalLayout = new QHBoxLayout(this);
    horizontalLayout->setContentsMargins(0, 0, 0, 0);

    icon = new QLabel(this);
    icon->setText(QString::fromUtf8(""));

    horizontalLayout->addWidget(icon);

    zoneText = new QLabel(this);
    horizontalLayout->addWidget(zoneText);
    setZone(ZoneManager::Internet);
}

ZoneLabel::~ZoneLabel()
{

}

void ZoneLabel::setZone(ZoneManager::Zone zone)
{
    switch(zone)
    {
        case ZoneManager::Internet:
            icon->setPixmap(QWebSettings::webGraphic(QWebSettings::DefaultFrameIconGraphic));
            zoneText->setText(tr("Internet   "));
            break;
        case ZoneManager::Trusted:
            icon->setPixmap(QPixmap(QString::fromUtf8(":/Swift/trusted")));
            zoneText->setText(tr("Trusted Sites    "));
            break;
        case ZoneManager::Restricted:
            icon->setPixmap(QPixmap(QString::fromUtf8(":/Swift/restricted")));
            zoneText->setText(tr("Restricted Sites    "));
            break;
    }
}

EditZoneDialog::EditZoneDialog(QWidget *parent = 0, ZoneManager::Zone zone = ZoneManager::Internet, QString inputURL = QString(""))
    :QDialog(parent),
    m_zone(zone)
{
    setWindowModality(Qt::ApplicationModal);
    resize(375, 297);
    setModal(true);
    gridLayout = new QGridLayout(this);
    m_pixmap = new QLabel(this);
    switch(zone)
    {
    case ZoneManager::Trusted:
        setWindowIcon(QIcon(QString::fromUtf8(":/Swift/trusted")));
        m_pixmap->setPixmap(QPixmap(QString::fromUtf8(":/Swift/trusted32")));
        setWindowTitle(tr("Trusted Websites"));
        break;
    case ZoneManager::Restricted:
        setWindowIcon(QIcon(QString::fromUtf8(":/Swift/restricted")));
        m_pixmap->setPixmap(QPixmap(QString::fromUtf8(":/Swift/restricted32")));
        setWindowTitle(tr("Restricted Websites"));
        break;
    };

    m_pixmap->setText(QString());
    m_pixmap->setMaximumWidth(35);

    descriptionLabel = new QLabel(this);
    descriptionLabel->setScaledContents(false);
    descriptionLabel->setWordWrap(true);
    descriptionLabel->setText(tr("You can add and remove websites from this zone.  All the websites within this zone will use the settings defined in the Preferences dialog."));

    line = new QFrame(this);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    addWebSiteLabel = new QLabel(this);
    addWebSiteLabel->setText(tr("Add this website to this zone:"));
    
    m_urlEdit = new QLineEdit(this);
    m_urlEdit->setText(inputURL);

    m_add = new QPushButton(this);
    m_add->setMaximumSize(QSize(90, 16777215));
    m_add->setText(tr("Add"));

    connect(m_add, SIGNAL(pressed()), this, SLOT(addWebSite()));

    websites = new QLabel(this);
    websites->setText(tr("Websites:"));
    
    m_websites = new QListWidget(this);
    QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(m_websites->sizePolicy().hasHeightForWidth());
    m_websites->setSizePolicy(sizePolicy1);
    m_websites->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(m_websites, SIGNAL(currentRowChanged(int)), this, SLOT(currentRowChanged(int)));

    m_remove = new QPushButton(this);
    m_remove->setObjectName(QString::fromUtf8("m_remove"));
    m_remove->setMaximumSize(QSize(90, 16777215));
    m_remove->setText(tr("Remove"));
    m_remove->setEnabled(false);
    connect(m_remove, SIGNAL(pressed()), this, SLOT(removeWebSite()));

    verticalSpacer = new QSpacerItem(20, 97, QSizePolicy::Minimum, QSizePolicy::Expanding);

    m_buttonBox = new QDialogButtonBox(this);
    m_buttonBox->setObjectName(QString::fromUtf8("m_buttonBox"));
    m_buttonBox->setOrientation(Qt::Horizontal);
    m_buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    gridLayout->addWidget(m_pixmap, 0, 0, 1, 1);
    gridLayout->addWidget(descriptionLabel, 0, 1, 1, 2);
    gridLayout->addWidget(line, 1, 0, 1, 3);
    gridLayout->addWidget(addWebSiteLabel, 2, 0, 1, 2);
    gridLayout->addWidget(m_urlEdit, 3, 0, 1, 2);
    gridLayout->addWidget(m_add, 3, 2, 1, 1);
    gridLayout->addWidget(websites, 4, 0, 1, 1);
    gridLayout->addWidget(m_websites, 5, 0, 2, 2);
    gridLayout->addWidget(m_remove, 5, 2, 1, 1);
    gridLayout->addItem(verticalSpacer, 6, 2, 1, 1);
    gridLayout->addWidget(m_buttonBox, 7, 0, 1, 3);

    QObject::connect(m_buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(m_buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    m_websites->addItems(ZoneManager::sitesForZone(zone));
}

void EditZoneDialog::addWebSite()
{
    QString site = m_urlEdit->text();
    QString result = ZoneManager::addWebSite(site, m_zone);
    if(result == QString("FAILED!!!"))
    {
        QMessageBox msg(QMessageBox::Information, tr("Invalid Input"), tr("The website you entered is invalid."), QMessageBox::Ok, this);
        msg.exec();
    }
    else
    {
        if(result != "")
        {
            m_websites->addItem(result);
            m_urlEdit->clear();
        }
    }
}

void EditZoneDialog::removeWebSite()
{
    QString itemToDelete = m_websites->selectedItems()[0]->text();
    QMessageBox msg(QMessageBox::Question, tr("Delete this site?"), tr("Are you sure you want to remove the website ") + itemToDelete + tr("from this zone?"), QMessageBox::Yes | QMessageBox::No, this);    
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        ZoneManager::removeWebSite(itemToDelete);
        m_websites->takeItem(m_websites->row(m_websites->selectedItems()[0]));
    }
}

void EditZoneDialog::currentRowChanged(int row)
{
    if(row == -1 || m_websites->selectedItems().count() == 0)
        m_remove->setEnabled(false);
    else
        m_remove->setEnabled(true);
}
