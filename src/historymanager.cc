/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "historymanager.h"

bool checkForHistoryDatabaseExistance()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.exec(QString("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'history';"));
    if(!query.first())
    {
        query.exec(QString("CREATE TABLE [history] (AccessCount INTEGER, Title TEXT, URL TEXT, Timestamp INTEGER)"));
        db.close();
        return false;
    }

    db.close();
    return true;
}

void addItemToHistory(QUrl url, QString title)
{
    if(pageAccesses(url) > 0)
    {
        incrementAccessCount(url);
        return;
    }

    uint timestamp = QDateTime::currentDateTime().toTime_t();
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("INSERT INTO `history` (`Title`, `URL`, `AccessCount`, `Timestamp`) VALUES (:title, :url, 1, :ts);");
    query.bindValue(":title", title);
    query.bindValue(":url", url.toString());
    query.bindValue(":ts", timestamp);
    query.exec();

    db.close();
}

unsigned int pageAccesses(QUrl url)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT `AccessCount` FROM `history` WHERE `URL` = :url");
    query.bindValue(":url", url.toString());
    query.exec();
    query.next();
    unsigned int accessCount = query.value(0).toInt();
    db.close();

    return accessCount;
}

void incrementAccessCount(QUrl url)
{
    unsigned int lastAccess = pageAccesses(url);
    lastAccess++;

    uint timestamp = QDateTime::currentDateTime().toTime_t();
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("UPDATE `history` SET `AccessCount` = :access, `Timestamp` = :ts WHERE `URL` = :url");
    query.bindValue(":access", lastAccess);
    query.bindValue(":url", url.toString());
    query.bindValue(":ts", timestamp);
    query.exec();

    db.close();
}

QList<historyitem_t> lastAccessedItems()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT `Title`, `URL` from `history` ORDER BY `Timestamp` DESC LIMIT 10");
    query.exec();
    QList<historyitem_t> ret;
    while(query.next())
    {
        historyitem_t item;
        item.title = query.value(0).toString();
        item.url = QUrl(query.value(1).toString());
        ret.append(item);
    }

    return ret;
}
