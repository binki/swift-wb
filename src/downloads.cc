/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "downloads.h"

DownloadsWindow::DownloadsWindow(QWidget *parent = 0)
        : QDialog(parent)
{
    resize(400, 300);
    layout = new QVBoxLayout(this);
    layout->setObjectName(QString::fromUtf8("layout"));
    layout->setContentsMargins(0, 0, 0, 0);
    downloadTable = new QTableWidget(1, 1, this);
    downloadTable->setObjectName(QString::fromUtf8("downloadTable"));
    layout->addWidget(downloadTable);
    buttonBox = new QDialogButtonBox(this);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Close|QDialogButtonBox::Reset);
    layout->addWidget(buttonBox);
    QObject::connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(buttonBox, SIGNAL(clicked(QAbstractButton*)), this, SLOT(clearItems()));
    setWindowTitle(tr("Downloads"));
    downloadTable->hideRow(downloadTable->indexAt(QPoint(10,10)).row());
    downloadTable->setGridStyle(Qt::SolidLine);
    downloadTable->setShowGrid(true);
    downloadTable->verticalHeader()->hide();
    downloadTable->horizontalHeader()->hide();
    downloadTable->setAlternatingRowColors(true);
    downloadTable->horizontalHeader()->setStretchLastSection(true);
    downloadTable->setSelectionMode(QAbstractItemView::SingleSelection);
}

void DownloadsWindow::addItem(DownloadItem *item)
{
    downloadTable->insertRow(0);
    QModelIndex idx = downloadTable->model()->index(0, 0);
    downloadTable->setIndexWidget(idx, item);
    downloadTable->resizeRowToContents(0);
}

void DownloadsWindow::clearItems()
{

}


DownloadItem::DownloadItem(QWidget *parent, QNetworkRequest request)
        : QWidget(parent)
{
    this->setGeometry(0, 0, 543, 94);
    mainLayout = new QHBoxLayout(this);
    mainLayout->setObjectName(QString::fromUtf8("mainLayout"));
    mainLayout->setContentsMargins(0, 0, 0, 0);
    iconLayout = new QVBoxLayout();
    iconLayout->setObjectName(QString::fromUtf8("iconLayout"));
    m_icon = new QLabel(this);
    m_icon->setObjectName(QString::fromUtf8("m_icon"));
    m_icon->setMaximumSize(QSize(48, 48));
    m_icon->setPixmap(QPixmap(QString::fromUtf8(":/Swift/swift-logo")));
    m_icon->setScaledContents(true);
    iconLayout->addWidget(m_icon);
    verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
    iconLayout->addItem(verticalSpacer);
    mainLayout->addLayout(iconLayout);
    informationLayout = new QVBoxLayout();
    informationLayout->setSpacing(4);
    informationLayout->setObjectName(QString::fromUtf8("informationLayout"));
    fileNameLayout = new QHBoxLayout();
    fileNameLayout->setObjectName(QString::fromUtf8("fileNameLayout"));
    m_fileName = new QLabel(this);
    m_fileName->setObjectName(QString::fromUtf8("m_fileName"));
    QFont font;
    font.setPointSize(14);
    m_fileName->setFont(font);
    fileNameLayout->addWidget(m_fileName);
    fileNameSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    fileNameLayout->addItem(fileNameSpacer);
    m_time = new QLabel(this);
    m_time->setObjectName(QString::fromUtf8("m_time"));
    fileNameLayout->addWidget(m_time);
    informationLayout->addLayout(fileNameLayout);
    hProgressLayout = new QHBoxLayout();
    hProgressLayout->setObjectName(QString::fromUtf8("hProgressLayout"));
    m_progress = new QProgressBar(this);
    m_progress->setObjectName(QString::fromUtf8("m_progress"));
    m_progress->setEnabled(true);
    m_progress->setValue(24);
    m_progress->setTextVisible(false);
    m_progress->setInvertedAppearance(false);
    hProgressLayout->addWidget(m_progress);
    m_stop = new QToolButton(this);
    m_stop->setObjectName(QString::fromUtf8("m_stop"));
    /* proper icon here */
    m_stop->setIcon(
        style()->standardIcon(QStyle::SP_BrowserStop));
    m_stop->setAutoRaise(true);
    hProgressLayout->addWidget(m_stop);
    m_pause = new QToolButton(this);
    m_pause->setObjectName(QString::fromUtf8("m_pause"));
    m_pause->setAutoRaise(true);
    hProgressLayout->addWidget(m_pause);
    informationLayout->addLayout(hProgressLayout);
    m_information = new QLabel(this);
    m_information->setObjectName(QString::fromUtf8("m_information"));
    informationLayout->addWidget(m_information);
    mainLayout->addLayout(informationLayout);

    m_request = request;

    retranslate();
}

bool DownloadItem::prompt()
{
    QMessageBox msg(QMessageBox::Question, "File Download",
        QString("You are about to download %1.  Would you like to open this file or save it?").arg(m_request.url().toString()),
        QMessageBox::Open|QMessageBox::Save|QMessageBox::Cancel, this);

    int i = msg.exec();
    role = i;

    if(i == QMessageBox::Cancel)
        return false;

    if(i == QMessageBox::Save) {
        bool ret = saveDownload();
        return ret;
    } else if(i == QMessageBox::Open) {
        openDownload();
        return true;
    } else {
        return false;
    }
}

bool DownloadItem::saveDownload()
{
    QString fileName = m_request.url().path().split("/").last();
    filename = QFileDialog::getSaveFileName(this, tr("Save Download"), filename);

    if(filename == QString(""))
        return false;

    manager = new QNetworkAccessManager(this);
    reply = manager->get(m_request);
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64, qint64)));
    connect(reply, SIGNAL(finished()), this, SLOT(finished()));

    const QFileInfo f_info(filename);
    m_fileName->setText(f_info.fileName());
    QFileIconProvider provider;
    QIcon icon(provider.icon(f_info));
    m_icon->setPixmap(icon.pixmap(48, 48, QIcon::Normal, QIcon::Off));
    m_information->setText(tr("Unknown Size from ") + m_request.url().host());

    return true;
}

void DownloadItem::openDownload()
{
    filename = QDesktopServices::storageLocation(QDesktopServices::TempLocation) + "/" + figureOutFileName();    
    manager = new QNetworkAccessManager(this);
    reply = manager->get(m_request);
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64, qint64)));
    connect(reply, SIGNAL(finished()), this, SLOT(finished()));
    const QFileInfo f_info(filename);
    m_fileName->setText(f_info.fileName());
    m_information->setText(tr("Unknown Size from ") + m_request.url().host());
}

QString DownloadItem::figureOutFileName()
{
    if(m_request.url().path().contains("/")) {
        QStringList list = m_request.url().path().split("/");
        if(list.last() == QString(""))
        {
            return "index.html";
        }
        else if(!list.last().contains("."))
        {
            return list.last() + QString(".html");
        }
        else
        {
            return list.last();
        }
    }

    return m_request.url().path();
}

void DownloadItem::retranslate()
{
    m_icon->setPixmap(QPixmap(":/Swift/swift-logo"));
    m_icon->setText(QString());
    m_fileName->setText(tr("swift.elf"));
    m_time->setText(QString());
    m_pause->setText(tr("||"));
    m_information->setText(tr("6.7MB - www.swift.ws"));
}

void DownloadItem::downloadProgress(qint64 recv, qint64 total)
{
    m_information->setText(QString("%1 KB of %2 KB from: %3").arg(
            QString::number(recv), QString::number(total), reply->url().host()));
    m_progress->setMaximum(total);
    m_progress->setValue(recv);
}

void DownloadItem::finished()
{
    m_information->setText(QString("%1 KB from: %2").arg(
            QString::number(reply->size()), reply->url().host()));
    m_progress->setVisible(false);
    m_stop->setVisible(false);
    m_pause->setVisible(false);
    m_active = false;

    QFile file(filename);
    file.open(QFile::ReadWrite);
    file.write(reply->readAll());
    file.close();

    if(role == QMessageBox::Open)
    {
        QDesktopServices::openUrl(QUrl("file://" + filename, QUrl::TolerantMode));
    }
}
