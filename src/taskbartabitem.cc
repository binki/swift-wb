/************************************

swift web browser
Copyright (C) 2006-2009 Chris Fuenty <zimmy@zimmy.co.uk>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

************************************/

#include "taskbartabitem.h"

TaskbarTabItem::TaskbarTabItem()
	: QWidget(0)
{
	setMaximumHeight(1);
	setMaximumWidth(1);
        setWindowFlags(Qt::Window|Qt::WindowStaysOnBottomHint|Qt::CustomizeWindowHint);
#ifdef _WIN32
        ShowWindow(winId(), SW_SHOWMINNOACTIVE);
#else
        show();
#endif
	setFocusPolicy(Qt::NoFocus);
}

TaskbarTabItem::~TaskbarTabItem()
{

}

void TaskbarTabItem::changeEvent(QEvent *cEvent)
{
	clearFocus();
	if(cEvent->type() == QEvent::ActivationChange)
	{
		emit gainFocus();
		setWindowState(Qt::WindowMaximized);
	}
}
