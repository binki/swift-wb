/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "webview.h"

WebPage::WebPage(QObject *parent = 0)
    : QWebPage(parent)
{
    connect(this, SIGNAL(unsupportedContent(QNetworkReply *)), 
        this, SLOT(handleUnsupportedContent(QNetworkReply *)));
}

QString WebPage::userAgentForUrl(const QUrl &url) const
{
    return QWebPage::userAgentForUrl(url);
    //return QString("Mozilla/5.0 (%Platform%; %Security%; %Subplatform%; %Locale%) AppleWebKit/%WebKitVersion% (KHTML, like Gecko) %AppVersion%");
}

bool WebPage::acceptNavigationRequest(QWebFrame *frame, const QNetworkRequest &request, NavigationType type)
{
    return true;
}

void WebPage::javaScriptAlert(QWebFrame *frame, const QString &msg)
{
    Q_UNUSED(frame);
    if(Settings::zonePreference(m_zone, "AllowExcessiveJSMessages"))
    {
        if(jsAlertCount < 8)
        {
            QMessageBox message(QMessageBox::Information, mainFrame()->title(), msg, QMessageBox::Ok);
            message.show();
            jsAlertCount++;
        }
    }
}

void WebPage::handleUnsupportedContent(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NoError)
        emit downloadRequested(reply->request());
    else
        handleErrorPage(reply);
}

void WebPage::handleErrorPage(QNetworkReply *reply)
{
    QString html;
    QFile *file;
    QTextStream *in;

    switch(reply->error()) {
        case QNetworkReply::HostNotFoundError:
            file = new QFile(":/ErrorPages/dnserror");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            html.replace("{HOSTNAME}", reply->url().host());
            file->close();
            break;
        case QNetworkReply::TimeoutError:
            file = new QFile(":/ErrorPages/timeouterror");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            break;
        case QNetworkReply::OperationCanceledError:
            file = new QFile(":/ErrorPages/opcanceled");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            break;
    case QNetworkReply::ConnectionRefusedError:
            file = new QFile(":/ErrorPages/connectionrefused");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            break;
    case QNetworkReply::RemoteHostClosedError:
            file = new QFile(":/ErrorPages/remotehostclosed");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            break;
    case QNetworkReply::ProxyNotFoundError:
            file = new QFile(":/ErrorPages/proxy404");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            break;
    case QNetworkReply::ProxyConnectionRefusedError:
            file = new QFile(":/ErrorPages/proxyrefused");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            break;
    };

    mainFrame()->setHtml(html, reply->url());

    file = new QFile(":/ErrorPages/opcanceled");
    in = new QTextStream(file);

    delete file;
    delete in;
}

WebView::WebView(TabWidget *parent = 0, int TabID = 0, bool isPrivate = false)
	: QWebView(parent),
    m_parent(parent),
    m_page(new WebPage(this)),
    m_private(isPrivate),
    m_network(new NetworkAccessManager(this, isPrivate)),
    m_zone(ZoneManager::Internet),
    m_tabIndex(TabID)
{
    if(isPrivate == true)
        settings()->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);

    settings()->setAttribute(QWebSettings::PluginsEnabled, true);

    setRenderHints(QPainter::SmoothPixmapTransform|QPainter::Antialiasing);
    setPage(m_page);
    m_page->setNetworkAccessManager(m_network);
    setupContextMenu();
    settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    connect(this, SIGNAL(urlChanged(const QUrl)), this, SLOT(urlChanged2(const QUrl)));
    connect(this, SIGNAL(loadStarted()), this, SLOT(loadStarted()));
    connect(this, SIGNAL(loadFinished(bool)), this, SLOT(loadFinished(bool)));
    connect(this, SIGNAL(linkClicked(const QUrl)), this, SLOT(linkClicked(const QUrl)));
    connect(this, SIGNAL(iconChanged()), this, SLOT(iconChanged()));
    connect(this, SIGNAL(titleChanged(QString)), this, SLOT(titleChanged(QString)));
    connect(page(), SIGNAL(downloadRequested(QNetworkRequest)), this, SLOT(downloadRequested(QNetworkRequest)));
    connect(page(), SIGNAL(linkHovered(QString, QString, QString)),
            this, SLOT(linkHovered(QString, QString, QString)));
    connect(page(), SIGNAL(loadProgress(int)), this, SLOT(loadProgress(int)));
    connect(m_network, SIGNAL(finished(QNetworkReply*)), this, SLOT(finished(QNetworkReply*)));
    connect(m_network, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), this, SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));
    page()->setForwardUnsupportedContent(true);
}


WebView::~WebView()
{
    delete m_page;
}

void WebView::setTabIndex(int tabID)
{
    m_tabIndex = tabID;
}

int WebView::tabIndex()
{
    return m_tabIndex;
}

void WebView::urlChanged2(const QUrl url)
{
    emit locationChanged(m_tabIndex, url);
}

void WebView::loadStarted()
{
    m_isSecure = false;
    m_sslConfig = QSslConfiguration();
    m_loading = true;
    emit loadStartedOnTab(m_tabIndex);
}

void WebView::loadProgress(int progress)
{
    emit loadProgressOnTab(m_tabIndex, progress);
}

void WebView::loadFinished(bool ok)
{
    if(!settings()->testAttribute(QWebSettings::PrivateBrowsingEnabled))
        addItemToHistory(url(), title());

    m_loading = false;
    m_zone = ZoneManager::webSiteZone(url());
    emit loadFinishedOnTab(m_tabIndex, ok);
}

void WebView::linkClicked(const QUrl &url)
{
	emit urlClickedOnTab(m_tabIndex, url);
}

void WebView::linkHovered(QString link, QString title, QString textContent)
{
    Q_UNUSED(textContent);
    emit linkHoveredOnTab(m_tabIndex, link, title);
}

void WebView::titleChanged(QString title)
{
    emit titleChangedOnTab(m_tabIndex, title);
}

void WebView::gainFocus()
{
    emit switchToTab(m_tabIndex);
}

QWebView* WebView::createWindow(QWebPage::WebWindowType type)
{
    if(type == QWebPage::WebBrowserWindow)
    {
        bool m_private = settings()->testAttribute(QWebSettings::PrivateBrowsingEnabled);
        return qobject_cast<QWebView*>(m_parent->newTab(m_private));
    }
}

void WebView::downloadRequested(QNetworkRequest request)
{
    DownloadItem *item = new DownloadItem(this, request);
    if(item->prompt())
        emit download(item);
}

void WebView::handleErrorPage(QNetworkReply *reply)
{
    QString html;
    QFile *file;
    QTextStream *in;

    switch(reply->error()) {
        case QNetworkReply::HostNotFoundError:
            file = new QFile(":/ErrorPages/dnserror");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            html.replace("{HOSTNAME}", reply->url().host());
            file->close();
            setHtml(html, reply->url());
            break;
        case QNetworkReply::TimeoutError:
            file = new QFile(":/ErrorPages/timeouterror");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            setHtml(html, reply->url());
            break;
        case QNetworkReply::OperationCanceledError:
            file = new QFile(":/ErrorPages/opcanceled");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            setHtml(html, reply->url());
            break;
    case QNetworkReply::ConnectionRefusedError:
            file = new QFile(":/ErrorPages/connectionrefused");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            setHtml(html, reply->url());
            break;
    case QNetworkReply::RemoteHostClosedError:
            file = new QFile(":/ErrorPages/remotehostclosed");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            setHtml(html, reply->url());
            break;
    case QNetworkReply::ProxyNotFoundError:
            file = new QFile(":/ErrorPages/proxy404");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            setHtml(html, reply->url());
            break;
    case QNetworkReply::ProxyConnectionRefusedError:
            file = new QFile(":/ErrorPages/proxyrefused");
            file->open(QFile::ReadOnly);
            in = new QTextStream(file);
            html = in->readAll();
            file->close();
            setHtml(html, reply->url());
            break;
    };

    file = new QFile(":/ErrorPages/opcanceled");
    in = new QTextStream(file);

    delete file;
    delete in;
}

void WebView::unsupportedContent(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NoError)
        downloadRequested(reply->request());
    else
        handleErrorPage(reply);
}

void WebView::iconChanged()
{
    emit iconChangedOnTab(m_tabIndex);
}

bool WebView::loading()
{
    return m_loading;
}

void WebView::printPreview()
{
    QPrintPreviewDialog *m_preview = new QPrintPreviewDialog(this);
    connect(m_preview, SIGNAL(paintRequested(QPrinter*)), this, SLOT(pageRequested(QPrinter*)));
    if(m_preview->exec()) { m_preview->printer()->setDocName(title()); }
    disconnect(m_preview, SIGNAL(paintRequested(QPrinter*)));
    delete m_preview;
}

void WebView::printDocument()
{
    QPrinter printer;
    QPrintDialog m_print(&printer, this);
    printer.setDocName(title());
    if(m_print.exec() == QDialog::Accepted)
        pageRequested(&printer);

    return;
}

void WebView::pageRequested(QPrinter *printer)
{
    page()->mainFrame()->print(printer);
}

void WebView::setupContextMenu()
{
    viewSource = new QAction(tr("View Source"), this);
    pageInfo = new QAction(tr("Get Info"), this);
    searchText = new QAction(tr("Web Search For: "), this);
    contextMenu = new QMenu(this);
    contextMenu->addAction(m_page->action(QWebPage::Back));
    contextMenu->addAction(m_page->action(QWebPage::Forward));
    contextMenu->addAction(m_page->action(QWebPage::Reload));
    contextMenu->addAction(m_page->action(QWebPage::Stop));
    contextMenu->addSeparator();
    contextMenu->addAction(m_page->action(QWebPage::Copy));
    contextMenu->addAction(m_page->action(QWebPage::SelectAll));
    contextMenu->addSeparator();
    contextMenu->addAction(m_page->action(QWebPage::InspectElement));
    contextMenu->addAction(viewSource);
    contextMenu->addAction(pageInfo);

    imageContextMenu = new QMenu(this);
    imageContextMenu->addAction(m_page->action(QWebPage::OpenImageInNewWindow));
    imageContextMenu->addAction(m_page->action(QWebPage::CopyImageToClipboard));
    imageContextMenu->addAction(m_page->action(QWebPage::DownloadImageToDisk));
    imageContextMenu->addSeparator();
    imageContextMenu->addAction(m_page->action(QWebPage::InspectElement));
    imageContextMenu->addAction(viewSource);
    imageContextMenu->addAction(pageInfo);

    textContextMenu = new QMenu(this);
    textContextMenu->addAction(searchText);
    textContextMenu->addAction(m_page->action(QWebPage::Copy));
    textContextMenu->addAction(m_page->action(QWebPage::SelectAll));
    textContextMenu->addSeparator();
    textContextMenu->addAction(m_page->action(QWebPage::InspectElement));
    textContextMenu->addAction(viewSource);
    textContextMenu->addAction(pageInfo);

    linkContextMenu = new QMenu(this);
    linkContextMenu->addAction(m_page->action(QWebPage::OpenLink));
    linkContextMenu->addAction(m_page->action(QWebPage::OpenLinkInNewWindow));
    linkContextMenu->addAction(m_page->action(QWebPage::DownloadLinkToDisk));
    linkContextMenu->addAction(m_page->action(QWebPage::CopyLinkToClipboard));
    linkContextMenu->addSeparator();
    linkContextMenu->addAction(m_page->action(QWebPage::InspectElement));
    linkContextMenu->addAction(viewSource);
    linkContextMenu->addAction(pageInfo);

    connect(pageInfo, SIGNAL(triggered()), this, SLOT(pageInfo()));
    connect(viewSource, SIGNAL(triggered()), this, SLOT(viewSource()));
}

void WebView::contextMenuEvent(QContextMenuEvent *event)
{
    try {
        QWebHitTestResult result = page()->mainFrame()->hitTestContent(event->pos());

        if(selectedText() != QString("") && !result.linkUrl().isEmpty())
        {
            searchText->setText(tr("Web Search For: %1").arg(selectedText()));
            textContextMenu->popup(mapToGlobal(event->pos()));
            return;
        }
        if(result.pixmap().isNull() == false)
        {
            imageContextMenu->popup(mapToGlobal(event->pos()));
            return;
        }
        if(!result.linkUrl().isEmpty())
        {
            linkContextMenu->popup(mapToGlobal(event->pos()));
            return;
        }

        contextMenu->popup(mapToGlobal(event->pos()));
    }
    catch(int e)
    {
		Q_UNUSED(e);
    }
}


void WebView::finished(QNetworkReply *reply)
{
    if(reply->header(QNetworkRequest::ContentTypeHeader).isValid())
        m_doctype = reply->header(QNetworkRequest::ContentTypeHeader).toString();
    else
        m_doctype = QString("unknown");

    if(reply->sslConfiguration().isNull())
        m_isSecure = false;   // We are not using SSL here.
    else
    {
        m_sslConfig = reply->sslConfiguration();
        m_isSecure = true;
    }
}

QSslConfiguration WebView::sslConfiguration()
{
    return m_sslConfig;
}

bool WebView::isSecure()
{
    return m_isSecure;
}

void WebView::sslErrors(QNetworkReply * reply, QList<QSslError> errors)
{
    if(Settings::hasSslException(reply->url().host()) == false)
    {
        if(!m_acceptSsl.contains(reply->url().host()))
        {
            QStringList info;
            QString text = QString(tr("The following error(s) occured when attempting to access %1:")).arg(reply->url().host());
            for(int i = 0; i < errors.count(); ++i)
            {
                info.append(errors[i].errorString());
            }
            info.append(QString(tr("\nYou may happily continue on your way just this once, leave this site now, or add a permanent exception and never be bothered again.")));

            QMessageBox msg(QMessageBox::Critical, QString(tr("SSL Errors on: %1")).arg(reply->url().host()),
               text, QMessageBox::Ok|QMessageBox::Cancel|QMessageBox::Save, this);
            msg.setInformativeText(info.join("\n"));
            msg.setButtonText(QMessageBox::Ok, tr("Continue"));
            msg.setButtonText(QMessageBox::Cancel, tr("Get me out of here"));
            msg.setButtonText(QMessageBox::Save, tr("Continue and Save Exception"));
            int result = msg.exec();

            switch(result)
            {
                case QMessageBox::Ok:
                m_acceptSsl.append(reply->url().host());
                reply->ignoreSslErrors();
                break;
                case QMessageBox::Save:
                Settings::appendSslException(reply->url().host());
                reply->ignoreSslErrors();
                break;
                default:
                break;
            }
        }
        else
            reply->ignoreSslErrors();
    }
    else
        reply->ignoreSslErrors();
}

void WebView::showPageInfo()
{
    m_pageInfo = new PageInfo(this, false);
    if(m_pageInfo->exec()) {}

    delete m_pageInfo;
}

void WebView::showPageSource()
{
    m_pageInfo = new PageInfo(this, true);
    if(m_pageInfo->exec()) {}

    delete m_pageInfo;
}
