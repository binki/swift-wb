/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "cookiejar.h"

CookieJar::CookieJar(QObject *parent)
	: QNetworkCookieJar(parent)
{
    //buildGTldTable();
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    /* quick check to see if the cookies table actually EXISTS */
    query.exec(QString("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'cookies';"));
    if(!query.first())
    {
        /* okay, well, we don't have the cookie table, so let's create it */
        query.exec(QString("CREATE TABLE [cookies] ([id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, [from] TEXT  NOT NULL,[name] text  NOT NULL, [value] text  NULL, [path] text NULL, [expires] INTEGER  NULL);"));
    }

    db.close();
}

CookieJar::~CookieJar()
{

}

/* The Swift cookie jar works on SQLite. */

QList<QNetworkCookie> CookieJar::cookiesForUrl (const QUrl &url) const
{
    QList<QNetworkCookie> jar;
    QString urlString(getSqlFormatDomain(url));

    if(!urlString.isEmpty())
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
        db.open();
        QSqlQuery query(db);
        query.prepare("SELECT `name`, `value`, `path`, `from`, `expires` FROM cookies WHERE `from` LIKE :name AND expires > :expires OR expires = 0;");
        query.bindValue(":name", urlString);
        query.bindValue(":expires", QDateTime::currentDateTime().toUTC().toTime_t());
        query.exec();

        while (query.next())
        {
            if(isValidCookie(url, query.value(3).toString(), query.value(2).toString()) == true)
            {
                QNetworkCookie cookie;
                cookie.setDomain(query.value(3).toString());
                cookie.setName(query.value(0).toByteArray());
                cookie.setValue(query.value(1).toByteArray());
                cookie.setPath(query.value(2).toString());
                if(query.value(4).toUInt() == 0)
                    cookie.setExpirationDate(QDateTime());
                else
                    cookie.setExpirationDate(QDateTime::fromTime_t(query.value(4).toUInt()));

                jar.append(cookie);
            }
        }
        db.close();
    }
    else
    {
        qDebug("failed to get proper domain");
    }
    return jar;
}

bool CookieJar::setCookiesFromUrl(const QList<QNetworkCookie> &list, const QUrl &url)
{
    Q_UNUSED(url);
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();

    for(int i = 0; i < list.count(); ++i)
    {
        QString domain = list.at(i).domain();
        if(domain == QString("")) /* if the domain is null, assume the domain it came from */
            domain = url.host();

        unsigned int expires = list.at(i).expirationDate().toTime_t();
        if(list.at(i).expirationDate().isNull())
            expires = 0;
        QString path = list.at(i).path();
        if(path.isNull())  /* if the path is null, assume the path it came from */
            path == url.path();

        QString name = QString(list.at(i).name().data());
        QString value = list.at(i).value().data();

        QSqlQuery query1(db);
        query1.prepare("SELECT count(*) FROM cookies WHERE `from` = :from AND `name` = :name;");
        query1.bindValue(":from", domain);
        query1.bindValue(":name", name);
        if(!query1.exec())
        {
            qDebug("swift WARN - failed to select cookies");
        }

        query1.next();
        int count = query1.value(0).toInt();
        QString strCount = query1.value(0).toString();
        if(count == 0)
        {
            QSqlQuery query2(db);
            query2.prepare("INSERT INTO cookies (`from`, `name`, `value`, `path`, `expires`) VALUES (:from, :name, :value, :path, :expires);");
            query2.bindValue(":from", domain);
            query2.bindValue(":name", name);
            query2.bindValue(":value", value);
            query2.bindValue(":path", path);
            query2.bindValue(":expires", expires);
            if(!query2.exec())
                qDebug("swift WARN: failed to set cookie");
        }
        else
        {
            if(expires < QDateTime::currentDateTime().toTime_t() && expires != 0)
            {
                /* it's set to the past - delete it */
                QSqlQuery query2(db);
                query2.prepare("DELETE FROM cookies WHERE `from` = :from AND `name` = :name;");
                query2.bindValue(":from", domain);
                query2.bindValue(":name", name);
                if(!query2.exec())
                    qDebug("swift WARN: failed to delete old cookie");
            }
            else
            {
                /* it's still current, update the information */
                QSqlQuery query2(db);
                query2.prepare("UPDATE cookies SET `value` = :value, `expires` = :expires, `path` = :path WHERE `from` = :from AND `name` = :name;");
                query2.bindValue(":from", domain);
                query2.bindValue(":name", name);
                query2.bindValue(":value", value);
                query2.bindValue(":expires", expires);
                query2.bindValue(":path", path);
                if(!query2.exec())
                    qDebug("swift WARN: failed to update cookie");
            }
        }
    }

    db.close();
    return true;
}

void CookieJar::deleteSessionCookies()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM cookies WHERE expires = 0;");
    query.exec();
    db.close();
}

bool CookieJar::isValidCookie(const QUrl &myUrl, QString cookieHost, QString cookiePath) const
{
    bool isValidCookie = false;
    if(myUrl.host() == cookieHost) /* direct match */
        isValidCookie = true;

    if(myUrl.host().endsWith(cookieHost)) /* tail matching */
        isValidCookie = true;

    if(!myUrl.path().startsWith(cookiePath)) /* path matching */
        isValidCookie = false;

    //qDebug("checking for valid cookie");
    return isValidCookie;
}

void CookieJar::buildGTldTable()
{
    gTldTable.append("aero");
    gTldTable.append("asia");
    gTldTable.append("biz");
    gTldTable.append("cat");
    gTldTable.append("com");
    gTldTable.append("coop");
    gTldTable.append("edu");
    gTldTable.append("gov");
    gTldTable.append("info");
    gTldTable.append("int");
    gTldTable.append("jobs");
    gTldTable.append("mil");
    gTldTable.append("mobi");
    gTldTable.append("museum");
    gTldTable.append("name");
    gTldTable.append("net");
    gTldTable.append("org");
    gTldTable.append("pro");
    gTldTable.append("tel");
    gTldTable.append("travel");

    levelTwo.append("co");
    levelTwo.append("com");
    levelTwo.append("org");
    levelTwo.append("net");
    levelTwo.append("gov");
    levelTwo.append("ac");
}

QString CookieJar::getSqlFormatDomain(const QUrl &myUrl) const
{
    /* this function attempts to get the bare minimum of a domain needed
       to preform a LIKE sql query, to lessen the load of the amount of
       cookies returned in the SELECT.
    */

    QStringList myHost = myUrl.host().split(".");
    QString myReturn;
    int c = myHost.count();
    /* sanity check - do we have a host and/or are we on http(s)? */
    if(myUrl.host().isEmpty() || !myUrl.scheme().startsWith("http"))
        return QString();

    /* sanity check - are we less than 2 parts and NOT localhost (only allowed) */
    if(c < 2 && myUrl.host() != QString("localhost"))
        return QString();

    if(c == 2)
    {
        /* we only have 2 parts, return the whole host */
        myReturn = myUrl.host();
    }
    else if(gTldTable.contains(myHost.last()))
    {
        /* it's a gtld, return the last 2 parts */
        myReturn = QString("%1.%2").arg(myHost.at(c-2)).arg(myHost.last());
    }
    else
    {
        /* were assumeably a ccTLD */
        if(levelTwo.contains(myHost.at(c-2)))
        {
            /* it's a 3rd level registration */
            myReturn = QString("%1.%2.%3").arg(myHost.at(c-3)).arg(myHost.at(c-2)).arg(myHost.last());
        }
        else
        {
            /* assume a 2nd level at minimum - better safe than sorry */
            myReturn = QString("%1.%2").arg(myHost.at(c-2)).arg(myHost.last());
        }
    }
    return QString("%" + myReturn);
}
