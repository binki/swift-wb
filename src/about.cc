/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "about.h"

AboutDialog::AboutDialog(QWidget *parent = 0)
        : QDialog(parent)
{
    if (objectName().isEmpty())
        setObjectName(QString::fromUtf8("AboutDialog"));
    setWindowModality(Qt::ApplicationModal);
    resize(445, 300);
    /* QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    setSizePolicy(sizePolicy); */
    setModal(true);
    buttonBox = new QDialogButtonBox(this);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setGeometry(QRect(350, 10, 81, 241));
    buttonBox->setOrientation(Qt::Vertical);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    swLogo = new QLabel(this);
    swLogo->setObjectName(QString::fromUtf8("swLogo"));
    swLogo->setGeometry(QRect(10, 10, 64, 64));
    swLogo->setPixmap(QPixmap(QString::fromUtf8(":/Swift/swift-logo")));
    swLogo->setScaledContents(true);
    product = new QLabel(this);
    product->setObjectName(QString::fromUtf8("product"));
    product->setGeometry(QRect(80, 10, 201, 17));
    product->setTextFormat(Qt::PlainText);
    label = new QLabel(this);
    label->setObjectName(QString::fromUtf8("label"));
    label->setGeometry(QRect(80, 40, 251, 16));
    textBrowser = new QTextBrowser(this);
    textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
    textBrowser->setGeometry(QRect(10, 80, 421, 211));
    retranslateUi();
    QObject::connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    QMetaObject::connectSlotsByName(this);
} // setupUi

void AboutDialog::retranslateUi()
{
    setWindowTitle(tr("About {CODENAME}").replace("{CODENAME}", qApp->applicationName()));
    swLogo->setText(QString());
    product->setText(tr("{CODENAME} {VERSION}"));
    label->setText(tr("Copyright (C) 2006-2010 Chris Fuenty"));
    QString htm;
    QFile *file = new QFile(":/ErrorPages/about");
    file->open(QFile::ReadOnly);
    QTextStream *in = new QTextStream(file);
    htm = in->readAll();
    file->close();
    delete in;
    delete file;

    htm.replace("{QT_VERSION}", QString(QT_VERSION_STR));
    htm.replace("{COMPILE_TIME}", QString(__DATE__) + QString(" ") + QString(__TIME__));
#ifdef __GNUC__
    htm.replace("{COMPILER_VERSION}", QString("GCC ") + QString::number(__GNUC__) + QString(".") + QString::number(__GNUC_MINOR__));
#elif MSVC
    htm.replace("COMPILER_VERSION", QString("Microsoft Visual C " + _MSC_VER));
#endif
    product->setText(product->text().replace("{CODENAME}", qApp->applicationName()));
    product->setText(product->text().replace("{VERSION}", qApp->applicationVersion()));

    textBrowser->setHtml(htm);
}
