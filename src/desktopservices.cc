/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "desktopservices.h"

proxy_t DesktopServices::iexploreProxy()
{
    /* iexploreProxy() makes an attempt to get the proxy details from Internet Options.
       As this function is tied down to IE, this is only available on Windows.
    */

    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings",
        QSettings::NativeFormat);

    QString IEProxyHost = settings.value("ProxyServer").toString();
    proxy_t ieProxy;
    if(IEProxyHost.startsWith("http"))
    {
        QString actualProxy = IEProxyHost.split("://").at(1);
        QStringList urlParts = actualProxy.split(":");
        ieProxy.proxyPort = urlParts.at(1).toInt();
        ieProxy.proxyHost = urlParts.at(0);
        ieProxy.proxyType = QNetworkProxy::HttpProxy;
    }
    if(IEProxyHost.startsWith("socks"))
    {
        QString actualProxy = IEProxyHost.split("=").at(1);
        QStringList urlParts = actualProxy.split(":");
        ieProxy.proxyPort = urlParts.at(1).toInt();
        ieProxy.proxyHost = urlParts.at(0);
        ieProxy.proxyType = QNetworkProxy::Socks5Proxy;
    }

    return ieProxy;
}