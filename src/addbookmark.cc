/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "addbookmark.h"

AddBookmarkDialog::AddBookmarkDialog(QWidget * parent, Qt::WindowFlags f)
    : QDialog(parent, f)
{
    if (objectName().isEmpty())
        setObjectName(QString::fromUtf8("addBookmarkDialog"));
    setWindowModality(Qt::ApplicationModal);
    resize(431, 160);
    setModal(true);
    buttonBox = new QDialogButtonBox(this);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setGeometry(QRect(340, 10, 81, 241));
    buttonBox->setOrientation(Qt::Vertical);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    labelAddBookmark = new QLabel(this);
    labelAddBookmark->setObjectName(QString::fromUtf8("labelAddBookmark"));
    labelAddBookmark->setGeometry(QRect(10, 10, 121, 17));
    labelAddBookmark->setTextFormat(Qt::RichText);
    label_task = new QLabel(this);
    label_task->setObjectName(QString::fromUtf8("label_task"));
    label_task->setGeometry(QRect(20, 30, 311, 41));
    label_task->setWordWrap(true);
    widget = new QWidget(this);
    widget->setObjectName(QString::fromUtf8("widget"));
    widget->setGeometry(QRect(10, 100, 411, 56));
    layout = new QHBoxLayout(widget);
    layout->setObjectName(QString::fromUtf8("layout"));
    layout->setContentsMargins(0, 0, 0, 0);
    vlayout1 = new QVBoxLayout();
    vlayout1->setObjectName(QString::fromUtf8("vlayout1"));
    label_title = new QLabel(widget);
    label_title->setObjectName(QString::fromUtf8("label_title"));

    vlayout1->addWidget(label_title);

    label_CreateIn = new QLabel(widget);
    label_CreateIn->setObjectName(QString::fromUtf8("label_CreateIn"));

    vlayout1->addWidget(label_CreateIn);


    layout->addLayout(vlayout1);

    vlayout2 = new QVBoxLayout();
    vlayout2->setObjectName(QString::fromUtf8("vlayout2"));
    titleBox = new QLineEdit(widget);
    titleBox->setObjectName(QString::fromUtf8("titleBox"));

    vlayout2->addWidget(titleBox);

    folderBox = new QComboBox(widget);
    folderBox->setObjectName(QString::fromUtf8("folderBox"));

    vlayout2->addWidget(folderBox);


    layout->addLayout(vlayout2);

    setWindowTitle(QApplication::translate("this", "Create Bookmark", 0, QApplication::UnicodeUTF8));
    labelAddBookmark->setText(QApplication::translate("this", "<b>Add Bookmark</b>", 0, QApplication::UnicodeUTF8));
    label_task->setText(QApplication::translate("this", "Add this page as a bookmark.  To reference this page later, use the Bookmark menu.", 0, QApplication::UnicodeUTF8));
    label_title->setText(QApplication::translate("this", "Title:", 0, QApplication::UnicodeUTF8));
    label_CreateIn->setText(QApplication::translate("this", "Create In:", 0, QApplication::UnicodeUTF8));
    //retranslateUi(this);
    QObject::connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    QMetaObject::connectSlotsByName(this);

    folderBox->addItem(style()->standardIcon(QStyle::SP_DirIcon), tr("Bookmark Root"), "\\");
}

AddBookmarkDialog::~AddBookmarkDialog() {}

void AddBookmarkDialog::addFolderLocation(QString path)
{
    QStringList lst = path.split("\\");
    int count = lst.count();
    if(lst.count() == 2) {
        folderBox->addItem(style()->standardIcon(QStyle::SP_DirIcon), "---" + lst.at(lst.count()-1),
           path);
    } else {
        /* we need to find the folder that this folder resdes in 2nd to last item */
        QString tfolder = lst.last();
        for(int i = 0; i < count - 1; i++)
        {
            tfolder.prepend("---");
        }

        lst.removeLast();
        int ipath = folderBox->findData(lst.join("\\"));
        folderBox->insertItem(++ipath, style()->standardIcon(QStyle::SP_DirIcon),
            tfolder, path);
    }
}

void AddBookmarkDialog::setPageDetails(QString u, QString t)
{
    url = u;
    title = t;
    titleBox->setText(title);
}

void AddBookmarkDialog::accept()
{
    QString path = folderBox->itemData(folderBox->currentIndex()).toString();
    emit addNewBookmark(path, url, titleBox->text());
    this->hide();
}
