/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "pageinfo.h"

PageInfo::PageInfo(WebView *parent = 0, bool showSource = false)
    :QDialog(parent)
{
    if (objectName().isEmpty())
        setObjectName(QString::fromUtf8("pageInfo"));
    resize(595, 359);
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(1);
    sizePolicy.setVerticalStretch(1);
    //sizePolicy.setHeightForWidth(sizePolicy.hasHeightForWidth());
    setSizePolicy(sizePolicy);
    gridLayout_2 = new QGridLayout(this);
    gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
    tabWidget = new QTabWidget(this);
    tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
    tabWidget->setSizeIncrement(QSize(1, 1));
    general = new QWidget();
    general->setObjectName(QString::fromUtf8("general"));
    gridLayout = new QGridLayout(general);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    verticalLayout = new QVBoxLayout();
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    verticalLayout_3 = new QVBoxLayout();
    verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
    pageTitle = new QLabel(general);
    pageTitle->setObjectName(QString::fromUtf8("pageTitle"));
    QFont font;
    font.setBold(true);
    font.setWeight(75);
    pageTitle->setFont(font);
    verticalLayout_3->addWidget(pageTitle);
    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setSpacing(5);
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    labelLayout = new QVBoxLayout();
    labelLayout->setSpacing(7);
    labelLayout->setObjectName(QString::fromUtf8("labelLayout"));
    labelLayout->setSizeConstraint(QLayout::SetFixedSize);
    tAddress = new QLabel(general);
    tAddress->setObjectName(QString::fromUtf8("tAddress"));
    labelLayout->addWidget(tAddress);
    tType = new QLabel(general);
    tType->setObjectName(QString::fromUtf8("tType"));
    labelLayout->addWidget(tType);
    tEncoding = new QLabel(general);
    tEncoding->setObjectName(QString::fromUtf8("tEncoding"));
    labelLayout->addWidget(tEncoding);
    tSize = new QLabel(general);
    tSize->setObjectName(QString::fromUtf8("tSize"));
    labelLayout->addWidget(tSize);
    tReferring = new QLabel(general);
    tReferring->setObjectName(QString::fromUtf8("tReferring"));
    labelLayout->addWidget(tReferring);
    horizontalLayout->addLayout(labelLayout);
    verticalLayout_2 = new QVBoxLayout();
    verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
    pageLocation = new QLabel(general);
    pageLocation->setObjectName(QString::fromUtf8("pageLocation"));
    verticalLayout_2->addWidget(pageLocation);
    pageType = new QLabel(general);
    pageType->setObjectName(QString::fromUtf8("pageType"));
    verticalLayout_2->addWidget(pageType);
    pageEncoding = new QLabel(general);
    pageEncoding->setObjectName(QString::fromUtf8("pageEncoding"));
    verticalLayout_2->addWidget(pageEncoding);
    pageSize = new QLabel(general);
    pageSize->setObjectName(QString::fromUtf8("pageSize"));
    verticalLayout_2->addWidget(pageSize);
    referringURL = new QLabel(general);
    referringURL->setObjectName(QString::fromUtf8("referringURL"));
    verticalLayout_2->addWidget(referringURL);
    horizontalLayout->addLayout(verticalLayout_2);
    horizontalLayout->setStretch(1, 1);
    verticalLayout_3->addLayout(horizontalLayout);
    verticalLayout->addLayout(verticalLayout_3);
    metaGroupBox = new QGroupBox(general);
    metaGroupBox->setObjectName(QString::fromUtf8("metaGroupBox"));
    gridLayout_3 = new QGridLayout(metaGroupBox);
    gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
    metaTags = new QTableWidget(metaGroupBox);
    if (metaTags->columnCount() < 2)
    metaTags->setColumnCount(2);
    QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
    metaTags->setHorizontalHeaderItem(0, __qtablewidgetitem);
    QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
    metaTags->setHorizontalHeaderItem(1, __qtablewidgetitem1);
    if (metaTags->rowCount() < 1)
    metaTags->setRowCount(1);
    QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
    metaTags->setVerticalHeaderItem(0, __qtablewidgetitem2);
    QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
    metaTags->setItem(0, 0, __qtablewidgetitem3);
    QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
    metaTags->setItem(0, 1, __qtablewidgetitem4);
    metaTags->setObjectName(QString::fromUtf8("metaTags"));
    metaTags->setAlternatingRowColors(true);
    metaTags->horizontalHeader()->setVisible(true);
    metaTags->horizontalHeader()->setHighlightSections(false);
    metaTags->horizontalHeader()->setMinimumSectionSize(53);
    metaTags->verticalHeader()->setVisible(false);
    gridLayout_3->addWidget(metaTags, 0, 0, 1, 1);
    verticalLayout->addWidget(metaGroupBox);
    gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);
    tabWidget->addTab(general, QString());
    security = new QWidget();
    security->setObjectName(QString::fromUtf8("security"));
    gridLayout_9 = new QGridLayout(security);
    gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
    identity = new QGroupBox(security);
    identity->setObjectName(QString::fromUtf8("identity"));
    gridLayout_6 = new QGridLayout(identity);
    gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
    identityLayout = new QGridLayout();
    identityLayout->setObjectName(QString::fromUtf8("identityLayout"));
    label = new QLabel(identity);
    label->setObjectName(QString::fromUtf8("label"));
    identityLayout->addWidget(label, 0, 0, 1, 1);
    website = new QLabel(identity);
    website->setObjectName(QString::fromUtf8("website"));
    website->setFont(font);
    identityLayout->addWidget(website, 0, 1, 1, 1);
    label_3 = new QLabel(identity);
    label_3->setObjectName(QString::fromUtf8("label_3"));
    identityLayout->addWidget(label_3, 1, 0, 1, 1);
    owner = new QLabel(identity);
    owner->setObjectName(QString::fromUtf8("owner"));
    owner->setFont(font);
    identityLayout->addWidget(owner, 1, 1, 1, 1);
    label_5 = new QLabel(identity);
    label_5->setObjectName(QString::fromUtf8("label_5"));
    identityLayout->addWidget(label_5, 2, 0, 1, 1);
    verifiedby = new QLabel(identity);
    verifiedby->setObjectName(QString::fromUtf8("verifiedby"));
    verifiedby->setFont(font);
    identityLayout->addWidget(verifiedby, 2, 1, 1, 1);
    identityLayout->setColumnStretch(1, 3);
    gridLayout_6->addLayout(identityLayout, 0, 0, 1, 1);
    gridLayout_9->addWidget(identity, 0, 0, 1, 1);
    privhistory = new QGroupBox(security);
    privhistory->setObjectName(QString::fromUtf8("privhistory"));
    gridLayout_7 = new QGridLayout(privhistory);
    gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
    label_2 = new QLabel(privhistory);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    gridLayout_7->addWidget(label_2, 0, 0, 1, 1);
    visitsToSite = new QLabel(privhistory);
    visitsToSite->setObjectName(QString::fromUtf8("visitsToSite"));
    visitsToSite->setFont(font);
    gridLayout_7->addWidget(visitsToSite, 0, 1, 1, 2);
    label_4 = new QLabel(privhistory);
    label_4->setObjectName(QString::fromUtf8("label_4"));
    gridLayout_7->addWidget(label_4, 1, 0, 1, 1);
    storingCookies = new QLabel(privhistory);
    storingCookies->setObjectName(QString::fromUtf8("storingCookies"));
    storingCookies->setFont(font);
    gridLayout_7->addWidget(storingCookies, 1, 1, 1, 2);
    label_6 = new QLabel(privhistory);
    label_6->setObjectName(QString::fromUtf8("label_6"));
    gridLayout_7->addWidget(label_6, 2, 0, 1, 1);
    savedPasswords = new QLabel(privhistory);
    savedPasswords->setObjectName(QString::fromUtf8("savedPasswords"));
    savedPasswords->setFont(font);
    gridLayout_7->addWidget(savedPasswords, 2, 1, 1, 2);
    pushButton = new QPushButton(privhistory);
    pushButton->setObjectName(QString::fromUtf8("pushButton"));
    gridLayout_7->addWidget(pushButton, 3, 0, 1, 2);
    pushButton_2 = new QPushButton(privhistory);
    pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
    gridLayout_7->addWidget(pushButton_2, 3, 2, 1, 1);
    gridLayout_9->addWidget(privhistory, 1, 0, 1, 1);
    groupBox = new QGroupBox(security);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    gridLayout_8 = new QGridLayout(groupBox);
    gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
    encryptedConnection = new QLabel(groupBox);
    encryptedConnection->setObjectName(QString::fromUtf8("encryptedConnection"));
    encryptedConnection->setFont(font);
    gridLayout_8->addWidget(encryptedConnection, 0, 0, 1, 1);
    viewCertificate = new QPushButton(groupBox);
    viewCertificate->setObjectName(QString::fromUtf8("viewCertificate"));
    viewCertificate->setEnabled(false);
    gridLayout_8->addWidget(viewCertificate, 0, 1, 1, 1);
    labelInfo = new QLabel(groupBox);
    labelInfo->setObjectName(QString::fromUtf8("labelInfo"));
    gridLayout_8->addWidget(labelInfo, 1, 0, 1, 1);
    gridLayout_9->addWidget(groupBox, 2, 0, 1, 1);
    verticalSpacer = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);
    gridLayout_9->addItem(verticalSpacer, 3, 0, 1, 1);
    tabWidget->addTab(security, QString());
    source = new QWidget();
    source->setObjectName(QString::fromUtf8("source"));
    gridLayout_5 = new QGridLayout(source);
    gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
    sourceBox = new QGroupBox(source);
    sourceBox->setObjectName(QString::fromUtf8("sourceBox"));
    gridLayout_4 = new QGridLayout(sourceBox);
    gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
    htmlSource = new QTextBrowser(sourceBox);
    htmlSource->setObjectName(QString::fromUtf8("htmlSource"));
    htmlSource->setAcceptRichText(false);
    gridLayout_4->addWidget(htmlSource, 0, 0, 1, 1);
    gridLayout_5->addWidget(sourceBox, 0, 0, 1, 1);
    tabWidget->addTab(source, QString());
    gridLayout_2->addWidget(tabWidget, 0, 0, 1, 1);

    retranslate();
    parent->title();
    setWindowTitle(tr("Page Info for: ") + parent->title());
    pageTitle->setText(parent->title());
    pageLocation->setText(parent->url().toString());
    pageSize->setText(QString::number(parent->page()->totalBytes() / 1024) + " KB");
    website->setText(parent->url().host());
    visitsToSite->setText(tr("Yes, ") + pageAccesses(parent->url()) + tr(" times."));
    if(!parent->sslConfiguration().isNull())
    {
        owner->setText(parent->sslConfiguration().peerCertificate().subjectInfo(QSslCertificate::Organization));
        verifiedby->setText(parent->sslConfiguration().peerCertificate().issuerInfo(QSslCertificate::Organization));
        encryptedConnection->setText(tr("Encrypted Connection"));
        viewCertificate->setEnabled(true);
        labelInfo->setText(tr("The website you are visiting is encrypted with ") + parent->sslConfiguration().sessionCipher().usedBits() + tr("bit security. "));
        if(parent->sslConfiguration().peerCertificate().expiryDate() < QDateTime::currentDateTime())
        {
            labelInfo->text().append(tr("The website you are visiting has a security certificate which has expired."));
        }
    }
    savedPasswords->setText(QApplication::translate("pageInfo", "No", 0, QApplication::UnicodeUTF8));
    storingCookies->setText((Settings::hasCookies(parent->url()) == true ? tr("Yes") : tr("No")));
    htmlSource->setPlainText(parent->page()->mainFrame()->toHtml());
    pageType->setText(parent->doctype());
    if(showSource == true)
        tabWidget->setCurrentIndex(2);
}

void PageInfo::retranslate()
{
    encryptedConnection->setText(QApplication::translate("pageInfo", "Unencrypted Connection", 0, QApplication::UnicodeUTF8));

    tabWidget->setTabText(tabWidget->indexOf(general), QApplication::translate("pageInfo", "General", 0, QApplication::UnicodeUTF8));
    identity->setTitle(QApplication::translate("pageInfo", "Identity", 0, QApplication::UnicodeUTF8));
    label->setText(QApplication::translate("pageInfo", "Web Site:", 0, QApplication::UnicodeUTF8));
    label_3->setText(QApplication::translate("pageInfo", "Owner:", 0, QApplication::UnicodeUTF8));

    tAddress->setText(QApplication::translate("pageInfo", "Location:", 0, QApplication::UnicodeUTF8));
    tType->setText(QApplication::translate("pageInfo", "Type:", 0, QApplication::UnicodeUTF8));
    tEncoding->setText(QApplication::translate("pageInfo", "Encoding:", 0, QApplication::UnicodeUTF8));
    tSize->setText(QApplication::translate("pageInfo", "Size:", 0, QApplication::UnicodeUTF8));
    tReferring->setText(QApplication::translate("pageInfo", "Referring URL:", 0, QApplication::UnicodeUTF8));
    metaGroupBox->setTitle(QApplication::translate("pageInfo", "Meta Tags", 0, QApplication::UnicodeUTF8));
    QTableWidgetItem *___qtablewidgetitem = metaTags->horizontalHeaderItem(0);
    ___qtablewidgetitem->setText(QApplication::translate("pageInfo", "Tag", 0, QApplication::UnicodeUTF8));
    QTableWidgetItem *___qtablewidgetitem1 = metaTags->horizontalHeaderItem(1);
    ___qtablewidgetitem1->setText(QApplication::translate("pageInfo", "Data", 0, QApplication::UnicodeUTF8));
    QTableWidgetItem *___qtablewidgetitem2 = metaTags->verticalHeaderItem(0);
    ___qtablewidgetitem2->setText(QApplication::translate("pageInfo", "Content", 0, QApplication::UnicodeUTF8));

    const bool __sortingEnabled = metaTags->isSortingEnabled();
    metaTags->setSortingEnabled(false);
    QTableWidgetItem *___qtablewidgetitem3 = metaTags->item(0, 0);
    ___qtablewidgetitem3->setText(QApplication::translate("pageInfo", "Content-Type", 0, QApplication::UnicodeUTF8));
    QTableWidgetItem *___qtablewidgetitem4 = metaTags->item(0, 1);
    ___qtablewidgetitem4->setText(QApplication::translate("pageInfo", "UTF-8", 0, QApplication::UnicodeUTF8));
    metaTags->setSortingEnabled(__sortingEnabled);

    label_5->setText(QApplication::translate("pageInfo", "Verified By:", 0, QApplication::UnicodeUTF8));
    privhistory->setTitle(QApplication::translate("pageInfo", "Privacy and History", 0, QApplication::UnicodeUTF8));
    label_2->setText(QApplication::translate("pageInfo", "Have I visited this site before?", 0, QApplication::UnicodeUTF8));
    label_4->setText(QApplication::translate("pageInfo", "Is this website storing cookies?", 0, QApplication::UnicodeUTF8));
    storingCookies->setText(QApplication::translate("pageInfo", "Yes", 0, QApplication::UnicodeUTF8));
    label_6->setText(QApplication::translate("pageInfo", "Do I have saved passwords?", 0, QApplication::UnicodeUTF8));
    savedPasswords->setText(QApplication::translate("pageInfo", "No", 0, QApplication::UnicodeUTF8));
    pushButton->setText(QApplication::translate("pageInfo", "View Cookies", 0, QApplication::UnicodeUTF8));
    pushButton_2->setText(QApplication::translate("pageInfo", "View Saved Passwords", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QApplication::translate("pageInfo", "Techincal Data", 0, QApplication::UnicodeUTF8));
    encryptedConnection->setText(QApplication::translate("pageInfo", "Unencrypted Connection", 0, QApplication::UnicodeUTF8));
    viewCertificate->setText(QApplication::translate("pageInfo", "View Certificate", 0, QApplication::UnicodeUTF8));
    labelInfo->setText(QApplication::translate("pageInfo", "The website you are visiting is unencrypted. Any data sent is suspectable to eavesdropping.", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(security), QApplication::translate("pageInfo", "Security", 0, QApplication::UnicodeUTF8));
    sourceBox->setTitle(QApplication::translate("pageInfo", "Source For: ", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(source), QApplication::translate("pageInfo", "Source", 0, QApplication::UnicodeUTF8));
}
