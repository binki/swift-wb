/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "settings.h"

Settings::Settings(QObject *parent = 0)
	: QSettings("Swift", "Swift", parent)
{
	
}

Settings::~Settings()
{

}

void Settings::setToolbarShown(bool show)
{
    setValue("ShowToolBar", show);
}

bool Settings::toolbarShown()
{
    if(value("ShowToolBar").isNull())
        return true;

    return value("ShowToolBar").toBool();
}

void Settings::setStatusBarShown(bool show)
{
    setValue("ShowStatusBar", show);
}

bool Settings::statusBarShown()
{
    if(value("ShowStatusBar").isNull())
        return true;

    return value("ShowStatusBar").toBool();
}

void Settings::setBookmarkBarShown(bool show)
{
    setValue("ShowBookmarkBar", show);
}

bool Settings::bookmarkBarShown()
{
    if(value("ShowBookmarkBar").isNull())
        return true;

    return value("ShowBookmarkBar").toBool();
}

void Settings::setMenuBarShown(bool show)
{
    setValue("ShowMenuBar", show);
}

bool Settings::menuBarShown()
{
    if(value("ShowMenuBar").isNull())
        return true;

    return value("ShowMenuBar").toBool();
}

bool Settings::alwaysShowTabs()
{
    return value("AlwaysShowTabs").toBool();
}

void Settings::appendSslException(QString host)
{
    QSettings tmp("Swift", "Swift");
    QStringList sslHosts = tmp.value("SslHosts").toStringList();
    sslHosts.append(host);
    tmp.setValue("SslHosts", sslHosts);
}

bool Settings::hasSslException(QString host)
{
    QSettings tmp("Swift", "Swift");
    QStringList sslHosts = tmp.value("SslHosts").toStringList();
    return sslHosts.contains(host);
}

bool Settings::zonePreference(ZoneManager::Zone zone, QString preference)
{
    QSettings tmp("Swift", "Swift");
    tmp.beginGroup("Zones");
    switch (zone) {
        case ZoneManager::Internet:
            tmp.beginGroup("Internet");
            break;
        case ZoneManager::Trusted:
            tmp.beginGroup("TrustedSites");
            break;
        case ZoneManager::Restricted:
            tmp.beginGroup("RestrictedSites");
            break;
    }

    bool result = tmp.value(preference).toBool();

    tmp.endGroup();
    tmp.endGroup();

    return result;
}

bool Settings::useProxy()
{
    QSettings tmp("Swift", "Swift");
    return tmp.value("UseNetworkProxy").toBool();
}

proxy_t Settings::networkProxy()
{
    QSettings tmp("Swift", "Swift");
    proxy_t proxy;

#ifdef Q_WS_WIN
    if(tmp.value("NetworkProxyToUse").toInt() == 0)
    {
        /* system default proxy */
        proxy = DesktopServices::iexploreProxy();
    }
#endif

    if(tmp.value("NetworkProxyToUse").toInt() == 1)
    {
        proxy.proxyHost = tmp.value("NetworkProxyHost").toString();
        proxy.proxyPort = tmp.value("NetworkProxyPort").toInt();
        proxy.userName = tmp.value("NetworkProxyUser").toString();
        proxy.password = tmp.value("NetworkProxyPass").toString();
        if(tmp.value("NetworkProxyType").toInt() == 0)
            proxy.proxyType = QNetworkProxy::Socks5Proxy;
        else
            proxy.proxyType = QNetworkProxy::HttpProxy;
    }

    return proxy;
}

bool Settings::hasCookies(QUrl website)
{
    /* kind of ghetto how I need to define a QObject to
       be able to use the cookie jar. oh well.
    */

    QObject *obj = new QObject();
    CookieJar *jar = new CookieJar(obj);

    QList<QNetworkCookie> cookies = jar->cookiesForUrl(website);

    bool hasCookies = cookies.count() == 0 ? true : false;
    delete jar;
    delete obj;

    return hasCookies;
}
