/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "searchbox.h"

SearchBox::SearchBox(QWidget *parent = 0)
        : QLineEdit(parent)
{
    setTextMargins(height() - 4, 1, 1, 1);
    tbIcon = new QToolButton(this);
    engMenu = new QMenu(this);
    tbIcon->setMaximumSize(height() - 4, height() - 4);
    tbIcon->setAutoRaise(true);
    tbIcon->move(2, 2);
    tbIcon->setArrowType(Qt::NoArrow);

    this->setMaximumWidth(170);
    Settings settings(this->parent());
    settings.beginGroup("SearchEngines");
    uset = 0;
    QPalette pal;
    pal.setColor(QPalette::Text, Qt::gray);
    setPalette(pal);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.exec(QString("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'searchengines';"));
    if(!query.first())
    {
        query.exec(QString("CREATE TABLE [searchengines] ([id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, [Name] TEXT  NULL, [SearchURL] TEXT  NULL, [Image] TEXT  NULL)"));
        query.exec(QString("INSERT INTO searchengines VALUES(1,'Google','http://www.google.com/search?q={searchTerms}&client=swift','iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAABJ0AAASdAHeZh94AAADCklEQVQ4jSXSy2ucVRjA4d97zvdNJpPJbTJJE9rYaCINShZtRCFIA1bbLryBUlyoLQjqVl12W7UbN4qb1gtuYhFRRBCDBITaesFbbI3RFBLSptEY05l0ZjLfnMvrov/Bs3gAcF71x6VVHTk+o8nDH+hrH89rUK9Z9Yaen57S3wVtGaMBNGC0IegWKIDxTtVaOHVugZVmH3HX3Zz+4l+W1xvkOjuZfPsspY4CNkZELEgEIJKwYlBjEwjec/mfCMVuorVs76R8+P0KYMmP30U2dT8eIZqAR2ipRcWjEYxGSCRhV08e04oYMoxYLi97EI9YCJ0FHBYbIVGDlUBLwRlLIuYW6chEmQt/rJO09RJjhjEJEYvJYGNhkbUhw43OXtIWDFRq9G87nAaSK6sVRm8r8fzRMWbOX2Xx7ypd7ZET03sQhDOz73DqSJOrd+7HSo4QIu0Nx/4rOzx+cRXZ9+z7+uqJ+3hiepxK3fHZT2tMjXYzOtzL6dmznPzhLexgN0QlxAAYxAlqUqRmkf5j59RlNQ6MFHhgcpCTTx8EUb5e+plD7x4jjg1ANCAgrRQAdR7xKXjBlGyLYi7PxaUmb8z8xcpGHVXLHaXdjI0egKyJiQYTEhSPREVIEUBNC+Mqm+xpz3j0njLPHB2nsh1QgeG+IS48dYbD5YNoo0ZUAbVEuTUoKuBSZOarX/WhyQn6eg2+usDWf0s0tq8zNPYk+WI/Lnge++hlvlyfQ3NdECzGRWKwEEA0qNY251n69kV6+Y0kbaCZoebG2X3oU7pKoyxuXOPe945zs9DCeosGIXoBDyaLdf6ce4Hbk+/Y299ksKtAuaeNsiyw8c1LKIZ95b0MdgxA5giixACpTxEPSau6QdFfI5/2cLPmEW+JAQrtJUJzDXF1dkwHzVodJMX4HFEcQQMaFdPeM0Jb/4PUtzzaLKAhRyJFwo6lbegRNFfk819muV5dR4JBQoQdQ2xFiDmSNDHiaptamR9Gq5cQ18AledrGDpOfeI5Lq8u88smbhMRisoSAgAYghdfn5H/JkHuRZ1owLAAAAABJRU5ErkJggg==');"));
        query.exec(QString("INSERT INTO searchengines VALUES(2,'Wikipedia','http://en.wikipedia.org/wiki/Special:Search?search={searchTerms}','iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAACXBIWXMAABJ0AAASdAHeZh94AAABxklEQVQokZ2SMWvbQBTH/3HBlEBxEgLN0kznYihGHULxYGyJM7G2yCStPBtncD5BUCDuR/DHaIYKTQlOJZrBSwlFUUNBLgJZQ2iorNryIDqcoMMVJ/VS0scN9+7+P967978lljI8JDIPUv8PgP55X22qcl3un/dZyk7ench1WTvS3KHLUqYdaWpTdYeuO3Q7Bx21qWYopcUXxVEw4vzu610A+nsdQBRGy4+XJVEihKzmVgFIopQBsPdmL57GhmFEYQRAaSjxNHZsx/f9m+837f32vJ32fjsDgBBSESuGbvi+D0CUREEQum+7pmmKFZFLr75c8f2fR7daLQCmaXJeaSjeN+/66zXvMAqjy0+XK2srdwClVBCEwWDgeR4vUiqV4knMbyfxJPmVUEr/Gmu5XDY/mI7tzE9GwciyLAAXHy82n20u+nCoHZLnRDf0KIwc2ym+LK6vrRuGAWAcjhtKg8seHXeP58wsntmf7dsftxtPN3aUnelsenZ6ls1mkyTZlrcXKwDI5/Pjn2NuAiGkWq0C6PV6tVrtTsRSdn/JdVmuy9xmlrLOQadQKNwXLC38Vsuyck9yW6+2eOp5XhAEfD48FoF/xm88DOALbxAC2AAAAABJRU5ErkJggg==');"));
        query.exec(QString("INSERT INTO searchengines VALUES(3,'Yahoo','http://search.yahoo.com/search?p={searchTerms}','iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAABJ0AAASdAHeZh94AAABaklEQVQ4jdWSMUtCYRSGH+WODhcRsTAxcBIHCacKUjFoCKloCZqkH9DQJApG4tTcENUgTVIi4RBhZhDhYCByESIxCTEJqRChCMMGuZeu5Q/wTIfvO+d5z/neD0Y+NHKSCRV67UYbKVVTFdjcZox2Pb6oS6kNavd7KkAmVOjd7pWGqny+d/EGpxSICiA3r58sMOkZA+Dx6pn0Zp6m1FKBZIgMsLlNCC/lVwCOV89xLFlZPvRQzTZoSi22PwIAvN232XUeUD57UgEt0+MIlVxdOZBSNayzD8yFncjg650iF7EsHerUpX6dyWFQphMG901v3WB2GVg7necyfEcyFlfuBEQAdIZ+my/q0mhtbvOfRyslqgCqZlkZQLTqlFww2vUq6zYyi0zMGOl+fRMpRiglqiRjcUQc2P0WKMHKkVexVLFxUO13iDj+2Ghzmwhk/RpB3kX+SLVck0ot3x9ZdP77kQxWHZ1Wd6jgiMUPrROOBOa0KcsAAAAASUVORK5CYII=');"));
        query.exec(QString("INSERT INTO searchengines VALUES(4,'Bing','http://www.bing.com/search?q={searchTerms}&form=OSDSRC','iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAABJ0AAASdAHeZh94AAABjklEQVQ4ja2SO0/CYBiFnxYxgLcCsQskYiJxUCJxcXCwK0z8AAc2J42jm1/iHzBxdHFwcMRFRnHQxA3BwUTjLTiItyqBGk2tQ1uSSmJi8Izf5ZzznvdAl5BYPLUAtGQfpaVR6a8EcrcOuibo+XlQrhlWodKgfGegG18AKEGZdCyIlgyhJfs9Y7YzSET8AFw/f/6qmIj4EVmV/ExY8hC4GArKiIxKLjVAItprkz59ULpoIooP3DgC+RmFrfm45CGYigUoLSZQQj54OYVa0b6IZyA8id4y0TauObl7B0BkVK+Dq9WkrXq5A8dLXu/TazC+gN4yCa+cOdn4vFtwLXO10zm840YJ+ZgbCwGgGyayG547KwDqbCeBMml/apkcXLTax5LYq1uiWO/M4P4Q6kf2q/AExLMA5DZv2a02AFjWokgA+e2atXWsAzAS8SMyw+RSgzaRo1qovnm24Fa/XYr1/UdLFB/QDdPjfCgo8+oUyg1uWYsisqrTgx8oVN6s0nmTsrMqF+lYgHQ80C7Qv+EbbFiOXievCRMAAAAASUVORK5CYII=');"));

    }

    query.prepare("SELECT `Name`, `SearchURL`, `Image` from searchengines ORDER BY `id` ASC;");
    query.exec();

    while (query.next()) {
        search_t tmp;
        tmp.name = query.value(0).toString();
        tmp.searchUrl = query.value(1).toString();
        tmp.icon = QByteArray::fromBase64(query.value(2).toByteArray());
        engines.append(tmp);
    }

    loadSearchEngines();
    tbIcon->setMenu(engMenu);
    tbIcon->setPopupMode(QToolButton::InstantPopup);

    /*engMenu->addSeparator();
    engMenu->addAction(tr("Manage Search Engines..."));*/

    connect(engMenu, SIGNAL(triggered(QAction*)), this, SLOT(menuTriggered(QAction*)));

    connect(this, SIGNAL(returnPressed()), this, SLOT(returnPressed()));
    connect(this, SIGNAL(textChanged(QString)),
            this, SLOT(textChanged(QString)));
    connect(this, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));
}

SearchBox::~SearchBox()
{

}

void SearchBox::loadSearchEngines()
{
    Settings settings(this);

    if(engines.count() != 0)
    {
        for(int i = 0; i < engines.count(); ++i)
        {
            QPixmap img;
            img.loadFromData(QByteArray(engines.at(i).icon));
            QIcon icon(img);
            engMenu->addAction(icon, engines.at(i).name);
            if(i == 0) {
                tbIcon->setIcon(icon);
                changeEngine(engines.at(i));
            }
        }
    }
}

QToolButton *SearchBox::engineMenu()
{
    return tbIcon;
}

void SearchBox::changeEngine(search_t engine)
{
    current = engine;
    setText(current.name);
    //field->setForegroundRole(QPalette::Inactive);
}

void SearchBox::selectionChanged()
{
    if(text() == current.name)
    {
        setText("");
        //field->setForegroundRole(QPalette::Active);
    }
}

void SearchBox::focusInEvent(QFocusEvent *evt)
{
    Q_UNUSED(evt);
    if(text() == current.name && uset == 0)
    {
        int cursorPos = cursorPosition();
        uset = 1;
        setText("");
        QPalette pal;
        pal.setColor(QPalette::Text, Qt::black);
        setPalette(pal);
        end(false);
        setCursorPosition(cursorPos);
    }
}

void SearchBox::focusOutEvent(QFocusEvent *evt)
{
    Q_UNUSED(evt);
    if(text() == "")
    {
        uset = 0;
        setText(current.name);
        QPalette pal;
        pal.setColor(QPalette::Text, Qt::gray);
        setPalette(pal);
    }
}

void SearchBox::returnPressed()
{
    QString ret = current.searchUrl;
    ret.replace("{searchTerms}", this->text());
    QUrl url(ret);
    emit navigateToSearch(url);
}

void SearchBox::menuTriggered(QAction *act)
{
    tbIcon->setIcon(act->icon());
    for(int i = 0; i < engines.count(); ++i)
    {
        if(engines.at(i).name == act->text())
        {
            current = engines.at(i);
            break;
        }
    }

    if(uset == 0)
        setText(current.name);
}
