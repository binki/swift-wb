/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "preferences.h"

PreferencesWindow::PreferencesWindow(QWidget *parent = 0)
    :QDialog(parent)
{
    if (objectName().isEmpty())
        setObjectName(QString::fromUtf8("Dialog"));
    resize(542, 368);

    //Settings *settings = new Settings(this);

    gridLayout_6 = new QGridLayout(this);
    gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
    verticalLayout = new QVBoxLayout();
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    tabWidget = new QTabWidget(this);
    tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
    tabWidget->setSizePolicy(sizePolicy);
    tabWidget->setElideMode(Qt::ElideNone);
    tabWidget->setDocumentMode(false);
    tab_general = new QWidget();
    tab_general->setObjectName(QString::fromUtf8("tab_general"));
    gridLayout_13 = new QGridLayout(tab_general);
    gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
    groupBoxStartup = new QGroupBox(tab_general);
    groupBoxStartup->setObjectName(QString::fromUtf8("groupBoxStartup"));
    gridLayout = new QGridLayout(groupBoxStartup);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    label_Startup = new QLabel(groupBoxStartup);
    label_Startup->setObjectName(QString::fromUtf8("label_Startup"));
    label_Startup->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

    gridLayout->addWidget(label_Startup, 0, 0, 1, 1);

    onStartup = new QComboBox(groupBoxStartup);
    onStartup->setObjectName(QString::fromUtf8("onStartup"));
    onStartup->setEditable(false);

    gridLayout->addWidget(onStartup, 0, 1, 1, 2);

    label_Homepage = new QLabel(groupBoxStartup);
    label_Homepage->setObjectName(QString::fromUtf8("label_Homepage"));
    label_Homepage->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

    gridLayout->addWidget(label_Homepage, 1, 0, 1, 1);

    homePage = new QLineEdit(groupBoxStartup);
    homePage->setObjectName(QString::fromUtf8("homePage"));
    homePage->setReadOnly(false);

    gridLayout->addWidget(homePage, 1, 1, 1, 2);

    setToCurrentPage = new QPushButton(groupBoxStartup);
    setToCurrentPage->setObjectName(QString::fromUtf8("setToCurrentPage"));

    gridLayout->addWidget(setToCurrentPage, 2, 1, 1, 1);

    horizontalSpacer_2 = new QSpacerItem(301, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    gridLayout->addItem(horizontalSpacer_2, 2, 2, 1, 1);


    gridLayout_13->addWidget(groupBoxStartup, 0, 0, 1, 1);

    downloadsGB = new QGroupBox(tab_general);
    downloadsGB->setObjectName(QString::fromUtf8("downloadsGB"));
    gridLayout_12 = new QGridLayout(downloadsGB);
    gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
    showDownloads = new QCheckBox(downloadsGB);
    showDownloads->setObjectName(QString::fromUtf8("showDownloads"));

    gridLayout_12->addWidget(showDownloads, 0, 0, 1, 1);

    hideDownloads = new QCheckBox(downloadsGB);
    hideDownloads->setObjectName(QString::fromUtf8("hideDownloads"));

    gridLayout_12->addWidget(hideDownloads, 1, 0, 1, 1);

    alwaysAsk = new QRadioButton(downloadsGB);
    alwaysAsk->setObjectName(QString::fromUtf8("alwaysAsk"));

    gridLayout_12->addWidget(alwaysAsk, 2, 0, 1, 1);

    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    saveFiles = new QRadioButton(downloadsGB);
    saveFiles->setObjectName(QString::fromUtf8("saveFiles"));
    QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(saveFiles->sizePolicy().hasHeightForWidth());
    saveFiles->setSizePolicy(sizePolicy1);

    horizontalLayout->addWidget(saveFiles);

    saveLocation = new QComboBox(downloadsGB);
    saveLocation->setObjectName(QString::fromUtf8("saveLocation"));
    saveLocation->setEditable(false);
    saveLocation->setInsertPolicy(QComboBox::InsertAtTop);

    horizontalLayout->addWidget(saveLocation);


    gridLayout_12->addLayout(horizontalLayout, 3, 0, 1, 1);


    gridLayout_13->addWidget(downloadsGB, 1, 0, 1, 1);

    verticalSpacer_4 = new QSpacerItem(159, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

    gridLayout_13->addItem(verticalSpacer_4, 2, 0, 1, 1);

    tabWidget->addTab(tab_general, QString());
    tabs = new QWidget();
    tabs->setObjectName(QString::fromUtf8("tabs"));
    gridLayout_11 = new QGridLayout(tabs);
    gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
    groupBoxTabs = new QGroupBox(tabs);
    groupBoxTabs->setObjectName(QString::fromUtf8("groupBoxTabs"));
    gridLayout_3 = new QGridLayout(groupBoxTabs);
    gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
    openNewTab = new QCheckBox(groupBoxTabs);
    openNewTab->setObjectName(QString::fromUtf8("openNewTab"));

    gridLayout_3->addWidget(openNewTab, 0, 0, 1, 1);

    confirmClose = new QCheckBox(groupBoxTabs);
    confirmClose->setObjectName(QString::fromUtf8("confirmClose"));

    gridLayout_3->addWidget(confirmClose, 1, 0, 1, 1);

    alwaysShowTabs = new QCheckBox(groupBoxTabs);
    alwaysShowTabs->setObjectName(QString::fromUtf8("alwaysShowTabs"));

    gridLayout_3->addWidget(alwaysShowTabs, 2, 0, 1, 1);


    gridLayout_11->addWidget(groupBoxTabs, 0, 0, 1, 1);

    verticalSpacer = new QSpacerItem(20, 170, QSizePolicy::Minimum, QSizePolicy::Expanding);

    gridLayout_11->addItem(verticalSpacer, 1, 0, 1, 1);

    tabWidget->addTab(tabs, QString());
    content = new QWidget();
    content->setObjectName(QString::fromUtf8("content"));
    gridLayout_10 = new QGridLayout(content);
    gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
    fontsGB = new QGroupBox(content);
    fontsGB->setObjectName(QString::fromUtf8("fontsGB"));
    gridLayout_2 = new QGridLayout(fontsGB);
    gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
    horizontalLayout_4 = new QHBoxLayout();
    horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
    labelFixedFont = new QLabel(fontsGB);
    labelFixedFont->setObjectName(QString::fromUtf8("labelFixedFont"));

    horizontalLayout_4->addWidget(labelFixedFont);

    fixedFont = new QFontComboBox(fontsGB);
    fixedFont->setObjectName(QString::fromUtf8("fixedFont"));

    horizontalLayout_4->addWidget(fixedFont);

    labelFixedSize = new QLabel(fontsGB);
    labelFixedSize->setObjectName(QString::fromUtf8("labelFixedSize"));

    horizontalLayout_4->addWidget(labelFixedSize);

    fixedSize = new QSpinBox(fontsGB);
    fixedSize->setObjectName(QString::fromUtf8("fixedSize"));
    fixedSize->setValue(12);

    horizontalLayout_4->addWidget(fixedSize);

    horizontalLayout_4->setStretch(1, 1);
    horizontalLayout_4->setStretch(3, 1);

    gridLayout_2->addLayout(horizontalLayout_4, 0, 0, 1, 1);

    horizontalLayout_3 = new QHBoxLayout();
    horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
    labelDefault = new QLabel(fontsGB);
    labelDefault->setObjectName(QString::fromUtf8("labelDefault"));

    horizontalLayout_3->addWidget(labelDefault);

    standardFont = new QFontComboBox(fontsGB);
    standardFont->setObjectName(QString::fromUtf8("standardFont"));

    horizontalLayout_3->addWidget(standardFont);

    labelStdSize = new QLabel(fontsGB);
    labelStdSize->setObjectName(QString::fromUtf8("labelStdSize"));

    horizontalLayout_3->addWidget(labelStdSize);

    standardSize = new QSpinBox(fontsGB);
    standardSize->setObjectName(QString::fromUtf8("standardSize"));
    standardSize->setValue(12);

    horizontalLayout_3->addWidget(standardSize);

    horizontalLayout_3->setStretch(1, 1);
    horizontalLayout_3->setStretch(3, 1);

    gridLayout_2->addLayout(horizontalLayout_3, 1, 0, 1, 1);


    gridLayout_10->addWidget(fontsGB, 0, 0, 1, 1);

    useCustomStylesheet = new QGroupBox(content);
    useCustomStylesheet->setObjectName(QString::fromUtf8("useCustomStylesheet"));
    useCustomStylesheet->setCheckable(true);
    useCustomStylesheet->setChecked(false);
    gridLayout_9 = new QGridLayout(useCustomStylesheet);
    gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
    userStylesheetPath = new QLineEdit(useCustomStylesheet);
    userStylesheetPath->setObjectName(QString::fromUtf8("userStylesheetPath"));

    gridLayout_9->addWidget(userStylesheetPath, 0, 0, 1, 1);

    browseStylesheet = new QPushButton(useCustomStylesheet);
    browseStylesheet->setObjectName(QString::fromUtf8("browseStylesheet"));

    gridLayout_9->addWidget(browseStylesheet, 0, 1, 1, 1);


    gridLayout_10->addWidget(useCustomStylesheet, 1, 0, 1, 1);

    verticalSpacer_2 = new QSpacerItem(20, 121, QSizePolicy::Minimum, QSizePolicy::Expanding);

    gridLayout_10->addItem(verticalSpacer_2, 2, 0, 1, 1);

    tabWidget->addTab(content, QString());
    zonestab = new QWidget();
    zonestab->setObjectName(QString::fromUtf8("zonestab"));
    gridLayout_5 = new QGridLayout(zonestab);
    gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
    horizontalLayout_2 = new QHBoxLayout();
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    labelChange = new QLabel(zonestab);
    labelChange->setObjectName(QString::fromUtf8("labelChange"));
    QFont font;
    font.setBold(false);
    font.setWeight(50);
    labelChange->setFont(font);

    horizontalLayout_2->addWidget(labelChange);

    zones = new QComboBox(zonestab);
    zones->addItem(QIcon(QWebSettings::webGraphic(QWebSettings::DefaultFrameIconGraphic)), QString());
    zones->addItem(QIcon(QString::fromUtf8(":/Swift/trusted")), QString());
    zones->addItem(QIcon(QString::fromUtf8(":/Swift/restricted")), QString());
    zones->setObjectName(QString::fromUtf8("zones"));
    QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    sizePolicy2.setHorizontalStretch(0);
    sizePolicy2.setVerticalStretch(0);
    sizePolicy2.setHeightForWidth(zones->sizePolicy().hasHeightForWidth());
    zones->setSizePolicy(sizePolicy2);

    horizontalLayout_2->addWidget(zones);


    gridLayout_5->addLayout(horizontalLayout_2, 0, 0, 1, 3);

    zoneSettings = new QTreeWidget(zonestab);
    QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem1->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem2->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem3->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem4->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem5->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem6->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem7->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem8->setCheckState(0, Qt::Unchecked);
    QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(zoneSettings);
    __qtreewidgetitem9->setCheckState(0, Qt::Unchecked);
    zoneSettings->setObjectName(QString::fromUtf8("zoneSettings"));
    zoneSettings->setIndentation(0);
    zoneSettings->header()->setVisible(false);
    loadZoneSettings(ZoneManager::Internet);

    gridLayout_5->addWidget(zoneSettings, 1, 0, 1, 3);

    horizontalSpacer = new QSpacerItem(291, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    gridLayout_5->addItem(horizontalSpacer, 2, 0, 1, 1);

    addRemove = new QPushButton(zonestab);
    addRemove->setObjectName(QString::fromUtf8("addRemove"));
    connect(addRemove, SIGNAL(pressed()), this, SLOT(addRemoveZone()));
    addRemove->setEnabled(false);

    gridLayout_5->addWidget(addRemove, 2, 1, 1, 1);

    zoneHelp = new QPushButton(zonestab);
    zoneHelp->setObjectName(QString::fromUtf8("zoneHelp"));
    QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
    sizePolicy3.setHorizontalStretch(0);
    sizePolicy3.setVerticalStretch(0);
    sizePolicy3.setHeightForWidth(zoneHelp->sizePolicy().hasHeightForWidth());
    zoneHelp->setSizePolicy(sizePolicy3);

    gridLayout_5->addWidget(zoneHelp, 2, 2, 1, 1);

    tabWidget->addTab(zonestab, QString());
    advanced = new QWidget();
    advanced->setObjectName(QString::fromUtf8("advanced"));
    gridLayout_8 = new QGridLayout(advanced);
    gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
    proxyGroup = new QGroupBox(advanced);
    proxyGroup->setObjectName(QString::fromUtf8("proxyGroup"));
    proxyGroup->setCheckable(true);
    proxyGroup->setChecked(false);
    gridLayout_4 = new QGridLayout(proxyGroup);
    gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
    proxy = new QComboBox(proxyGroup);
    proxy->setObjectName(QString::fromUtf8("proxy"));
    sizePolicy2.setHeightForWidth(proxy->sizePolicy().hasHeightForWidth());
    proxy->setSizePolicy(sizePolicy2);

    gridLayout_4->addWidget(proxy, 0, 0, 1, 2);

    horizontalSpacer_4 = new QSpacerItem(66, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    gridLayout_4->addItem(horizontalSpacer_4, 0, 4, 1, 1);

    labelType = new QLabel(proxyGroup);
    labelType->setObjectName(QString::fromUtf8("labelType"));

    gridLayout_4->addWidget(labelType, 1, 0, 1, 1);

    proxyType = new QComboBox(proxyGroup);
    proxyType->setObjectName(QString::fromUtf8("proxyType"));

    gridLayout_4->addWidget(proxyType, 1, 1, 1, 1);

    labelHostname = new QLabel(proxyGroup);
    labelHostname->setObjectName(QString::fromUtf8("labelHostname"));

    gridLayout_4->addWidget(labelHostname, 1, 2, 1, 1);

    proxyHost = new QLineEdit(proxyGroup);
    proxyHost->setObjectName(QString::fromUtf8("proxyHost"));

    gridLayout_4->addWidget(proxyHost, 1, 3, 1, 3);

    labelPort = new QLabel(proxyGroup);
    labelPort->setObjectName(QString::fromUtf8("labelPort"));

    gridLayout_4->addWidget(labelPort, 1, 6, 1, 1);

    proxyPort = new QSpinBox(proxyGroup);
    proxyPort->setObjectName(QString::fromUtf8("proxyPort"));
    proxyPort->setMinimum(10);
    proxyPort->setMaximum(10000);
    proxyPort->setValue(8080);

    gridLayout_4->addWidget(proxyPort, 1, 7, 1, 1);

    labelUName = new QLabel(proxyGroup);
    labelUName->setObjectName(QString::fromUtf8("labelUName"));

    gridLayout_4->addWidget(labelUName, 2, 0, 1, 1);

    proxyUser = new QLineEdit(proxyGroup);
    proxyUser->setObjectName(QString::fromUtf8("proxyUser"));

    gridLayout_4->addWidget(proxyUser, 2, 1, 1, 3);

    labelPass = new QLabel(proxyGroup);
    labelPass->setObjectName(QString::fromUtf8("labelPass"));

    gridLayout_4->addWidget(labelPass, 2, 5, 1, 1);

    proxyPass = new QLineEdit(proxyGroup);
    proxyPass->setObjectName(QString::fromUtf8("proxyPass"));
    proxyPass->setEchoMode(QLineEdit::Password);

    gridLayout_4->addWidget(proxyPass, 2, 6, 1, 2);


    gridLayout_8->addWidget(proxyGroup, 0, 0, 1, 1);

    additionalSettingsGB = new QGroupBox(advanced);
    additionalSettingsGB->setObjectName(QString::fromUtf8("additionalSettingsGB"));
    additionalSettingsGB->setCheckable(false);
    gridLayout_7 = new QGridLayout(additionalSettingsGB);
    gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
    labelMaxCache = new QLabel(additionalSettingsGB);
    labelMaxCache->setObjectName(QString::fromUtf8("labelMaxCache"));

    gridLayout_7->addWidget(labelMaxCache, 0, 0, 1, 1);

    spinBox = new QSpinBox(additionalSettingsGB);
    spinBox->setObjectName(QString::fromUtf8("spinBox"));
    spinBox->setMaximum(499);
    spinBox->setValue(50);

    gridLayout_7->addWidget(spinBox, 0, 1, 1, 1);

    horizontalSpacer_3 = new QSpacerItem(304, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    gridLayout_7->addItem(horizontalSpacer_3, 0, 2, 1, 1);

    defaultBrowser = new QCheckBox(additionalSettingsGB);
    defaultBrowser->setObjectName(QString::fromUtf8("defaultBrowser"));

    gridLayout_7->addWidget(defaultBrowser, 1, 0, 1, 3);


    gridLayout_8->addWidget(additionalSettingsGB, 1, 0, 1, 1);

    verticalSpacer_3 = new QSpacerItem(20, 73, QSizePolicy::Minimum, QSizePolicy::Expanding);

    gridLayout_8->addItem(verticalSpacer_3, 2, 0, 1, 1);

    tabWidget->addTab(advanced, QString());

    verticalLayout->addWidget(tabWidget);

    buttonBox = new QDialogButtonBox(this);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    sizePolicy2.setHeightForWidth(buttonBox->sizePolicy().hasHeightForWidth());
    buttonBox->setSizePolicy(sizePolicy2);
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    verticalLayout->addWidget(buttonBox);
    gridLayout_6->addLayout(verticalLayout, 0, 0, 1, 1);
    retranslate();
    QObject::connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    connect(zones, SIGNAL(currentIndexChanged(int)), this, SLOT(zoneIndexChanged(int)));
    loadSettings();
}

void PreferencesWindow::retranslate()
{
    setWindowTitle(tr("Swift Preferences"));
    groupBoxStartup->setTitle(QApplication::translate("Dialog", "Startup Behavior", 0, QApplication::UnicodeUTF8));
    downloadsGB->setTitle(QApplication::translate("Dialog", "Downloads", 0, QApplication::UnicodeUTF8));
    downloadsGB->setTitle(QApplication::translate("Dialog", "Downloads", 0, QApplication::UnicodeUTF8));
    groupBoxTabs->setTitle(QApplication::translate("Dialog", "Tab Behavior", 0, QApplication::UnicodeUTF8));
    fontsGB->setTitle(QApplication::translate("Dialog", "Fonts", 0, QApplication::UnicodeUTF8));
    proxyGroup->setTitle(QApplication::translate("Dialog", "Use a Proxy Server", 0, QApplication::UnicodeUTF8));
    useCustomStylesheet->setTitle(QApplication::translate("Dialog", "Use a Custom Stylesheet", 0, QApplication::UnicodeUTF8));
	additionalSettingsGB->setTitle(QApplication::translate("Dialog", "Additional Settings", 0, QApplication::UnicodeUTF8));
    label_Startup->setText(QApplication::translate("this", "On Startup:", 0, QApplication::UnicodeUTF8));
    onStartup->clear();
    onStartup->insertItems(0, QStringList()
     << QApplication::translate("Dialog", "Show My Homepage", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("Dialog", "Show a Blank Page", 0, QApplication::UnicodeUTF8)
     /* << QApplication::translate("Dialog", "Show Speed Dial", 0, QApplication::UnicodeUTF8) */
     << QApplication::translate("Dialog", "Restore My Previous Tabs", 0, QApplication::UnicodeUTF8)
    );
    label_Homepage->setText(QApplication::translate("Dialog", "Home Page:", 0, QApplication::UnicodeUTF8));
    setToCurrentPage->setText(QApplication::translate("Dialog", "Set to Current Page", 0, QApplication::UnicodeUTF8));
    showDownloads->setText(QApplication::translate("Dialog", "Show Downloads window when downloading a file.", 0, QApplication::UnicodeUTF8));
    hideDownloads->setText(QApplication::translate("Dialog", "Hide downloads window when all downloads are completed.", 0, QApplication::UnicodeUTF8));
    alwaysAsk->setText(QApplication::translate("Dialog", "Always ask me where to save files", 0, QApplication::UnicodeUTF8));
    saveFiles->setText(QApplication::translate("Dialog", "Save files to: ", 0, QApplication::UnicodeUTF8));
    saveLocation->clear();
    saveLocation->insertItems(0, QStringList()
     << QApplication::translate("Dialog", "Select Location", 0, QApplication::UnicodeUTF8)
    );
    tabWidget->setTabText(tabWidget->indexOf(tab_general), QApplication::translate("Dialog", "General", 0, QApplication::UnicodeUTF8));
    openNewTab->setText(QApplication::translate("Dialog", "Open new windows in a new tab instead.", 0, QApplication::UnicodeUTF8));
    confirmClose->setText(QApplication::translate("Dialog", "Confirm when closing multiple tabs.", 0, QApplication::UnicodeUTF8));
    alwaysShowTabs->setText(QApplication::translate("Dialog", "Always show the tab bar", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(tabs), QApplication::translate("Dialog", "Tabs", 0, QApplication::UnicodeUTF8));
    labelDefault->setText(QApplication::translate("Dialog", "Standard Font:", 0, QApplication::UnicodeUTF8));
    labelStdSize->setText(QApplication::translate("Dialog", "Size", 0, QApplication::UnicodeUTF8));
    labelFixedFont->setText(QApplication::translate("Dialog", "Fixed Width Font", 0, QApplication::UnicodeUTF8));
    labelFixedSize->setText(QApplication::translate("Dialog", "Size", 0, QApplication::UnicodeUTF8));
    browseStylesheet->setText(QApplication::translate("Dialog", "Browse", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(content), QApplication::translate("Dialog", "Content", 0, QApplication::UnicodeUTF8));
    labelChange->setText(QApplication::translate("Dialog", "Change Settings For:", 0, QApplication::UnicodeUTF8));
    zones->setItemText(0, QApplication::translate("Dialog", "Internet", 0, QApplication::UnicodeUTF8));
    zones->setItemText(1, QApplication::translate("Dialog", "Trusted Sites", 0, QApplication::UnicodeUTF8));
    zones->setItemText(2, QApplication::translate("Dialog", "Restricted Sites", 0, QApplication::UnicodeUTF8));

    QTreeWidgetItem *___qtreewidgetitem = zoneSettings->headerItem();
    ___qtreewidgetitem->setText(0, QApplication::translate("Dialog", "Configuration", 0, QApplication::UnicodeUTF8));

    const bool __sortingEnabled = zoneSettings->isSortingEnabled();
    zoneSettings->setSortingEnabled(false);
    QTreeWidgetItem *___qtreewidgetitem1 = zoneSettings->topLevelItem(0);
    ___qtreewidgetitem1->setText(0, QApplication::translate("Dialog", "Allow usage of NPAPI plugins", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem2 = zoneSettings->topLevelItem(1);
    ___qtreewidgetitem2->setText(0, QApplication::translate("Dialog", "Allow Javascript", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem3 = zoneSettings->topLevelItem(2);
    ___qtreewidgetitem3->setText(0, QApplication::translate("Dialog", "Allow spawning of new windows", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem4 = zoneSettings->topLevelItem(3);
    ___qtreewidgetitem4->setText(0, QApplication::translate("Dialog", "Allow clipboard access", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem5 = zoneSettings->topLevelItem(4);
    ___qtreewidgetitem5->setText(0, QApplication::translate("Dialog", "Allow status bar updates", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem6 = zoneSettings->topLevelItem(5);
    ___qtreewidgetitem6->setText(0, QApplication::translate("Dialog", "Allow file downloads", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem7 = zoneSettings->topLevelItem(6);
    ___qtreewidgetitem7->setText(0, QApplication::translate("Dialog", "Enable AdBlock", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem8 = zoneSettings->topLevelItem(7);
    ___qtreewidgetitem8->setText(0, QApplication::translate("Dialog", "Allow excessive Javascript Message Boxes", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem9 = zoneSettings->topLevelItem(8);
    ___qtreewidgetitem9->setText(0, QApplication::translate("Dialog", "Allow user initiated popup wndows", 0, QApplication::UnicodeUTF8));
    QTreeWidgetItem *___qtreewidgetitem10 = zoneSettings->topLevelItem(9);
    ___qtreewidgetitem10->setText(0, QApplication::translate("Dialog", "Allow other popup windows", 0, QApplication::UnicodeUTF8));
    zoneSettings->setSortingEnabled(__sortingEnabled);

    addRemove->setText(QApplication::translate("Dialog", "Add/Remove Websites", 0, QApplication::UnicodeUTF8));
    zoneHelp->setText(QApplication::translate("Dialog", "Zones Help", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(zonestab), QApplication::translate("Dialog", "Zones", 0, QApplication::UnicodeUTF8));
    proxy->clear();
    proxy->insertItems(0, QStringList()
     << QApplication::translate("Dialog", "Use system default proxy", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("Dialog", "Use custom proxy", 0, QApplication::UnicodeUTF8)
    );
    labelType->setText(QApplication::translate("Dialog", "Type:", 0, QApplication::UnicodeUTF8));
    proxyType->clear();
    proxyType->insertItems(0, QStringList()
     << QApplication::translate("Dialog", "SOCKS5", 0, QApplication::UnicodeUTF8)
     << QApplication::translate("Dialog", "HTTP", 0, QApplication::UnicodeUTF8)
    );
    labelHostname->setText(QApplication::translate("Dialog", "Host:", 0, QApplication::UnicodeUTF8));
    labelPort->setText(QApplication::translate("Dialog", "Port:", 0, QApplication::UnicodeUTF8));
    labelUName->setText(QApplication::translate("Dialog", "User Name:", 0, QApplication::UnicodeUTF8));
    labelPass->setText(QApplication::translate("Dialog", "Password:", 0, QApplication::UnicodeUTF8));
    labelMaxCache->setText(QApplication::translate("Dialog", "Maximum Cache Size:", 0, QApplication::UnicodeUTF8));
    spinBox->setSpecialValueText(QString());
    spinBox->setSuffix(QApplication::translate("Dialog", " MB", 0, QApplication::UnicodeUTF8));
    defaultBrowser->setText(QApplication::translate("Dialog", "Upon Startup, check to see if Swift is the default browser.", 0, QApplication::UnicodeUTF8));
    tabWidget->setTabText(tabWidget->indexOf(advanced), QApplication::translate("Dialog", "Advanced", 0, QApplication::UnicodeUTF8));
}

void PreferencesWindow::saveZoneSettings(ZoneManager::Zone zone)
{
    Settings settings(this);
    settings.beginGroup("Zones");
    switch (zone) {
        case ZoneManager::Internet:
            settings.beginGroup("Internet");
            break;
        case ZoneManager::Trusted:
            settings.beginGroup("TrustedSites");
            break;
        case ZoneManager::Restricted:
            settings.beginGroup("RestrictedSites");
            break;
    }

    settings.setValue("AllowNPAPI", (zoneSettings->topLevelItem(0)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowJavaScript", (zoneSettings->topLevelItem(1)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowWindowSpawns", (zoneSettings->topLevelItem(2)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowClipboard", (zoneSettings->topLevelItem(3)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowStatusBarUpdates", (zoneSettings->topLevelItem(4)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowFileDownloads", (zoneSettings->topLevelItem(5)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("EnableAdBlock", (zoneSettings->topLevelItem(6)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowExcessiveJSMessages", (zoneSettings->topLevelItem(7)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowUserPopups", (zoneSettings->topLevelItem(8)->checkState(0) == Qt::Checked ? true : false));
    settings.setValue("AllowOtherPopups", (zoneSettings->topLevelItem(9)->checkState(0) == Qt::Checked ? true : false));

    settings.endGroup();
    settings.endGroup();
}

void PreferencesWindow::loadZoneSettings(ZoneManager::Zone zone)
{
    Settings settings(this);
    settings.beginGroup("Zones");
    switch (zone) {
        case ZoneManager::Internet:
            settings.beginGroup("Internet");
            break;
        case ZoneManager::Trusted:
            settings.beginGroup("TrustedSites");
            break;
        case ZoneManager::Restricted:
            settings.beginGroup("RestrictedSites");
            break;
    }

    zoneSettings->topLevelItem(0)->setCheckState(0, (settings.value("AllowNPAPI").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(1)->setCheckState(0, (settings.value("AllowJavaScript").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(2)->setCheckState(0, (settings.value("AllowWindowSpawns").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(3)->setCheckState(0, (settings.value("AllowClipboard").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(4)->setCheckState(0, (settings.value("AllowStatusBarUpdates").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(5)->setCheckState(0, (settings.value("AllowFileDownloads").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(6)->setCheckState(0, (settings.value("EnableAdBlock").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(7)->setCheckState(0, (settings.value("AllowExcessiveJSMessages").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(8)->setCheckState(0, (settings.value("AllowUserPopups").toBool() == true ? Qt::Checked : Qt::Unchecked));
    zoneSettings->topLevelItem(9)->setCheckState(0, (settings.value("AllowOtherPopups").toBool() == true ? Qt::Checked : Qt::Unchecked));

    settings.endGroup();
    settings.endGroup();
}

void PreferencesWindow::zoneIndexChanged(int index)
{
    saveZoneSettings(m_selectedZone);
    switch(index) {
        case 0:
            m_selectedZone = ZoneManager::Internet;
            break;
        case 1:
            m_selectedZone = ZoneManager::Trusted;
            break;
        case 2:
            m_selectedZone = ZoneManager::Restricted;
            break;
    }
    loadZoneSettings(m_selectedZone);
    if(m_selectedZone == ZoneManager::Internet)
    {
        addRemove->setEnabled(false);
    } 
    else
    {
        addRemove->setEnabled(true);
    }

}

void PreferencesWindow::loadSettings()
{
    /* General */
    settings = new Settings(this);
    homePage->setText(settings->value("Homepage").toString());
    onStartup->setCurrentIndex(settings->value("StartupAction").toInt());
    showDownloads->setChecked(settings->value("ShowDownloadsWindow").toBool());
    hideDownloads->setChecked(settings->value("HideDownloadsWindow").toBool());
    settings->value("AlwaysAsk").toBool() == true ?
            alwaysAsk->setChecked(true) : saveFiles->setChecked(true);

    /* Tab Options */
    confirmClose->setChecked(settings->value("ConfirmClose").toBool());
    alwaysShowTabs->setChecked(settings->value("AlwaysShowTabs").toBool());
    openNewTab->setChecked(settings->value("OpenNewTab").toBool());

    /* advanced */
    proxy->setCurrentIndex(settings->value("NetworkProxyToUse").toInt());
    proxyGroup->setChecked(settings->value("UseNetworkProxy").toBool());
    proxyHost->setText(settings->value("NetworkProxyHost").toString());
    proxyPort->setValue(settings->value("NetworkProxyPort").toInt());
    proxyUser->setText(settings->value("NetworkProxyUser").toString());
    proxyPass->setText(settings->value("NetworkProxyPass").toString());
    proxyType->setCurrentIndex(settings->value("NetworkProxyType").toInt());
    defaultBrowser->setChecked(settings->value("CheckForDefault").toBool());
    spinBox->setValue(settings->value("MaximumCache").toInt());
}

void PreferencesWindow::saveSettings()
{
    /* General */
    settings->setValue("Homepage", homePage->text());
    settings->setValue("StartupAction", onStartup->currentIndex());
    if(showDownloads->checkState() == Qt::Checked)
        settings->setValue("ShowDownloadsWindow", true);
    else
        settings->setValue("ShowDownloadsWindow", false);

    if(hideDownloads->checkState() == Qt::Checked)
        settings->setValue("HideDownloadsWindow", true);
    else
        settings->setValue("HideDownloadsWindow", false);

    settings->setValue("AlwaysAsk", (alwaysAsk->isChecked() == true ? true : false));

    /* Tab Options */
    settings->setValue("ConfirmClose", confirmClose->isChecked());
    settings->setValue("AlwaysShowTabs", alwaysShowTabs->isChecked());
    settings->setValue("OpenNewTab", openNewTab->isChecked());

    /* advanced */
    settings->setValue("NetworkProxyToUse", proxy->currentIndex());
    settings->setValue("UseNetworkProxy", proxyGroup->isChecked());
    settings->setValue("NetworkProxyHost", proxyHost->text());
    settings->setValue("NetworkProxyPort", proxyPort->text());
    settings->setValue("NetworkProxyUser", proxyUser->text());
    settings->setValue("NetworkProxyPass", proxyPass->text());
    settings->setValue("NetworkProxyType", proxyType->currentIndex());
    settings->setValue("CheckForDefault", defaultBrowser->isChecked());
    settings->setValue("MaximumCache", spinBox->value());
}

void PreferencesWindow::accept()
{
    saveSettings();
    this->close();
}

void PreferencesWindow::addRemoveZone()
{
    EditZoneDialog *dlg = new EditZoneDialog(this, m_selectedZone, "");
    if(dlg->exec()) { }
    delete dlg;
}
