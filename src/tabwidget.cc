/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "tabwidget.h"

TabBar::TabBar(QWidget *parent)
    : QTabBar(parent)
{
    setMovable(true);
    setElideMode(Qt::ElideMiddle);

    createContextMenu();

    /* create an end new tab "button" */
}

TabBar::~TabBar()
{

}

void TabBar::createContextMenu()
{
    tabContextMenu = new QMenu(this);
    newTab = new QAction(tr("New Tab"), this);
    closeTab = new QAction(tr("Close Tab"), this);
    closeAllTabs = new QAction(tr("Close Other Tabs"), this);
    closeTabsOnRight = new QAction(tr("Close Tabs on Right"), this);
    closeTabsOnLeft = new QAction(tr("Close Tabs on left"), this);
    reloadTab = new QAction(tr("Reload Tab"), this);
    reloadAllTabs = new QAction(tr("Reload All Tabs"), this);

    tabContextMenu->addAction(newTab);
    tabContextMenu->addSeparator();
    tabContextMenu->addAction(closeTab);
    tabContextMenu->addAction(closeAllTabs);
    tabContextMenu->addAction(closeTabsOnRight);
    tabContextMenu->addAction(closeTabsOnLeft);
    tabContextMenu->addSeparator();
    tabContextMenu->addAction(reloadTab);
    tabContextMenu->addAction(reloadAllTabs);

    connect(newTab, SIGNAL(triggered()), this, SIGNAL(newTabRequested()));
    connect(closeTab, SIGNAL(triggered()), this, SLOT(closeTabRequested()));
    connect(closeAllTabs, SIGNAL(triggered()), this, SLOT(closeAllTabsSlot()));
    connect(reloadAllTabs, SIGNAL(triggered()), this, SIGNAL(allReloadRequested()));
    connect(reloadTab, SIGNAL(triggered()), this, SLOT(reloadTabSlot()));
    connect(closeTabsOnRight, SIGNAL(triggered()), this, SLOT(closeToRight()));
    connect(closeTabsOnLeft, SIGNAL(triggered()), this, SLOT(closeToLeft()));
}

void TabBar::contextMenuEvent(QContextMenuEvent *event)
{
    closeTabsOnLeft->setEnabled(true);
    closeTabsOnRight->setEnabled(true);
    closeAllTabs->setEnabled(true);
    reloadAllTabs->setEnabled(true);
    closeTab->setEnabled(true);

    if(tabAt(event->pos()) >= 0)
    {
        if(tabAt(event->pos()) == 0)
            closeTabsOnLeft->setEnabled(false);

        if(tabAt(event->pos()) == count()-1)
            closeTabsOnRight->setEnabled(false);

        if(count() == 1)
        {
            closeAllTabs->setEnabled(false);
            closeTab->setEnabled(false);
            reloadAllTabs->setEnabled(false);
        }

        m_lastUsedPoint = event->pos();
        tabContextMenu->popup(QWidget::mapToGlobal(event->pos()));
    }
}

void TabBar::closeTabRequested()
{
    emit closeTabById(tabAt(m_lastUsedPoint));
}

void TabBar::closeAllTabsSlot()
{
    emit closeAllTabsButID(tabAt(m_lastUsedPoint));
}

void TabBar::reloadTabSlot()
{
    emit tabReloadRequested(tabAt(m_lastUsedPoint));
}

void TabBar::closeToLeft()
{
    emit closeLeftRequested(tabAt(m_lastUsedPoint));
}

void TabBar::closeToRight()
{
    emit closeRightRequested(tabAt(m_lastUsedPoint));
}

TabWidget::TabWidget(QWidget *parent)
    : QTabWidget(parent)
{
    dl = new DownloadsWindow(this);
    b = new TabBar(this);
    setTabBar(b);
    Settings settings(this);
    
    connect(b, SIGNAL(newTabRequested()), this, SLOT(newTabRequsted()));
    connect(b, SIGNAL(tabMoved(int,int)), this, SLOT(tabMoved(int, int)));
    connect(b, SIGNAL(closeTabById(int)), this, SLOT(tabCloseRequested(int)));
    connect(b, SIGNAL(closeAllTabsButID(int)), this, SLOT(closeAllButID(int)));
    connect(b, SIGNAL(closeLeftRequested(int)), this, SLOT(closeLeftRequested(int)));
    connect(b, SIGNAL(closeRightRequested(int)), this, SLOT(closeRightRequested(int)));
    connect(b, SIGNAL(allReloadRequested()), this, SLOT(allReloadRequested()));
    connect(b, SIGNAL(tabReloadRequested(int)), this, SLOT(tabReloadRequested(int)));
}

TabWidget::~TabWidget()
{
    delete dl;
}

WebView* TabWidget::newTab(bool m_private = false)
{
    return newTab(QUrl("about:blank"), m_private);
}

void TabWidget::newTabRequested()
{
    newTab();
}

void TabWidget::tabReloadRequested(int index)
{
    webView(index)->reload();
}

void TabWidget::allReloadRequested()
{
    for(int i = 0; i < count(); ++i)
    {
        webView(i)->reload();
    }
}

void TabWidget::closeRightRequested(int index)
{
    for(int i = count() - 1; i > index; i--)
    {
        closeTab(i);
    }
    reorderWebViewTabID();
}

void TabWidget::closeLeftRequested(int index)
{
    int curIndex = index;
    while(curIndex != 0)
    {
        closeTab(0);
        curIndex--;
    }

    reorderWebViewTabID();
}

void TabWidget::closeAllButID(int index)
{
    closeRightRequested(index);
    closeLeftRequested(index);
}

WebView* TabWidget::newTab(QUrl url = QUrl("about:blank"), bool m_private = false)
{
    WebView *webView = new WebView(this, 0, m_private);
    if(m_private == true)
        webView->settings()->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);
    QString iconPath = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    QDir dir(iconPath);

    if(!dir.exists())
        dir.mkpath(iconPath);

    if(m_private == true)
        webView->settings()->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);

    webView->settings()->setIconDatabasePath(iconPath);
    if(url != QUrl("about:blank"))
        webView->load(url);
    int index = addTab(webView, "Untitled");
    webView->setTabIndex(index);
    this->tabBar()->setCurrentIndex(index);
    webView->activateWindow();

    Settings settings(this);
    if(settings.value("AlwaysShowTabs").toBool() == false)
        this->tabBar()->show();

    /* Connect our Signals */
    connect(webView, SIGNAL(locationChanged(int, const QUrl)), this, SLOT(locationChanged(int, const QUrl)));
    connect(webView, SIGNAL(loadStartedOnTab(int)), this, SLOT(loadStartedOnTab(int)));
    connect(webView, SIGNAL(loadProgressOnTab(int, int)), this, SLOT(loadProgressOnTab(int, int)));
    connect(webView, SIGNAL(loadFinishedOnTab(int, bool)), this, SLOT(loadFinishedOnTab(int, bool)));
    connect(webView, SIGNAL(urlClickedOnTab(int, const QUrl&)), this, SLOT(urlClickedOnTab(int, const QUrl&)));
    connect(webView, SIGNAL(download(DownloadItem*)), this, SLOT(download(DownloadItem*)));
    connect(webView, SIGNAL(linkHoveredOnTab(int, QString, QString)), this,
            SLOT(linkHoveredOnTab(int, QString, QString)));
    connect(webView, SIGNAL(switchToTab(int)), this, SLOT(switchToTab(int)));
    connect(webView, SIGNAL(titleChangedOnTab(int,QString)), this, SLOT(titleChangedOnTab(int,QString)));

    setTabIcon(index, QIcon(QWebSettings::webGraphic(QWebSettings::DefaultFrameIconGraphic)));

    if(b->count() != 1 && !tabsClosable())
    {
        connect(b, SIGNAL(tabCloseRequested(int)), this, SLOT(tabCloseRequested(int)));
        setTabsClosable(true);
    }

    if(settings.value("AlwaysShowTabs").toBool() == false && b->count() == 1)
        b->hide();

    return webView;
}

WebView* TabWidget::currentWebView()
{
    return qobject_cast<WebView*>(this->currentWidget());
}

void TabWidget::closeTab(int index)
{
    emit trashedTab(webView(index)->title(), webView(index)->url());
    QWidget *idx = widget(index);
    idx->setParent(0);
    delete idx;
}

void TabWidget::tabCloseRequested(int index)
{
    closeTab(index);
}

void TabWidget::locationChanged(int tabID, const QUrl url)
{
    if(currentIndex() == tabID)
        emit updateLocation(url);
}

WebView* TabWidget::webView(int index)
{
    return qobject_cast<WebView*>(this->widget(index));
}

void TabWidget::loadStartedOnTab(int tabID)
{
    if(currentIndex() == tabID)
    {
        emit loadStarted();
        emit updateActions();
    }
}

void TabWidget::goBack()
{
    currentWebView()->triggerPageAction(QWebPage::Back);
}

void TabWidget::goForward()
{
    currentWebView()->triggerPageAction(QWebPage::Forward);
}

void TabWidget::stop()
{
    currentWebView()->triggerPageAction(QWebPage::Stop);
}

void TabWidget::reload()
{
    currentWebView()->triggerPageAction(QWebPage::Reload);
}

void TabWidget::loadProgressOnTab(int tabID, int progress)
{
    if(tabID == currentIndex())
        emit loadProgress(progress);
}

void TabWidget::urlClickedOnTab(int tabID, const QUrl url)
{
    if(tabID == currentIndex())
        emit urlClicked(url);
}

void TabWidget::loadFinishedOnTab(int tabID, bool ok)
{
    iconChangedOnTab(tabID);
    if(tabID == currentIndex())
        emit loadFinished(ok);
}

void TabWidget::linkHoveredOnTab(int tabID, QString url, QString text)
{
    if(tabID == currentIndex())
        emit linkHovered(url, text);
}

void TabWidget::reorderWebViewTabID()
{
    for(int i = 0; i < count(); ++i)
    {
        /* this might cause leaks - beware */
        webView(i)->setTabIndex(i);
    }
}

void TabWidget::tabRemoved(int index)
{
    Q_UNUSED(index);
    Settings settings(this);

    if(count() == 1)
    {
        disconnect(b, SIGNAL(tabCloseRequested(int)));
        setTabsClosable(false);

        if(settings.value("AlwaysShowTabs").toBool() == false)
            this->tabBar()->hide();
    }

    reorderWebViewTabID();
}

void TabWidget::switchToTab(int tab)
{
    setCurrentIndex(tab);
    emit raiseParent();
}

void TabWidget::showDownloadsWindow()
{
    dl->show();
}

void TabWidget::download(DownloadItem *item)
{
    if(!dl->isVisible())
        dl->show();

    dl->addItem(item);
}

void TabWidget::iconChangedOnTab(int tabID)
{
    if(webView(tabID)->icon().isNull())
        setTabIcon(tabID, QIcon(QWebSettings::webGraphic(QWebSettings::DefaultFrameIconGraphic)));
    else
        setTabIcon(tabID, webView(tabID)->settings()->iconForUrl(webView(tabID)->url()));
    if(tabID == currentIndex())
        emit updateIcon();
}

void TabWidget::tabMoved(int from, int to)
{
    Q_UNUSED(from);
    Q_UNUSED(to);
    reorderWebViewTabID();
}

void TabWidget::titleChangedOnTab(int tabID, QString title)
{
    setTabText(tabID, title);
    if(tabID == currentIndex())
        emit(titleChanged(title));
}
