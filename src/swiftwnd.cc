/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "swiftwnd.h"

SwiftMainWindow::SwiftMainWindow(QWidget *parent, Qt::WFlags flags, bool isPrivate, bool isPopup, QUrl url)
    : QMainWindow(parent, flags),
    m_private(isPrivate),
    m_popup(isPopup)
{
    settings = new Settings(this);
    if(!settings->value("LastWindowRect").isNull())
        restoreGeometry(settings->value("LastWindowRect").toByteArray());
    else
        resize(600, 400);
    checkForHistoryDatabaseExistance();

    setWindowIcon(QIcon(QString::fromUtf8(":/Swift/swift-logo")));

    setupActions();

    setupTabWidget();
    setupZoomButton();
    setupStatusBar();

    mainToolBar = new QToolBar(this);
    mainToolBar->setWindowTitle(tr("Toolbar"));
    mainToolBar->setFloatable(false);
    mainToolBar->setMovable(false);
    mainToolBar->setIconSize(QSize(16, 16));
    addToolBar(Qt::TopToolBarArea, mainToolBar);
    if(settings->toolbarShown() == false) {
        mainToolBar->setVisible(false);
    } else {
        mainToolBar->setVisible(true);
    }

    addToolBarBreak(Qt::TopToolBarArea);

    bookmarkBar = new QToolBar(this);
    bookmarkBar->setWindowTitle(tr("Bookmark Bar"));
    bookmarkBar->setFloatable(false);
    bookmarkBar->setMovable(false);
    bookmarkBar->setIconSize(QSize(16, 16));
    bookmarkBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    addToolBar(Qt::TopToolBarArea, bookmarkBar);
    if(settings->bookmarkBarShown() == false) {
        bookmarkBar->setVisible(false);
    } else {
        bookmarkBar->setVisible(true);
    }

    /* setup the Location Bar */
    locationBar = new LocationBar(this);
    connect(locationBar, SIGNAL(loadUrl(QUrl)), this, SLOT(navigateToLocation(QUrl)));
    connect(locationBar, SIGNAL(loadSearch(QUrl)), this, SLOT(navigateToLocation(QUrl)));
    locationBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    setupFindBar();

    bookmarkDlg = new AddBookmarkDialog(this, Qt::Dialog);
    bookmarkDlg->hide();
    connect(bookmarkDlg, SIGNAL(addNewBookmark(QString,QString,QString)),
            this, SLOT(addNewBookmark(QString,QString,QString)));

    setupMenuBar();

    bm = new BookmarkManager(this);
    parseBookmarks();
    connect(menu_Bookmark, SIGNAL(triggered(QAction *)), this, SLOT(bookmarkTriggered(QAction *)));


    tabWidget->setCurrentIndex(0);
    setCentralWidget(tabWidget);

    mainToolBar->addAction(menuButton);
    mainToolBar->addAction(action_Back);
    mainToolBar->addAction(action_Forward);
    mainToolBar->addAction(action_Stop);
    mainToolBar->addAction(action_Reload);
    mainToolBar->addWidget(locationBar);
    mainToolBar->addAction(action_Add_Trusted);

    connect(mainToolBar->toggleViewAction(), SIGNAL(triggered()), this, SLOT(toggleToolBar()));
    connect(bookmarkBar->toggleViewAction(), SIGNAL(triggered()), this, SLOT(toggleBookmarkBar()));

    setWindowTitle(tr("Swift"));
    if(m_private == true)
        setWindowTitle(tr("Swift [Private]"));

    if(!url.isEmpty())
        navigateToLocation(url);
}

SwiftMainWindow::~SwiftMainWindow()
{
    delete locationBar;
}

void SwiftMainWindow::setupTabWidget()
{
    m_newTab = new QToolButton(this);
    m_newTab->setAutoRaise(true);
    m_newTab->setIcon(QIcon(QString::fromUtf8(":/Swift/newtab")));
    m_newTab->setToolTip(tr("Open a new Tab"));
    connect(m_newTab, SIGNAL(clicked()), this, SLOT(newTab()));

    m_trashedTabs = new QMenu(this);
    m_trash = new QToolButton(this);
    m_trash->setAutoRaise(true);
    m_trash->setIcon(QIcon(QString::fromUtf8(":/Swift/trash")));
    m_trash->setToolTip(tr("Closed Tabs"));
    m_trash->setMenu(m_trashedTabs);
    connect(m_trashedTabs, SIGNAL(triggered(QAction*)), this, SLOT(reopenTrashedTab(QAction*)));
    m_trash->setEnabled(false);

    tabWidget = new TabWidget(this);
    tabWidget->setGeometry(QRect(0, 0, 601, 351));
    tabWidget->setCornerWidget(m_newTab, Qt::TopLeftCorner);
    tabWidget->setCornerWidget(m_trash, Qt::TopRightCorner);
    tabWidget->newTab(QUrl(settings->value("HomePage").toString()), m_private);
    connect(tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(tabCloseRequested(int)));
    connect(tabWidget, SIGNAL(currentChanged(int)), this, SLOT(currentChanged(int)));
    connect(tabWidget, SIGNAL(updateLocation(const QUrl)), this, SLOT(updateLocation(const QUrl)));
    connect(tabWidget, SIGNAL(updateActions()), this, SLOT(updateActions()));
    connect(tabWidget, SIGNAL(loadProgress(int)), this, SLOT(loadProgress(int)));
    connect(tabWidget, SIGNAL(loadStarted()), this, SLOT(loadStarted()));
    connect(tabWidget, SIGNAL(loadFinished(bool)), this, SLOT(loadFinished(bool)));
    connect(tabWidget, SIGNAL(urlClicked(const QUrl)), this, SLOT(urlClicked(const QUrl)));
    connect(tabWidget, SIGNAL(linkHovered(QString, QString)), this,
            SLOT(linkHovered(QString, QString)));
    connect(tabWidget, SIGNAL(raiseParent()), this, SLOT(raiseParent()));
    connect(tabWidget, SIGNAL(updateIcon()), this, SLOT(updateIcon()));
    connect(tabWidget, SIGNAL(trashedTab(QString,QUrl)), this, SLOT(trashedTab(QString,QUrl)));
    /* Map Actions */
    connect(action_Back, SIGNAL(triggered()), tabWidget, SLOT(goBack()));
    connect(action_Forward, SIGNAL(triggered()), tabWidget, SLOT(goForward()));
    connect(action_Reload, SIGNAL(triggered()), tabWidget, SLOT(reload()));
    connect(action_Stop, SIGNAL(triggered()), tabWidget, SLOT(stop()));
}

void SwiftMainWindow::setupStatusBar()
{
    m_loadProgress = new QProgressBar(this);
    m_loadProgress->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_loadProgress->setMaximumHeight(16);
    m_loadProgress->setTextVisible(false);

    recentStatus = new QLabel(this);
    recentStatus->setGeometry(0, 0, 150, 16);
    m_zone = new ZoneLabel(this);

    statusBar = new QStatusBar(this);
    setStatusBar(statusBar);
    statusBar->addWidget(recentStatus, 1);
    statusBar->addPermanentWidget(m_loadProgress);
    statusBar->addPermanentWidget(m_zone);
    statusBar->addPermanentWidget(m_zoomFactor);
    if(settings->statusBarShown() == false)
    {
        statusBar->setVisible(false);
        action_Status_Bar->setChecked(false);
    }
    else
    {
        statusBar->setVisible(true);
        action_Status_Bar->setChecked(true);
    }
}

void SwiftMainWindow::setupMenuBar()
{
    menuBar = new QMenuBar(this);
    #ifdef _WIN32
    //turnMenuBarTransparent();
    #endif
    menu_File = new QMenu(menuBar);
    menu_Edit = new QMenu(menuBar);
    menu_Go = new QMenu(menuBar);
    menu_Bookmark = new QMenu(menuBar);
    menu_Tools = new QMenu(menuBar);
    menu_Help = new QMenu(menuBar);
    menu_View = new QMenu(menuBar);
    setMenuBar(menuBar);

    menu_File->setTitle(tr("&File"));
    menu_Edit->setTitle(tr("&Edit"));
    menu_Go->setTitle(tr("&Go"));
    menu_Bookmark->setTitle(tr("&Bookmarks"));
    menu_Tools->setTitle(tr("&Tools"));
    menu_Help->setTitle(tr("&Help"));
    menu_View->setTitle(tr("&View"));

    menu_File->addAction(action_New_Window);
    menu_File->addAction(action_New_Tab);
    menu_File->addAction(actionOpen_Location);
    menu_File->addAction(actionOpen_File);
    menu_File->addSeparator();
    menu_File->addAction(action_Save_Page_As);
    menu_File->addAction(actionSend_Link);
    menu_File->addSeparator();
    menu_File->addAction(action_Print);
    menu_File->addAction(action_Print_Preview);
    menu_File->addSeparator();
    menu_File->addAction(action_Import);
    menu_File->addSeparator();
    menu_File->addAction(action_Work_Offline);
    menu_File->addAction(action_Close_Tab);
    menu_File->addAction(actionClose_Window);
    menu_File->addSeparator();
    menu_File->addAction(action_Exit);
    menu_Edit->addAction(action_Undo);
    menu_Edit->addAction(action_Redo);
    menu_Edit->addSeparator();
    menu_Edit->addAction(action_Cut);
    menu_Edit->addAction(actionC_opy);
    menu_Edit->addAction(action_Paste);
    menu_Edit->addSeparator();
    menu_Edit->addAction(action_Find);
    menu_Edit->addAction(actionFind_Again);
    menu_Edit->addSeparator();
    menu_Edit->addAction(actionWeb_Search);
    menu_Edit->addSeparator();
    menu_Go->addAction(action_Back);
    menu_Go->addAction(action_Forward);
    menu_Go->addAction(action_Home);
    menu_Go->addSeparator();
    menu_Go->addAction(action_History);
    menu_Go->addSeparator();
    menu_Go->addMenu(m_trashedTabs);
    m_trashedTabs->setTitle(tr("Trashed Tabs/Popups"));
    //menu_Go->addAction(action_Recent_Pages);
    menu_Bookmark->addAction(action_Add_Bookmark);
    menu_Bookmark->addAction(action_Bookmark_All_Tabs);
    menu_Bookmark->addSeparator();
    menu_Bookmark->addAction(action_Organize_Bookmarks);
    menu_Bookmark->addSeparator();
    menu_Tools->addAction(action_Downloads);
    menu_Tools->addAction(action_Error_Console);
    menu_Tools->addSeparator();
    menu_Tools->addAction(action_Page_Info);
    menu_Tools->addAction(actionPage_Source);
    menu_Tools->addSeparator();
    menu_Tools->addAction(action_Start_Private_Browsing);
    menu_Tools->addAction(action_Clear_Private_Data);
    #ifndef _WIN32
    menu_Edit->addSeparator();
    menu_Edit->addAction(action_Preferences);
    #else
    menu_Tools->addSeparator();
    menu_Tools->addAction(action_Preferences);
    #endif
    menu_Help->addAction(action_Swift_Help);
    menu_Help->addSeparator();
    menu_Help->addAction(action_Swift_on_the_Web);
    menu_Help->addAction(action_New_Users_Guide);
    menu_Help->addAction(action_Report_a_Bug);
    menu_Help->addSeparator();
    menu_Help->addAction(action_Check_for_Updates);
    menu_Help->addSeparator();
    menu_Help->addAction(action_About_Swift);
    menu_View->addAction(action_Menu_Bar);
    menu_View->addAction(mainToolBar->toggleViewAction());
    menu_View->addAction(bookmarkBar->toggleViewAction());
    menu_View->addAction(action_Status_Bar);
    menu_View->addSeparator();
    menu_View->addAction(action_Stop);
    menu_View->addSeparator();
    menu_View->addAction(action_Zoom_In);
    menu_View->addAction(actionZoom_Out);
    menu_View->addAction(actionNormal_Zoom_Level);
    menu_View->addSeparator();
    menu_View->addAction(action_Full_Screen);

    menuBar->addAction(menu_File->menuAction());
    menuBar->addAction(menu_Edit->menuAction());
    menuBar->addAction(menu_View->menuAction());
    menuBar->addAction(menu_Go->menuAction());
    menuBar->addAction(menu_Bookmark->menuAction());
    menuBar->addAction(menu_Tools->menuAction());
    menuBar->addAction(menu_Help->menuAction());
    mainMenu = new QMenu(this);
    mainMenu->addAction(action_New_Tab);
    mainMenu->addAction(action_Print);
    mainMenu->addAction(action_Add_Bookmark);
    mainMenu->addAction(action_Add_Trusted);
    mainMenu->addSeparator();
    mainMenu->addMenu(menu_File);
    mainMenu->addMenu(menu_Edit);
    mainMenu->addMenu(menu_View);
    mainMenu->addMenu(menu_Go);
    mainMenu->addMenu(menu_Bookmark);
    mainMenu->addMenu(menu_Tools);
    mainMenu->addMenu(menu_Help);
    mainMenu->addSeparator();
    mainMenu->addAction(action_Preferences);
    mainMenu->addAction(action_Exit);
    menuButton->setMenu(mainMenu);
    menuButton->setIcon(QIcon(QString::fromUtf8(":/Swift/swift-logo")));

    connect(menu_Go, SIGNAL(aboutToHide()), this, SLOT(aboutToHideGoMenu()));
    connect(menu_Go, SIGNAL(aboutToShow()), this, SLOT(aboutToShowGoMenu()));
    connect(menu_Go, SIGNAL(triggered(QAction*)), this, SLOT(triggeredGoMenuAction(QAction*)));
    if(!settings->menuBarShown())
    {
        if(!mainToolBar->isVisible())
        {
            mainToolBar->setVisible(true);
            toggleToolBar();
        }
        mainToolBar->toggleViewAction()->setDisabled(true);
        action_Menu_Bar->setChecked(false);
        settings->setMenuBarShown(false);
        menuBar->setVisible(false);
        menuButton->setVisible(true);
        mainToolBar->toggleViewAction()->setDisabled(true);
        action_Menu_Bar->setEnabled(true);
    }
    else
    {
        menuButton->setVisible(false);
        action_Menu_Bar->setChecked(true);
    }
}

void SwiftMainWindow::setupActions()
{
    menuButton = new QAction(this);
    action_Add_Trusted = new QAction(this);
    action_New_Window = new QAction(this);
    action_New_Window->setIconVisibleInMenu(false);
    action_New_Tab = new QAction(this);
    action_New_Tab->setIconVisibleInMenu(false);
    actionOpen_Location = new QAction(this);
    actionOpen_Location->setIconVisibleInMenu(false);
    actionOpen_File = new QAction(this);
    actionOpen_File->setIconVisibleInMenu(false);
    action_Save_Page_As = new QAction(this);
    action_Save_Page_As->setIconVisibleInMenu(false);
    actionSend_Link = new QAction(this);
    actionSend_Link->setIconVisibleInMenu(false);
    action_Print = new QAction(this);
    action_Print->setIconVisibleInMenu(false);
    action_Print_Preview = new QAction(this);
    action_Import = new QAction(this);
    action_Work_Offline = new QAction(this);
    action_Close_Tab = new QAction(this);
    action_Close_Tab->setIconVisibleInMenu(false);
    actionClose_Window = new QAction(this);
    actionClose_Window->setIconVisibleInMenu(false);
    action_Exit = new QAction(this);
    action_Exit->setMenuRole(QAction::QuitRole);
    action_Exit->setIconVisibleInMenu(false);
    action_Undo = new QAction(this);
    action_Redo = new QAction(this);
    action_Cut = new QAction(this);
    actionC_opy = new QAction(this);
    action_Paste = new QAction(this);
    action_Find = new QAction(this);
    actionWeb_Search = new QAction(this);
    actionFind_Again = new QAction(this);
    action_Back = new QAction(this);
    action_Back->setIconVisibleInMenu(false);
    action_Forward = new QAction(this);
    action_Forward->setIconVisibleInMenu(false);
    action_Home = new QAction(this);
    action_History = new QAction(this);
    action_Recent_Pages = new QAction(this);
    action_Add_Bookmark = new QAction(this);
    action_Add_Bookmark->setData(QVariant(QString("add_Bookmark")));
    action_Bookmark_All_Tabs = new QAction(this);
    action_Bookmark_All_Tabs->setEnabled(false);
    action_Organize_Bookmarks = new QAction(this);
    action_Organize_Bookmarks->setEnabled(false);
    action_Downloads = new QAction(this);
    action_Error_Console = new QAction(this);
    action_Page_Info = new QAction(this);
    actionPage_Source = new QAction(this);
    action_Start_Private_Browsing = new QAction(this);
    action_Clear_Private_Data = new QAction(this);
    action_Preferences = new QAction(this);
    action_Preferences->setMenuRole(QAction::PreferencesRole);
    action_Swift_Help = new QAction(this);
    action_Swift_on_the_Web = new QAction(this);
    action_New_Users_Guide = new QAction(this);
    action_Report_a_Bug = new QAction(this);
    action_About_Swift = new QAction(this);
    action_Check_for_Updates = new QAction(this);
    action_Status_Bar = new QAction(this);
    action_Status_Bar->setCheckable(true);
    action_Menu_Bar = new QAction(this);
    action_Menu_Bar->setCheckable(true);
    /* do we need stop/reload? */
    action_Stop = new QAction(this);
    action_Stop->setIconVisibleInMenu(false);
    action_Reload = new QAction(this);
    action_Reload->setIconVisibleInMenu(false);
    action_Zoom_In = new QAction(this);
    actionZoom_Out = new QAction(this);
    actionNormal_Zoom_Level = new QAction(this);
    action_Full_Screen = new QAction(this);

    /* preform connections */
    connect(action_New_Window, SIGNAL(triggered()), this, SLOT(newWindow()));
    connect(action_Print, SIGNAL(triggered()), this, SLOT(print()));
    connect(action_Print_Preview, SIGNAL(triggered()), this, SLOT(printPreview()));
    connect(action_New_Tab, SIGNAL(triggered()), this, SLOT(newTab()));
    connect(actionNormal_Zoom_Level, SIGNAL(triggered()), this, SLOT(normalZoomLevel()));
    connect(action_Downloads, SIGNAL(triggered()), this, SLOT(showDownloads()));
    connect(action_Preferences, SIGNAL(triggered()), this, SLOT(showPreferences()));
    connect(action_About_Swift, SIGNAL(triggered()), this, SLOT(showAbout()));
    connect(action_Status_Bar, SIGNAL(triggered()), this, SLOT(toggleStatusBar()));
    connect(action_Menu_Bar, SIGNAL(triggered()), this, SLOT(toggleMenuBar()));
    connect(actionZoom_Out, SIGNAL(triggered()), this, SLOT(zoomOut()));
    connect(action_Zoom_In, SIGNAL(triggered()), this, SLOT(zoomIn()));
    connect(action_Full_Screen, SIGNAL(triggered()), this, SLOT(toggleFullScreen()));
    connect(action_Exit, SIGNAL(triggered()), this, SLOT(exitSwift()));
    connect(action_Find, SIGNAL(triggered()), this, SLOT(showFindBar()));
    connect(actionFind_Again, SIGNAL(triggered()), this, SLOT(findText()));
    connect(action_Add_Trusted, SIGNAL(triggered()), this, SLOT(addTrusted()));
    connect(actionPage_Source, SIGNAL(triggered()), this, SLOT(showSource()));
    connect(action_Page_Info, SIGNAL(triggered()), this, SLOT(showInfo()));
    connect(action_Start_Private_Browsing, SIGNAL(triggered()), this, SLOT(startPrivateBrowsing()));

    //connect(actionWeb_Search, SIGNAL(triggered()), locationBar, SLOT(activateSearchBox()));

    /* translate and set strings */
    action_New_Window->setText(tr("&New Window"));
    action_New_Window->setShortcut(tr("Ctrl+N"));
    action_New_Tab->setText(tr("New &Tab"));
    action_New_Tab->setShortcut(tr("Ctrl+T"));
    action_New_Tab->setIcon(QIcon(QString::fromUtf8(":/Swift/newtab")));
    actionOpen_Location->setText(tr("Open &Location"));
    actionOpen_Location->setShortcut(tr("Ctrl+L"));
    actionOpen_File->setText(tr("Open &File..."));
    actionOpen_File->setShortcut(tr("Ctrl+F"));
    action_Save_Page_As->setText(tr("&Save Page As..."));
    action_Save_Page_As->setShortcut(tr("Ctrl+S"));
    actionSend_Link->setText(tr("Send L&ink..."));
    action_Print->setText(tr("&Print..."));
    action_Print->setShortcut(tr("Ctrl+P"));
    action_Print_Preview->setText(tr("P&rint Preview"));
    action_Import->setText(tr("&Import..."));
    action_Work_Offline->setText(tr("&Work Offline"));
    action_Close_Tab->setText(tr("&Close Tab"));
    action_Close_Tab->setShortcut(tr("Ctrl+W"));
    actionClose_Window->setText(tr("Close &Window"));
    action_Exit->setText(tr("&Exit"));
    action_Exit->setShortcut(tr("F3"));
    action_Undo->setText(tr("&Undo"));
    action_Undo->setShortcut(tr("Ctrl+Z"));
    action_Redo->setText(tr("&Redo"));
    action_Redo->setShortcut(tr("Ctrl+Y"));
    action_Cut->setText(tr("&Cut"));
    action_Cut->setShortcut(tr("Ctrl+X"));
    actionC_opy->setText(tr("C&opy"));
    actionC_opy->setShortcut(tr("Ctrl+C"));
    action_Paste->setText(tr("&Paste"));
    action_Find->setText(tr("&Find"));
    action_Find->setShortcut(tr("Ctrl+F"));
    actionWeb_Search->setText(tr("Web &Search"));
    actionFind_Again->setText(tr("Find &Again"));
    action_Back->setText(tr("&Back"));
    action_Forward->setText(tr("&Forward"));
    action_Home->setText(tr("&Home"));
    action_History->setText(tr("&History"));
    action_Recent_Pages->setText(tr("&Recent Pages"));
    action_Add_Bookmark->setText(tr("&Add Bookmark"));
    action_Add_Bookmark->setShortcut(tr("Ctrl+D"));
    action_Bookmark_All_Tabs->setText(tr("&Bookmark All Tabs"));
    action_Organize_Bookmarks->setText(tr("&Organize Bookmarks"));
    action_Downloads->setText(tr("&Downloads"));
    action_Downloads->setShortcut(tr("Ctrl+J"));
    action_Error_Console->setText(tr("&Error Console"));
    action_Error_Console->setShortcut(tr("Ctrl+Shift+J"));
    action_Page_Info->setText(tr("&Page Info"));
    actionPage_Source->setText(tr("Page &Source"));
    actionPage_Source->setShortcut(tr("Ctrl+U"));
    action_Start_Private_Browsing->setText(tr("Start &Private Browsing"));
    action_Clear_Private_Data->setText(tr("&Clear Private Data"));
    action_Preferences->setText(tr("&Preferences"));
    action_Swift_Help->setText(tr("&Swift Help"));
    action_Swift_Help->setShortcut(tr("F1"));
    action_Swift_on_the_Web->setText(tr("&Swift on the Web"));
    action_New_Users_Guide->setText(tr("&New Users Guide"));
    action_Report_a_Bug->setText(tr("&Report a Bug "));
    action_About_Swift->setText(tr("&About Swift"));
    action_Check_for_Updates->setText(tr("&Check for Updates"));
    action_Status_Bar->setText(tr("&Status Bar"));
    action_Menu_Bar->setText(tr("&Menu Bar"));
    action_Menu_Bar->setShortcut(tr("F9"));
    action_Reload->setShortcut(tr("F5"));
    action_Stop->setText(tr("&Stop"));
    action_Reload->setText(tr("&Reload"));
    action_Stop->setShortcut(tr("Esc"));
    action_Zoom_In->setText(tr("Zoom &In"));
    action_Zoom_In->setShortcut(tr("Ctrl++"));
    actionZoom_Out->setText(tr("Zoom &Out"));
    actionZoom_Out->setShortcut(tr("Ctrl+-"));
    actionNormal_Zoom_Level->setText(tr("Normal Zoom Level"));
    actionNormal_Zoom_Level->setShortcut(tr("Ctrl+0"));
    action_Full_Screen->setText(tr("&Full Screen"));
    action_Full_Screen->setShortcut(tr("F11"));
    action_Add_Trusted->setText(tr("Trust Website"));
    action_Add_Trusted->setToolTip(tr("Trust this Website"));
    action_Add_Trusted->setStatusTip(tr("Add this website to your Trusted Sites list"));
    action_Add_Trusted->setIcon(QIcon(QString::fromUtf8(":/Swift/trusted")));
    menuButton->setToolTip(tr("Main Menu"));
    menuButton->setStatusTip(tr("The Swift Main Menu"));
    /* set icons */
    action_Reload->setIcon(style()->standardIcon(QStyle::SP_BrowserReload));
    action_Back->setIcon(style()->standardIcon(QStyle::SP_ArrowBack));
    action_Forward->setIcon(style()->standardIcon(QStyle::SP_ArrowForward));
    action_Stop->setIcon(style()->standardIcon(QStyle::SP_BrowserStop));
}

void SwiftMainWindow::toggleFullScreen()
{
    action_Full_Screen->isChecked() ? setWindowState(Qt::WindowNoState) : setWindowState(Qt::WindowFullScreen);
}

void SwiftMainWindow::aboutToShowGoMenu()
{
    removeHistoryActions();
    QList<historyitem_t> items = lastAccessedItems();
    if(items.count() == 0)
    {
        action_History->setText(tr("No History Items"));
        return;
    }

    for(int i = 0; i < items.count(); i++)
    {
        historyitem_t item = items.at(i);
        if(item.title == QString("") || item.url.toString() == QString(""))
            continue;

        if(item.title.length() > 60)
        {
            item.title.truncate(60);
            item.title.append("...");
        }

        QAction *tmp = new QAction(QWebSettings::iconForUrl(item.url), item.title, this);
        if(QWebSettings::iconForUrl(item.url).isNull())
            tmp->setIcon(QWebSettings::iconForUrl(QUrl("")));
        tmp->setIconVisibleInMenu(true);
        tmp->setObjectName(QString("historyItem"));
        tmp->setData(QString(item.url.toString()));
        menu_Go->insertAction(action_History, tmp);
    }
    action_History->setVisible(false);
}

void SwiftMainWindow::removeHistoryActions()
{
    int actCount = menu_Go->actions().count();
    for(int i = actCount - 1; i >= 0; i--)
    {
        if(menu_Go->actions()[i]->objectName() == QString("historyItem"))
        {
            QAction *tmp = menu_Go->actions()[i];
            menu_Go->removeAction(tmp);
            delete tmp;
        }
    }
    action_History->setVisible(true);
}

void SwiftMainWindow::triggeredGoMenuAction(QAction *action)
{
    if(!action->data().isNull())
    {
        QUrl url(action->data().toString());
        newTab(url);
    }
}

/* Find Bar Functions */

void SwiftMainWindow::setupFindBar()
{
    findBar = new QToolBar(tr("Find In Page"), this);
    findInPageText = new QLineEdit(this);
    findLabel = new QLabel(tr("Find:"), this);
    findNext = new QAction(style()->standardIcon(QStyle::SP_ArrowDown), tr("Next"), this);
    findPrevious = new QAction(style()->standardIcon(QStyle::SP_ArrowUp), tr("Previous"), this);
    findMatchCase = new QCheckBox(tr("Match Case"), this);
    closeFindBar = new QAction(this);
    infoLabel = new QLabel(this);
    infoPixmap = new QLabel(this);
    infoPixmap->setPixmap(style()->standardPixmap(QStyle::SP_MessageBoxInformation));

    infoPixmap->setVisible(false);
    infoLabel->setVisible(false);

    closeFindBar->setText("");
    closeFindBar->setToolTip(tr("Close Find bar"));
    closeFindBar->setIcon(style()->standardIcon(QStyle::SP_DockWidgetCloseButton));

    findInPageText->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    findBar->setIconSize(QSize(16, 16));
    findBar->setAllowedAreas(Qt::BottomToolBarArea);
    findBar->setFloatable(false);
    findBar->setMovable(false);

    findBar->addAction(closeFindBar);
    findBar->addWidget(findLabel);
    findBar->addWidget(findInPageText);
    findBar->addAction(findNext);
    findBar->addAction(findPrevious);
    findBar->addWidget(findMatchCase);
    findBar->addWidget(infoPixmap);
    findBar->addWidget(infoLabel);

    connect(findNext, SIGNAL(triggered()), this, SLOT(findText()));
    connect(findPrevious, SIGNAL(triggered()), this, SLOT(findTextPrevious()));
    connect(findInPageText, SIGNAL(textChanged(QString)), this, SLOT(findTextChanged(QString)));
    connect(closeFindBar, SIGNAL(triggered()), this, SLOT(hideFindBar()));
    addToolBar(Qt::BottomToolBarArea, findBar);

    hideFindBar();
}

void SwiftMainWindow::showFindBar()
{
    if(!findBar->isVisible())
    {
        findInPageText->setText("");
        findBar->setVisible(true);
        findInPageText->setFocus(Qt::OtherFocusReason);
    }
}

void SwiftMainWindow::hideFindBar()
{
    findInPageText->setText("");
    infoPixmap->setVisible(false);
    infoLabel->setVisible(false);
    findBar->setVisible(false);
}

void SwiftMainWindow::findText()
{
    bool result;

    if(findMatchCase->isChecked())
        result = tabWidget->currentWebView()->findText(findInPageText->text(),
            QWebPage::FindWrapsAroundDocument|QWebPage::FindCaseSensitively);
    else
        result = tabWidget->currentWebView()->findText(findInPageText->text(),
            QWebPage::FindWrapsAroundDocument);

    if(!result)
    {
        infoPixmap->setVisible(true);
        infoLabel->setVisible(true);
        infoLabel->setText(tr("Search String not found."));
    }
    else
    {
        infoPixmap->setVisible(false);
        infoLabel->setVisible(false);
    }
}

void SwiftMainWindow::findTextPrevious()
{
    bool result;
    if(findMatchCase->isChecked())
        result = tabWidget->currentWebView()->findText(findInPageText->text(),
            QWebPage::FindBackward|QWebPage::FindWrapsAroundDocument|QWebPage::FindCaseSensitively);
    else
        result = tabWidget->currentWebView()->findText(findInPageText->text(),
            QWebPage::FindBackward|QWebPage::FindWrapsAroundDocument);

    if(!result)
    {

        infoPixmap->setVisible(true);
        infoLabel->setVisible(true);
        infoLabel->setText(tr("Search String not found."));
    }
    else
    {
        infoPixmap->setVisible(false);
        infoLabel->setVisible(false);
    }
}

void SwiftMainWindow::findTextChanged(QString text)
{
    text = "";
    findText();
}

/* These functions work with zooming functions
        setupZoomButton - sets up the statusbar zoom menu and button
        changeZoom - changes the Zoom Level based on a triggered action from the SB zoom menu
        setZoomItemsUnchecked - unchecks all zoom menu items
        zoomIn - zooms in the page by ten percent
        zoomOut - zooms out the page by ten percent
        normalZoomLevel() - resets to 100%
        setCheckedZoomItem(string) - changes zoom level checked in menu
*/

void SwiftMainWindow::setupZoomButton() /* 100% */
{
    m_zoomFactor = new QToolButton(this);
    m_zoomFactor->setAutoRaise(true);
    m_zoomFactor->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    m_zoomFactor->setPopupMode(QToolButton::MenuButtonPopup);
    m_zoomMenu = new QMenu(this);
    m_zoomMenu->addAction("160%")->setCheckable(true);
    m_zoomMenu->addAction("150%")->setCheckable(true);
    m_zoomMenu->addAction("140%")->setCheckable(true);
    m_zoomMenu->addAction("130%")->setCheckable(true);
    m_zoomMenu->addAction("120%")->setCheckable(true);
    m_zoomMenu->addAction("110%")->setCheckable(true);
    m_zoomMenu->addAction("100%")->setCheckable(true);
    m_zoomMenu->actions().at(6)->setChecked(true);
    m_zoomMenu->addAction("90%")->setCheckable(true);
    m_zoomMenu->addAction("80%")->setCheckable(true);
    m_zoomMenu->addAction("70%")->setCheckable(true);
    m_zoomMenu->addAction("60%")->setCheckable(true);
    m_zoomMenu->addAction("50%")->setCheckable(true);
    m_zoomMenu->addAction("40%")->setCheckable(true);
    m_zoomFactor->setIcon(QIcon(QString::fromUtf8(":/Swift/zoom")));
    m_zoomFactor->setText(tr("100%"));
    m_zoomFactor->setMaximumHeight(18);
    m_zoomFactor->setMenu(m_zoomMenu);
    connect(m_zoomMenu, SIGNAL(triggered(QAction*)), this, SLOT(changeZoom(QAction*)));
}

void SwiftMainWindow::changeZoom(QAction *zoomLevel)
{
    if(zoomLevel->text().remove("%").toInt() < 100) {
        tabWidget->currentWebView()->setZoomFactor(zoomLevel->text().remove("%").insert(0, ".").toDouble());
    } else {
        tabWidget->currentWebView()->setZoomFactor(zoomLevel->text().remove("%").insert(1, ".").toDouble());
    }
    setZoomItemsUnchecked();
    zoomLevel->setChecked(true);
    m_zoomFactor->setText(zoomLevel->text());

    if(zoomLevel->text() == QString("40%"))
        actionZoom_Out->setEnabled(false);

    if(zoomLevel->text() == QString("160%"))
        action_Zoom_In->setEnabled(false);
}

void SwiftMainWindow::setZoomItemsUnchecked() /* 100% */
{
    for(int i = 0; i < m_zoomMenu->actions().count(); i++)
    {
        m_zoomMenu->actions().at(i)->setChecked(false);
    }
}

void SwiftMainWindow::zoomIn()
{
    setZoomItemsUnchecked();
    actionZoom_Out->setEnabled(true);
    action_Zoom_In->setEnabled(true);

    QString txt;

    tabWidget->currentWebView()->setZoomFactor(tabWidget->currentWebView()->zoomFactor() + .1);
    if(tabWidget->currentWebView()->zoomFactor() > 1.0)
    {
        /* three digits */
        txt = QString::number(tabWidget->currentWebView()->zoomFactor(), 103, 6);
        txt.append("0%");
        txt.remove(".");
        m_zoomFactor->setText(txt);
        setCheckedZoomItem(txt);
    } else if(tabWidget->currentWebView()->zoomFactor() == 1.0) {
        /* 100% */
        txt = QString::number(tabWidget->currentWebView()->zoomFactor(), 103, 6);
        txt.append("00%");
        txt.remove(".");
        m_zoomFactor->setText(txt);
        setCheckedZoomItem(txt);
    } else {
        /* two digits */
        txt = QString::number(tabWidget->currentWebView()->zoomFactor(), 103, 4);
        txt.remove(0, 2);
        txt.append("0%");
        m_zoomFactor->setText(txt);
        setCheckedZoomItem(txt);
    }

    if(txt == QString("40%"))
        actionZoom_Out->setEnabled(false);

    if(txt == QString("160%"))
        action_Zoom_In->setEnabled(false);
}

void SwiftMainWindow::zoomOut()
{
    setZoomItemsUnchecked();
    actionZoom_Out->setEnabled(true);
    action_Zoom_In->setEnabled(true);

    QString txt;

    tabWidget->currentWebView()->setZoomFactor(tabWidget->currentWebView()->zoomFactor() - .1);
    if(tabWidget->currentWebView()->zoomFactor() > 1.0)
    {
        /* three digits */
        txt = QString::number(tabWidget->currentWebView()->zoomFactor(), 103, 6);
        txt.append("0%");
        txt.remove(".");
        m_zoomFactor->setText(txt);
        setCheckedZoomItem(txt);
    } else if(tabWidget->currentWebView()->zoomFactor() == 1.0) {
        /* 100% */
        txt = QString::number(tabWidget->currentWebView()->zoomFactor(), 103, 6);
        txt.append("00%");
        txt.remove(".");
        m_zoomFactor->setText(txt);
        setCheckedZoomItem(txt);
    } else {
        /* two digits */
        txt = QString::number(tabWidget->currentWebView()->zoomFactor(), 103, 4);
        txt.remove(0, 2);
        txt.append("0%");
        m_zoomFactor->setText(txt);
        setCheckedZoomItem(txt);
    }

    if(txt == QString("40%"))
        actionZoom_Out->setEnabled(false);

    if(txt == QString("160%"))
        action_Zoom_In->setEnabled(false);
}

void SwiftMainWindow::normalZoomLevel()
{
    tabWidget->currentWebView()->setZoomFactor(1);
    m_zoomFactor->setText("100%");
    setZoomItemsUnchecked();
    setCheckedZoomItem("100%");
    actionZoom_Out->setEnabled(true);
    action_Zoom_In->setEnabled(true);
}

void SwiftMainWindow::setCheckedZoomItem(QString zoom)
{
    for(int i = 0; i < m_zoomMenu->actions().count(); i++)
    {
        if(m_zoomMenu->actions().at(i)->text() == zoom)
            m_zoomMenu->actions().at(i)->setChecked(true);
    }
}

/* These functions work with WebView Navigation on the active tab

*/

void SwiftMainWindow::navigateToLocation(QUrl url)
{
    tabWidget->currentWebView()->load(url);
    //locationBar->addItem(locationBar->lineEdit()->text());
    updateActions();
}

void SwiftMainWindow::updateLocation(const QUrl url)
{
    QString wTitle;

    locationBar->setText(url.toString());
    if(tabWidget->currentWebView()->title().isNull())
        wTitle = tr("(Loading)");
    else
        wTitle = tabWidget->currentWebView()->title();
    updateActions();
    if(m_private == true)
        wTitle.append(tr(" [Private]"));
    setWindowTitle(wTitle);
}

void SwiftMainWindow::newTab()
{
    tabWidget->newTab(m_private);
    updateActions();
    locationBar->activateWindow();
    m_zone->setZone(tabWidget->currentWebView()->zone());
}

void SwiftMainWindow::newTab(QUrl url)
{
    tabWidget->newTab(url, m_private);
    updateActions();
    m_zone->setZone(tabWidget->currentWebView()->zone());
}

void SwiftMainWindow::currentChanged(int index)
{
    if(index >= 0) {
        QString wTitle = tabWidget->tabText(index);
        if(m_private == true)
            wTitle.append(tr(" [Private]"));
        setWindowTitle(wTitle);

        locationBar->setIcon(tabWidget->tabIcon(index));
        locationBar->lineEdit()->setText(tabWidget->webView(index)->url().toString());
        if(tabWidget->currentWebView()->isSecure() == true)
            locationBar->setSecure(tabWidget->currentWebView()->sslConfiguration());
        else
            locationBar->setInsecure();
        updateActions();
        m_zone->setZone(tabWidget->currentWebView()->zone());
    }
}

void SwiftMainWindow::updateActions()
{
    action_Back->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Back)->isEnabled());
    action_Forward->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Forward)->isEnabled());
    action_Reload->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Reload)->isEnabled());
    action_Stop->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Stop)->isEnabled());
    action_Cut->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Cut)->isEnabled());
    action_Paste->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Paste)->isEnabled());
    action_Undo->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Undo)->isEnabled());
    action_Redo->setEnabled(tabWidget->currentWebView()->pageAction(QWebPage::Redo)->isEnabled());
    if(tabWidget->currentWebView()->pageAction(QWebPage::Reload)->isEnabled() == true) {
        action_Stop->setVisible(false);
        action_Reload->setVisible(true);
    } else {
        action_Stop->setVisible(true);
        action_Reload->setVisible(false);
    }

    if(tabWidget->currentWebView()->loading() == false)
        m_loadProgress->setVisible(false);
    else
        m_loadProgress->setVisible(true);
}

void SwiftMainWindow::loadProgress(int progress)
{
    m_loadProgress->setValue(progress);
    recentStatus->setText(QString(tr("Loading Page: %1%").arg(progress)));
}

void SwiftMainWindow::loadStarted()
{
    m_loadProgress->setVisible(true);
    updateActions();

    QString wTitle = tr("(Loading)");
    if(m_private == true)
        wTitle.append(tr(" [Private]"));
    setWindowTitle(wTitle);

}

void SwiftMainWindow::loadFinished(bool ok)
{
    Q_UNUSED(ok);
    if(tabWidget->currentWebView()->isSecure() == true)
        locationBar->setSecure(tabWidget->currentWebView()->sslConfiguration());
    else
        locationBar->setInsecure();

    recentStatus->setText(tr("Document: Done"));
    updateActions();
    m_loadProgress->setVisible(false);
    QString wTitle;
    if(tabWidget->currentWebView()->title().isNull())
        wTitle = tr("(Untitled)");
    else
        wTitle = tabWidget->currentWebView()->title();
    if(m_private == true)
        wTitle.append(tr(" [Private]"));
    setWindowTitle(wTitle);

    m_zone->setZone(tabWidget->currentWebView()->zone());

    incrementAccessCount(tabWidget->currentWebView()->url());
}

void SwiftMainWindow::urlClicked(const QUrl url)
{
    recentStatus->setText(tr("Loading Document: ") + url.toString());
}

void SwiftMainWindow::closeEvent(QCloseEvent *e)
{
    /* check if we want to notify of closed tabs or not */
    if(tabWidget->count() > 1) {
        QMessageBox msg(QMessageBox::Question, tr("Close Window?"), tr("This window has ") + QString::number(tabWidget->count()) + tr(" open tabs, are you sure you want to close this window?"), QMessageBox::Yes | QMessageBox::No, this);
        int result = msg.exec();
        if(result == QMessageBox::Yes) {
            e->accept();
        } else {
            e->ignore();
        }
    }

    /* save tabs/window state here */

    settings->setValue("LastWindowRect", saveGeometry());
}

void SwiftMainWindow::startPrivateBrowsing()
{
    QMessageBox msg(QMessageBox::Information, tr("Starting Private Browsing"), tr("You are starting Private Browsing."), QMessageBox::Ok, this);
    QString info(tr("Private Browsing will not save history, will not save any website cookies, and not send any currently stored cookies. "));
    info.append(tr("Please note that Private Browsing will not safegaurd you from websites that collect personal data, browsing habits monitored by your ISP, "));
    info.append(tr("information collected from you in exchange for free smilies, or information gathered from nearby bystanders."));
    msg.setInformativeText(info);
    msg.exec();

    SwiftMainWindow *w = new SwiftMainWindow(0, Qt::Window, true, false);
    w->show();
}

void SwiftMainWindow::newWindow()
{
    SwiftMainWindow *w = new SwiftMainWindow();
    w->show();
}

void SwiftMainWindow::exitSwift()
{
    locationBar->saveSplitterRect();
    qApp->closeAllWindows();
    qApp->exit();
}

void SwiftMainWindow::linkHovered(QString url, QString text)
{
    Q_UNUSED(text);
    if(!url.isEmpty())
        statusBar->showMessage(url, 100000);
    else
        statusBar->clearMessage();
}

void SwiftMainWindow::raiseParent()
{
    activateWindow();
    raise();
}

void SwiftMainWindow::trashedTab(QString title, QUrl url)
{
    m_trashedTabs->addAction(title)->setObjectName(url.toString());
    m_trashedTabs->setEnabled(true);
    m_trash->setEnabled(true);
}

void SwiftMainWindow::reopenTrashedTab(QAction* action)
{
    newTab(QUrl(action->objectName()));
    m_trashedTabs->removeAction(action);
    if(m_trashedTabs->actions().count() == 0) {
        m_trash->setEnabled(false);
    }
}

void SwiftMainWindow::toggleStatusBar()
{
    if(statusBar->isVisible() == false)
    {
        action_Status_Bar->setChecked(true);
        settings->setStatusBarShown(true);
        statusBar->setVisible(true);
    }
    else
    {
        action_Status_Bar->setChecked(false);
        settings->setStatusBarShown(false);
        statusBar->setVisible(false);
    }
}

void SwiftMainWindow::toggleToolBar()
{
    if(mainToolBar->isVisible() == false)
    {
        if(!menuBar->isVisible())
            toggleMenuBar();
        action_Menu_Bar->setEnabled(false);
        settings->setToolbarShown(false);
    }
    else
    {
        action_Menu_Bar->setEnabled(true);
        settings->setToolbarShown(true);
    }
}

void SwiftMainWindow::toggleBookmarkBar()
{
    if(bookmarkBar->isVisible() == false)
        settings->setBookmarkBarShown(false);
    else
        settings->setBookmarkBarShown(true);
}

void SwiftMainWindow::toggleMenuBar()
{
    if(menuBar->isVisible() == true)
    {
        if(!mainToolBar->isVisible())
        {
            mainToolBar->setVisible(false);
            toggleToolBar();
        }
        mainToolBar->toggleViewAction()->setDisabled(true);
        action_Menu_Bar->setChecked(false);
        settings->setMenuBarShown(false);
        menuBar->setVisible(false);
        menuButton->setVisible(true);
        mainToolBar->toggleViewAction()->setDisabled(true);
    }
    else
    {
        action_Menu_Bar->setChecked(true);
        settings->setMenuBarShown(true);
        menuBar->setVisible(true);
        mainToolBar->toggleViewAction()->setDisabled(false);
        menuButton->setVisible(false);
    }
}

void SwiftMainWindow::parseBookmarks()
{
    QList<bookmark_t> bookmarks = bm->parse();
    for(int i = 0; i < bookmarks.count(); ++i)
    {
        if(bookmarks.at(i).path != QString("\\"))
        {
            QString newPath = bookmarks.at(i).path;
            newPath.remove(0,1);

            if(bookmarks.at(i).isSeperator == false)
            {
                if(!haveMenu(newPath))
                   insertFolder(newPath);
                insertBookmark(bookmarks.at(i).title, bookmarks.at(i).url, newPath);
            }
            else
                 insertSeparator(newPath);
        }
    }

    for(int i = 0; i < bookmarks.count(); ++i)
    {
        if(bookmarks.at(i).path == QString("\\"))
        {
            QString newPath = bookmarks.at(i).path;
            if(bookmarks.at(i).isSeperator == false)
               insertBookmark(bookmarks.at(i).title, bookmarks.at(i).url, newPath);
            else
                insertSeparator(newPath);
        }
    }
}

void SwiftMainWindow::addBookmarkFolder(QString folder)
{
    bookmarkDlg->addFolderLocation(folder);
    QStringList lst = folder.split("\\");
    if(lst.count() == 1)
    {
        QMenu *menu = new QMenu(folder, this);
        menu->setIcon(style()->standardIcon(QStyle::SP_DirIcon));
        menu->menuAction()->setIconVisibleInMenu(true);
        menu_Bookmark->addMenu(menu);
        bookmarkFolders.append(menu);
    }
    else
    {
        /* we need to find the folder that this folder resdes in 2nd to last item */
        QMenu *target = findMenu(folder);
        QMenu *menu = new QMenu(lst.at(lst.count() - 1), this);
        menu->setIcon(style()->standardIcon(QStyle::SP_DirIcon));
        menu->menuAction()->setIconVisibleInMenu(true);
        target->addMenu(menu);
        bookmarkFolders.append(menu);
    }
}

void SwiftMainWindow::insertSeparator(QString path)
{
    if(path == QString(""))
    {
        menu_Bookmark->addSeparator();
    }
    else
    {
        /* finding the folder for the bookmark, last item */
        QMenu *target = findMenu(path);
        target->addSeparator();
        if(path.contains("Bookmark Bar"))
        {
            bookmarkBar->addSeparator();
        }
    }
}

void SwiftMainWindow::insertBookmark(QString title, QString url, QString path)
{
    QUrl uri(url);
    if(path == QString("\\"))
    {
        QIcon icon = (QWebSettings::iconForUrl(uri).isNull() == true ? style()->standardIcon(QStyle::SP_FileIcon) : QWebSettings::iconForUrl(uri));
        QAction *action =
            new QAction(icon, title, this);
        action->setData(url);
        action->setIconVisibleInMenu(true);
        menu_Bookmark->addAction(action);
    }
    else
    {
        /* finding the folder for the bookmark, last item */
        QMenu *target = findMenu(path);
        QIcon icon;
        if(QWebSettings::iconForUrl(uri).isNull() == true)
            icon = style()->standardIcon(QStyle::SP_FileIcon);
        else
            QWebSettings::iconForUrl(uri);
        QAction *action =
            new QAction(icon, title, this);
        action->setData(url);
        action->setIconVisibleInMenu(true);
        target->addAction(action);
        if(path.contains("Bookmark Bar"))
        {
            bookmarkBar->addAction(action);
        }
    }
}

void SwiftMainWindow::insertFolder(QString folder)
{
    bookmarkDlg->addFolderLocation(folder);
    QStringList lst = folder.split("\\");
    if(lst.count() == 1)
    {
        QMenu *menu = new QMenu(folder, this);
        menu->setIcon(style()->standardIcon(QStyle::SP_DirIcon));
        menu->menuAction()->setIconVisibleInMenu(true);
        menu_Bookmark->addMenu(menu);
        bookmarkFolders.append(menu);
    }
    else
    {
        /* we need to find the folder that this folder resdes in 2nd to last item */
        QMenu *target = findMenu(folder);
        QMenu *menu = new QMenu(lst.at(lst.count() - 1), this);
        menu->setIcon(style()->standardIcon(QStyle::SP_DirIcon));
        menu->menuAction()->setIconVisibleInMenu(true);
        target->addMenu(menu);
        bookmarkFolders.append(menu);
    }
}

bool SwiftMainWindow::haveMenu(QString path)
{
    if(!path.contains("\\"))
    {
        for(int i = 0; i < bookmarkFolders.count(); i++)
        {
            if(bookmarkFolders.at(i)->title() == path)
                return true;
       }
    }

    QStringList lst = path.split("\\");
    for(int i = 0; i < bookmarkFolders.count(); i++)
    {
       QString menu = lst.at(lst.count() - 1);
       QString folder = bookmarkFolders.at(i)->title();
       if(bookmarkFolders.at(i)->title() ==
          lst.at(lst.count() - 1))
       {
           return true;
       }
    }

    return false;
}

QMenu* SwiftMainWindow::findMenu(QString path)
{
    if(!path.contains("\\"))
    {
        for(int i = 0; i < bookmarkFolders.count(); i++)
        {
            if(bookmarkFolders.at(i)->title() == path)
                return bookmarkFolders.at(i);
       }
    }

    QStringList lst = path.split("\\");
    for(int i = 0; i < bookmarkFolders.count(); i++)
    {
       QString menu = lst.at(lst.count() - 1);
       QString folder = bookmarkFolders.at(i)->title();
       if(bookmarkFolders.at(i)->title() ==
          lst.at(lst.count() - 1))
       {
           return bookmarkFolders.at(i);
       }
    }

    return new QMenu();
}

void SwiftMainWindow::bookmarkTriggered(QAction *action)
{
    if(action->data() == QVariant(QString("add_Bookmark")))
    {
        bookmarkDlg->setPageDetails(tabWidget->currentWebView()->url().toString(),
            tabWidget->currentWebView()->title());
        bookmarkDlg->show();
    }
    else
    {
        QString url = action->data().toString();
        tabWidget->currentWebView()->load(QUrl(url));
    }
}

void SwiftMainWindow::addNewBookmark(QString path, QString url, QString title)
{
    bm->addBookmark(path, url, title);
}

void SwiftMainWindow::navigateToSearch(QUrl search)
{
    tabWidget->currentWebView()->load(search);
}

void SwiftMainWindow::showAbout()
{
    AboutDialog *about = new AboutDialog(this);
    if(about->exec()) {}
    delete about;
}

void SwiftMainWindow::showPreferences()
{
    PreferencesWindow *pref = new PreferencesWindow(this);
    if(pref->exec()) {}
    delete pref;
}

void SwiftMainWindow::showDownloads()
{
    tabWidget->showDownloadsWindow();
}

void SwiftMainWindow::updateIcon()
{
    locationBar->setIcon(tabWidget->tabIcon(tabWidget->currentIndex()));
}

void SwiftMainWindow::print()
{
    tabWidget->currentWebView()->printDocument();
}

void SwiftMainWindow::printPreview()
{
    tabWidget->currentWebView()->printPreview();
}

void SwiftMainWindow::addTrusted()
{
    QString url = tabWidget->currentWebView()->url().host();
    EditZoneDialog *zone = new EditZoneDialog(this, ZoneManager::Trusted, url);
    if(zone->exec()) {}
    delete zone;
}

void SwiftMainWindow::showSource()
{
    tabWidget->currentWebView()->showPageSource();
}

void SwiftMainWindow::showInfo()
{
    tabWidget->currentWebView()->showPageInfo();
}
