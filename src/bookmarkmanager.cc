/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "bookmarkmanager.h"

BookmarkManager::BookmarkManager(QObject *parent)
        :QObject(parent)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query(db);
    query.exec(QString("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'bookmarks';"));
    if(!query.first())
    {
        query.exec(QString("CREATE TABLE [bookmarks] ([id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, [title] TEXT  NOT NULL ,[url] TEXT  NOT NULL, [path] TEXT  NOT NULL, [keywords] TEXT  NULL, [order] INTEGER  NULL)"));
        query.exec(QString("INSERT INTO bookmarks VALUES(1,'Swift','http://www.swift.ws/','\\Bookmark Bar',NULL,1);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(2,'eBay','http://www.ebay.com','\\Bookmark Bar',NULL,2);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(3,'GMail','http://mail.google.com','\\Bookmark Bar',NULL,3);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(4,'Facebook','http://www.facebook.com','\\Bookmark Bar',NULL,4);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(5,'Swift Homepage','http://www.swift.ws','\\Swift',NULL,1);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(6,'Swift Introduction Gude','http://www.swift.ws/starthere','\\Swift',NULL,2);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(7,'Translate Swift','http://www.swift.ws/translate','\\Swift',NULL,3);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(8,'-',0,'\\',NULL,1);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(9,'Facebook','http://www.facebook.com','\\',NULL,2);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(10,'Twitter','http://www.twitter.com','\\',NULL,3);"));
        query.exec(QString("INSERT INTO bookmarks VALUES(11,'Reddit - What''s New Online?','http://www.reddit.com','\\',NULL,4);"));
    }

    db.close();
}

BookmarkManager::~BookmarkManager()
{

}

void BookmarkManager::addFolder(QString path, QString folder)
{

}

void BookmarkManager::addBookmark(QString path, QString url, QString name)
{
}

QList<bookmark_t> BookmarkManager::parse()
{
    QList<bookmark_t> bookmarks;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
    db.open();
    QSqlQuery query;
    query.prepare("SELECT `title`, `url`, `path` from bookmarks ORDER BY `path` ASC, `order` ASC;");
    query.exec();

    while (query.next()) {
        bookmark_t bookmark;
        if(query.value(0).toString() == "-")
        {
            bookmark.isSeperator = true;
            bookmark.path = query.value(2).toString();
        }
        else
        {
            bookmark.isSeperator = false;
            bookmark.title = query.value(0).toString();
            bookmark.url = query.value(1).toString();
            bookmark.path = query.value(2).toString();
        }

        bookmarks.append(bookmark);
    }
    db.close();

    return bookmarks;
}

