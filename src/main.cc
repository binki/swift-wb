/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

#include "swiftwnd.h"
#include "sevenhelper.h"
#include <QtGui/QApplication>
#include <QUrl>
#include <QString>

#define SHIT     true
//#define CODENAME   "Somonauk"

#ifdef STATIC
Q_IMPORT_PLUGIN(qjpeg)
Q_IMPORT_PLUGIN(qgif)
#endif

int main(int argc, char *argv[])
{
    //Q_INIT_RESOURCE(swift);
    QApplication a(argc, argv);
    a.setApplicationVersion("1.0");
    QUrl url;

#ifndef CODENAME
    a.setOrganizationName("Swift");
    a.setApplicationName("Swift");
#else
    a.setOrganizationName(CODENAME);
    a.setApplicationName(CODENAME);
#endif

    if(argc != 0)
        url = QUrl(QString(argv[1]));

    SwiftMainWindow w(0, 0, false, false, url);

#ifdef _WIN32
    QSettings settings("Swift", "Swift");
    if (settings.value("Use7Transparency").toBool() &&
        QtWin::isCompositionEnabled())
    {
        QtWin::extendFrameIntoClientArea(&w);
        w.setContentsMargins(0, 0, 0, 0);
    }
#endif

    w.show();
   return a.exec();
}
