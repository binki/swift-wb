/************************************

swift web browser
Copyright (C) 2006-2010 Chris Fuenty <zimmy@zimmy.co.uk>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

************************************/

/* 
It was claimed that I have an agenda against old visual studio.
That is false and this ASCII art was placed here to prove it.
It will remain in the source. --stitch

####### ######  #       #     #  #####
#     # #     # #        #   #  #     #
#     # #     # #         # #         #
#     # ######  #          #        ##
#     # #   #   #          #       #
#     # #    #  #          #
####### #     # #######    #       #

*/

#include "locationbar.h"

LocationBar::LocationBar(QWidget *parent)
        : QWidget(parent)
{
    Settings settings(this);
    m_layout = new QHBoxLayout(this);
    m_layout->setSpacing(0);
    m_layout->setContentsMargins(0,0,0,0);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    m_lineEdit = new QComboBox(this);
    m_edit = new QLineEdit(this);

    m_edit->setTextMargins(m_lineEdit->height() - 4, 0, 0, 0);
    m_pageActions = new QToolButton(m_edit);
    m_pageActions->setAutoRaise(true);
    m_pageActions->raise();
    m_pageActions->setMaximumSize(m_lineEdit->height() - 4, m_lineEdit->height() - 4);
    m_pageActions->setIcon(QIcon(QWebSettings::webGraphic(QWebSettings::DefaultFrameIconGraphic)));
    m_splitter = new QSplitter(this);
    m_splitter->setOrientation(Qt::Horizontal);
    m_lineEdit->setLineEdit(m_edit);
    m_searchBox = new SearchBox(this);
    m_splitter->addWidget(m_lineEdit);
    m_splitter->addWidget(m_searchBox);
    m_layout->addWidget(m_splitter);
    if(!settings.value("ToolbarSizeGripState").isNull())
        m_splitter->restoreGeometry(settings.value("ToolbarSizeGripState").toByteArray());
    else
        m_searchBox->setGeometry(0, 0, 120, 16);


    connect(m_searchBox, SIGNAL(navigateToSearch(QUrl)), this, SLOT(navigateToSearch(QUrl)));
    connect(m_edit, SIGNAL(returnPressed()), this, SLOT(returnPressed()));
    connect(m_lineEdit, SIGNAL(currentIndexChanged(const QString &)),
        m_lineEdit, SLOT(currentIndexChanged(const QString &)));
    connect(m_edit, SIGNAL(textEdited(const QString&)), this, SLOT(textEdited(const QString&)));

    QWidget *tableParent = qobject_cast<QWidget*>(this->parent());
    m_tableWidget = new AwesomeBarView(tableParent);
    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer, SIGNAL(timeout()), this, SLOT(searchTimeout()));
    connect(m_tableWidget, SIGNAL(urlActivated(QUrl)), this, SIGNAL(loadUrl(QUrl)));
    shownView = false;
}

LocationBar::~LocationBar()
{
    Settings settings(this);
    settings.setValue("ToolbarSizeGripState", m_splitter->saveGeometry());
}

void LocationBar::saveSplitterRect()
{
    Settings settings(this);
    settings.setValue("ToolbarSizeGripState", m_splitter->saveGeometry());
}

void LocationBar::activateSearchBox()
{
    m_searchBox->activateWindow();
}

void LocationBar::activatetLocation()
{
    m_lineEdit->activateWindow();
}

void LocationBar::returnPressed()
{
    if(m_tableWidget->isVisible())
        m_tableWidget->setVisible(false);

    /* this needs to be redone when i'm less tired */
    QString url = m_lineEdit->lineEdit()->text();
    QUrl final;
    final.setUrl(url, QUrl::TolerantMode);
    if(final.scheme() == NULL)
        final.setScheme("http");

    if(final.host() == NULL)
    {
        final.setHost(url);
        final.setPath("/");
    }

    emit loadUrl(final);
}

void LocationBar::navigateToSearch(QUrl url)
{
    emit loadSearch(url);
}

void LocationBar::setIcon(QIcon icon)
{
    if(icon.isNull())
        m_pageActions->setIcon(QIcon(QWebSettings::webGraphic(QWebSettings::DefaultFrameIconGraphic)));
    else
        m_pageActions->setIcon(icon);
}

void LocationBar::setText(QString text)
{
    m_edit->setText(text);
}

void LocationBar::setSecure(QSslConfiguration config)
{
    Q_UNUSED(config);
    m_edit->setAutoFillBackground(true);
    m_lineEdit->setAutoFillBackground(true);
    QPalette pal;
    pal.setColor(QPalette::Base,QColor(243, 247, 129));
    m_edit->setPalette(pal);
    m_edit->setBackgroundRole(QPalette::Base);
    m_lineEdit->setPalette(pal);
    m_lineEdit->setBackgroundRole(QPalette::Base);
}

void LocationBar::setInsecure()
{
    QLineEdit tmp;
    m_edit->setAutoFillBackground(false);
    m_lineEdit->setAutoFillBackground(false);
    m_edit->setPalette(tmp.palette());
    m_edit->setBackgroundRole(QPalette::Base);
    m_lineEdit->setPalette(tmp.palette());
    m_lineEdit->setBackgroundRole(QPalette::Base);
}

void LocationBar::textEdited(const QString &text)
{
    if(timer->isActive())
        timer->stop();

    /* make sure that we have typed (or selected) something new here */
    if(text != m_lastTypedItem)
    {
        timer->start();
        shownView = true;
    }

    m_lastTypedItem = text;
}

void LocationBar::searchTimeout()
{
    if(!shownView)
        return;

    m_tableWidget->clearContents();
    QPoint global = mapToParent(m_lineEdit->pos());
    global.setY(global.y() + m_lineEdit->height()-1);
    int width = m_lineEdit->width();

    m_tableWidget->move(global);
    m_tableWidget->setMaximumWidth(width);
    m_tableWidget->setGeometry(global.x(), global.y()+ m_lineEdit->height()-1, width, height() * 11);

    QList<urlitem_t> items = completeString(m_edit->text());

    if(items.count() == 0)
        return;

    int count = items.count();

    for(int i = 0; i < items.count(); ++i)
    {
        AwesomeBarItem *pop = new AwesomeBarItem(this, items[i], i);
        pop->setMaximumHeight(pop->height() / 2);
        m_tableWidget->addItem(i, pop);
    }
    m_tableWidget->scroll(0, 0);
    m_tableWidget->doShow(); /* do this last to prevent showing a blank resultset */
    m_tableWidget->hideRow(count++);
    shownView = false;
    return;
}

QList<urlitem_t> LocationBar::completeString(QString search)
{
    /* I will be the first to admit, at the moment this is a very ghetto system.
       The goal will be eventually to ship swift with a full text search enabled
       SQLite library or require it as a dependency.  I don't really like going
       either route here.   -- stitch */

    QList<urlitem_t> results;

    QStringList searchStrings = search.split(" ");
    unsigned int count = searchStrings.count();

    /* if we don't have a count, absoultely nothing will be returned. */
    if(count != 0)
    {
        /* do some formatting wizardy for a SQL LIKE statement */
        QString sqlSearch;
        if(count == 1)
        {
            sqlSearch = search.prepend('%').append('%');
        }
        else
        {
            sqlSearch = searchStrings.join(QString("%")).prepend('%').append('%');
        }

        /* our SQL search queries */

        QString historySearch = QString("SELECT DISTINCT `Title`, `URL` FROM `history` WHERE `Title` LIKE :string ORDER BY `AccessCount` DESC LIMIT 15");
        QString bookmarkSearch = QString("SELECT `title`, `url` FROM `bookmarks` WHERE `title` LIKE :string;");

        /* Preform the SQL queries and get the results.
           Individual results are of type urlitem_t and will be returned in a QList */

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/swift.db");
        db.open();
        QSqlQuery query(db);
        query.prepare(bookmarkSearch);
        query.bindValue("string", sqlSearch);
        query.exec();
        while (query.next())
        {
            urlitem_t item;
            item.isBookmark = false;
            item.title = query.value(0).toString();
            item.url = QUrl(query.value(1).toString());
            results.append(item);
        }

        query.clear();

        query.prepare(historySearch);
        query.bindValue("string", sqlSearch);
        query.exec();
        while (query.next())
        {
            urlitem_t item;
            item.isBookmark = false;
            item.title = query.value(0).toString();
            item.url = QUrl(query.value(1).toString());
            results.append(item);
        }
        
        db.close();
    }

    /* return the results */
    return results;
}

AwesomeBarItem::AwesomeBarItem(QWidget *parent, urlitem_t url, int position)
    :QWidget(parent),
    m_url(url),
    position(position)
{
    if (objectName().isEmpty())
        setObjectName(QString::fromUtf8("widget"));
    resize(465, 99);
    gridLayout = new QGridLayout(this);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    mainLayout = new QHBoxLayout();
    mainLayout->setObjectName(QString::fromUtf8("mainLayout"));
    pageIconLayout = new QVBoxLayout();
    pageIconLayout->setObjectName(QString::fromUtf8("pageIconLayout"));
    pageIcon = new QLabel(this);
    pageIcon->setObjectName(QString::fromUtf8("pageIcon"));
    QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(pageIcon->sizePolicy().hasHeightForWidth());
    pageIcon->setSizePolicy(sizePolicy);
    pageIcon->setMaximumSize(QSize(16, 16));
    //if(trusted == true)
    if(!QWebSettings::iconForUrl(m_url.url).isNull())
        pageIcon->setPixmap(QPixmap(QWebSettings::iconForUrl(m_url.url).pixmap(16, 16, QIcon::Normal, QIcon::Off)));
    else
        pageIcon->setPixmap(QPixmap(QWebSettings::webGraphic(QWebSettings::DefaultFrameIconGraphic)));

    pageIconLayout->addWidget(pageIcon);
    verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
    pageIconLayout->addItem(verticalSpacer);
    mainLayout->addLayout(pageIconLayout);
    textLayout = new QVBoxLayout();
    textLayout->setSpacing(8);
    textLayout->setObjectName(QString::fromUtf8("textLayout"));
    titleLabel = new QLabel(this);
    titleLabel->setObjectName(QString::fromUtf8("titleLabel"));
    QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(titleLabel->sizePolicy().hasHeightForWidth());
    titleLabel->setSizePolicy(sizePolicy1);
    QFont font;
    font.setBold(true);
    font.setWeight(75);
    titleLabel->setFont(font);
    titleLabel->setText(m_url.title);
    textLayout->addWidget(titleLabel);
    urlLabel = new QLabel(this);
    urlLabel->setObjectName(QString::fromUtf8("urlLabel"));
    QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
    sizePolicy2.setHorizontalStretch(0);
    sizePolicy2.setVerticalStretch(0);
    sizePolicy2.setHeightForWidth(urlLabel->sizePolicy().hasHeightForWidth());
    urlLabel->setSizePolicy(sizePolicy2);
    urlLabel->setIndent(14);
    urlLabel->setText(m_url.url.toString());

    textLayout->addWidget(urlLabel);
    mainLayout->addLayout(textLayout);
    bookmarkLayout = new QVBoxLayout();
    bookmarkLayout->setObjectName(QString::fromUtf8("bookmarkLayout"));
    bookmarkIcon = new QLabel(this);
    bookmarkIcon->setObjectName(QString::fromUtf8("bookmarkIcon"));
    sizePolicy.setHeightForWidth(bookmarkIcon->sizePolicy().hasHeightForWidth());
    bookmarkIcon->setSizePolicy(sizePolicy);
    bookmarkIcon->setMaximumSize(QSize(16, 16));
    if(m_url.isBookmark == true)
        bookmarkIcon->setPixmap(QPixmap(QString::fromUtf8(":/Swift/add_bookmark")));
    bookmarkIcon->setScaledContents(true);
    bookmarkLayout->addWidget(bookmarkIcon);
    verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
    bookmarkLayout->addItem(verticalSpacer_2);
    mainLayout->addLayout(bookmarkLayout);
    mainLayout->setStretch(0, 1);
    mainLayout->setStretch(1, 1);
    mainLayout->setStretch(2, 1);

    gridLayout->addLayout(mainLayout, 0, 0, 1, 1);
    setMouseTracking(true);
}

void AwesomeBarItem::mouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    emit hilighted(position);
}

AwesomeBarView::AwesomeBarView(QWidget *parent = 0)
    :QTableWidget(1, 1, parent)
{
    verticalHeader()->hide();
    horizontalHeader()->hide();
    setVisible(false);
    setAlternatingRowColors(true);
    horizontalHeader()->setStretchLastSection(true);
    setSelectionMode(QAbstractItemView::SingleSelection);
    m_myOrigSize = size();
}

void AwesomeBarView::addItem(int position, AwesomeBarItem *item)
{
    insertRow(position);
    QModelIndex idx = model()->index(position, 0);
    setIndexWidget(idx, item);
    m_itemHeight = item->height();
    resizeRowToContents(position);
    connect(item, SIGNAL(hilighted(int)), this, SLOT(hilightedItem(int)));
}

void AwesomeBarView::doShow()
{
    if(rowCount() < 5)
    {
        int height = rowCount() * m_itemHeight;
        height = height + (m_itemHeight / 2);
        setGeometry(x(), y(), width(), height);
    }
    else
    {
        int height = 6 * m_itemHeight;
        setGeometry(x(), y(), width(), height);
    }
    setVisible(true);
}

void AwesomeBarView::leaveEvent(QEvent *event)
{
    if(event->type() != QEvent::MouseButtonPress)
    {
        clearObjects();
        setVisible(false);
    }
}

void AwesomeBarView::hilightedItem(int position)
{
    try {
        setCurrentCell(position, 0);
    } catch(int e) {
        Q_UNUSED(e);
    }
}

void AwesomeBarView::mousePressEvent(QMouseEvent *event)
{
    try 
    {
        int row = rowAt(event->pos().y());
        QModelIndex idx = model()->index(row, 0);
        if(row < rowCount() || row > 0)
        {
            AwesomeBarItem *item = qobject_cast<AwesomeBarItem*>(indexWidget(idx));
            emit(urlActivated(item->url()));
            clearObjects();
            setVisible(false);
        }
    }
    catch(int e)
    {
        Q_UNUSED(e);
        /* we don't care if we accidently activate a non-existant row, just ignore it */
    }
}

void AwesomeBarView::clearObjects()
{
    int count = rowCount();
    if(count == 0)
        return;

    for(int i = count; i >= 0; i--)
        removeRow(i);

    setGeometry(x(), y(), m_myOrigSize.width(), m_myOrigSize.height());
}
