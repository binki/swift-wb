TARGET = swift
TEMPLATE = app
INCLUDEPATH += ../include
DESTDIR = ../build
MOC_DIR = ../moc
QT = core \
    gui \
    network \
    xml \
    sql \
    webkit
SOURCES += ../src/webview.cc \
    ../src/tabwidget.cc \
    ../src/swiftwnd.cc \
    ../src/settings.cc \
    ../src/networkaccessmanager.cc \
    ../src/main.cc \
    ../src/locationbar.cc \
    ../src/cookiejar.cc \
    ../src/bookmarkmanager.cc \
    ../src/addbookmark.cc \
    ../src/searchbox.cc \
    ../src/about.cc \
    ../src/downloads.cc \
    ../src/preferences.cc \
    ../src/historymanager.cc \
    ../src/zonemanager.cc \
    ../src/pageinfo.cc \
    ../src/desktopservices.cc
win32:SOURCES += ../src/sevenhelper.cc
HEADERS += ../include/webview.h \
    ../include/tabwidget.h \
    ../include/swiftwnd.h \
    ../include/settings.h \
    ../include/networkaccessmanager.h \
    ../include/locationbar.h \
    ../include/cookiejar.h \
    ../include/bookmarkmanager.h \
    ../include/addbookmark.h \
    ../include/searchbox.h \
    ../include/about.h \
    ../include/downloads.h \
    ../include/preferences.h \
    ../include/historymanager.h \
    ../include/zonemanager.h \
    ../include/pageinfo.h \
    ../include/desktopservices.h
win32:HEADERS += ../include/sevenhelper.h
RESOURCES += ../res/swift.qrc
win32:RC_FILE = ../win32/swift.rc
OTHER_FILES += 
